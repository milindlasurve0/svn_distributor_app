package com.mindsarray.pay1distributor.services;

import android.app.IntentService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.google.android.gms.gcm.GcmPubSub;
import com.google.android.gms.gcm.GoogleCloudMessaging;
import com.google.android.gms.iid.InstanceID;
import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.util.Storage;

import java.io.IOException;
import java.util.Random;

/**
 * Created by rohan on 27/8/15.
 */
public class RegistrationIntentService extends IntentService {

    private static final String TAG = "RegIntentService";

    public RegistrationIntentService() {
        super(TAG);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Random randomGenerator = new Random();
        for (int n = 0; n < 5; ++n) {
            try {
                InstanceID instanceID = InstanceID.getInstance(this);
                String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
                        GoogleCloudMessaging.INSTANCE_ID_SCOPE, null);
                Storage.setSyncPreferencesString(this, Storage.SHAREDPREFERENCE_GCMID, "" + token);
                Log.e(TAG, "GCM Registration Token: " + token);
                break;
            } catch (IOException e) {
                Log.e(TAG, "Failed to complete token refresh", e);
                try {
                    Thread.sleep((1 << n) * 1000 + randomGenerator.nextInt(1001));
                }
                catch(Exception ex){
                    ex.printStackTrace();
                }
            }
        }
        Intent registrationComplete = new Intent(Storage.REGISTRATION_COMPLETE);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }
}