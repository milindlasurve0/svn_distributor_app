package com.mindsarray.pay1distributor.services;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.gcm.GcmListenerService;
import com.mindsarray.pay1distributor.contenthelpers.NotificationsDataSource;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

public class gcmListenerService extends GcmListenerService {

    String TAG = "gcmListenerService";
    NotificationsDataSource mNotificationsDataSource;
    Context mContext = gcmListenerService.this;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        try {
            Log.e(TAG, "From: " + from);
            Log.e(TAG, "Data: " + data.toString());

//            if(data.containsKey("mobile")) {
//                String mobile = data.getString("mobile");
//                if(mobile.equals(Storage.getPersistentString(this, Storage.SHAREDPREFERENCE_MOBILE_NUMBER))) {
//                    if (data.containsKey("type")) {
//                        String type = data.getString("type");
//                        Log.e(TAG, "Type: " + type);
//                        String message, balance;
//                        switch (type) {
//                            case "balance":
//                                if (data.containsKey("message")) {
//                                    message = data.getString("message");
//                                    JSONObject messageJson = new JSONObject(message);
//                                    balance = messageJson.getString("balance");
//                                    Log.e("Balance: ", "" + balance);
//                                    Storage.setPersistentString(this, Storage.SHAREDPREFERENCE_BALANCE, balance);
//
//                                    Motley.notify(this, "Balance update", "Current balance: Rs. " + balance);
//                                }
//                            case "notification":
//                                if (data.containsKey("msg")) {
//                                    message = data.getString("msg");
//                                    balance = data.getString("balance");
//                                    if (!balance.equals("null") && !balance.equals(""))
//                                        Storage.setPersistentString(this, Storage.SHAREDPREFERENCE_BALANCE, balance);
//
//                                    Motley.notify(this, "Pay1", message);
//                                }
//                        }
//                    }
//                }
//            }

            String msg = data.getString("data");
            String description = "", title = "", type = "", balance = "";
            try {
                JSONObject jsonObject = new JSONObject(msg);
                description = jsonObject.getString("msg");
                title = jsonObject.getString("title");
                type = jsonObject.getString("type");
                balance = jsonObject.getString("balance");

                mNotificationsDataSource = new NotificationsDataSource(mContext);
                mNotificationsDataSource.open();
                mNotificationsDataSource.save(description, type, System.currentTimeMillis() + "");
                mNotificationsDataSource.close();

                if (type.equalsIgnoreCase("notification")) {
                    Motley.notify(this, "Pay1", description);
                    if (!balance.equals("null") && !balance.equals(""))
                        Storage.setPersistentString(this, Storage.SHAREDPREFERENCE_BALANCE, balance);
                } else if (type.equalsIgnoreCase("banner")) {

                }

                Intent intent = new Intent("last_notification");
                mContext.sendBroadcast(intent);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
