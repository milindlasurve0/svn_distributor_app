package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohan on 15/4/16.
 */

public class SendTopupRequestActivity extends ChildActivity {

    private Context mContext = SendTopupRequestActivity.this;
    private String topupBank, transferType;
    private Map transferTypeHash = new HashMap();
    private EditText topupAmountEditText, topupTransIdEditText;
    Spinner bankSpinner;
    Spinner transferTypesSpinner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_send_topup_request);

        banksAndTransferTypes();
    }

    private void banksAndTransferTypes() {
        final Map params = new HashMap();
        params.put("method", "banksAndTransferTypes");

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Fetching banks..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject description = jsonObjectResponse.getJSONObject("description");
                        JSONArray banks = description.getJSONArray("banks");
                        JSONArray transferTypes = description.getJSONArray("transfer_types");

                        bankSpinner = (Spinner) findViewById(R.id.bankSpinner);
                        transferTypesSpinner = (Spinner) findViewById(R.id.transferTypes);

                        bankSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                topupBank = parent.getItemAtPosition(position).toString();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        transferTypesSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                            @Override
                            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                transferType = transferTypeHash.get(parent.getItemAtPosition(position).toString()).toString();
                            }

                            @Override
                            public void onNothingSelected(AdapterView<?> parent) {

                            }
                        });

                        ArrayList banksList = new ArrayList<String>();
                        for(int i = 0; i < banks.length(); i++){
                            banksList.add(banks.getString(i));
                        }


                        ArrayList transferTypeList = new ArrayList<String>();
                        for(int i = 0; i < transferTypes.length(); i++){
                            String[] ttKeyValue = transferTypes.getString(i).split(":");
                            transferTypeList.add(ttKeyValue[1]);
                            transferTypeHash.put(ttKeyValue[1], ttKeyValue[0]);
                        }

                        // Creating adapter for spinner
                        ArrayAdapter<String> banksAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, banksList);
                        ArrayAdapter<String> transferTypesAdapter = new ArrayAdapter<String>(mContext, android.R.layout.simple_spinner_item, transferTypeList);

                        // Drop down layout style - list view with radio button
                        banksAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        transferTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

                        // attaching data adapter to spinner
                        bankSpinner.setAdapter(banksAdapter);
                        transferTypesSpinner.setAdapter(transferTypesAdapter);
                        bankSpinner.setSelection(0);
                        transferTypesSpinner.setSelection(0);

                        topupAmountEditText = (EditText) findViewById(R.id.topupAmount);
                        topupTransIdEditText = (EditText) findViewById(R.id.topupTransId);
                        topupAmountEditText.setText("");
                        topupTransIdEditText.setText("");

                        Button sendRequestButton = (Button) findViewById(R.id.sendRequestButton);
                        sendRequestButton.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                if(!topupAmountEditText.getText().toString().equals("")){
                                    sendTopupRequest();
                                }
                                else {
                                    Motley.showOneButtonDialog(mContext, "Enter an amount to continue", "Invalid Details", "OK");
                                }
                            }
                        });
                    }
                }
                catch(Exception e) {
                    Log.e("banksAndXferTypes", e.getMessage() + "");
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(params, responseListener, errorBlock);
    }

    public void sendTopupRequest(){
        Map requestParams = new HashMap();

        requestParams.put("method", "sendBalanceTopupRequest");
        requestParams.put("bank_acc_id", topupBank);
        requestParams.put("trans_type_id", transferType);
        requestParams.put("amount", topupAmountEditText.getText().toString());
        requestParams.put("bank_trans_id", topupTransIdEditText.getText().toString());

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Sending Request..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        String description = jsonObjectResponse.getString("description");

                        bankSpinner.setSelection(0);
                        transferTypesSpinner.setSelection(0);
                        topupAmountEditText.setText("");
                        topupTransIdEditText.setText("");

                        Motley.showOneButtonDialog(mContext, description, "Request Received", "OK");
                    }

                }
                catch(Exception e) {
                    Log.e("sendTopupRequest", e.getMessage() + "");
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };

        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(requestParams, responseListener, errorBlock);
    }
}
