package com.mindsarray.pay1distributor;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;

import com.google.android.gms.analytics.HitBuilders;
import com.mindsarray.pay1distributor.adapters.DashboardGridAdapter;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.models.GridMenuItem;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import java.util.ArrayList;


public class DashboardActivity extends BaseActivity {
    GridView dashboardGridView;
    ArrayList<GridMenuItem> dashboardGridItems = new ArrayList<GridMenuItem>();
    DashboardGridAdapter dashboardGridAdapter;
    Context mContext = DashboardActivity.this;
    Activity mActivity = DashboardActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_dashboard);

        setActivityView(R.layout.activity_dashboard);

        View actionBarTitleView = getActionBarView().findViewById(R.id.title);
        actionBarTitleView.setBackgroundResource(R.drawable.ic_logo_bg_theme_red);
        actionBar.setDisplayHomeAsUpEnabled(false);

        dashboardGridItems.add(new GridMenuItem(R.drawable.ic_balance_transfer, getResources().getString(R.string.title_activity_retailer_balance_transfer), 0));
        dashboardGridItems.add(new GridMenuItem(R.drawable.ic_create_retailer, getResources().getString(R.string.title_activity_create_retailer), 0));
        dashboardGridItems.add(new GridMenuItem(R.drawable.ic_retailer_list, getResources().getString(R.string.title_activity_retailers), 0));
        dashboardGridItems.add(new GridMenuItem(R.drawable.ic_report_balance_transfer, getResources().getString(R.string.report_balance_transfer), 0));

//        if(Storage.is_session(mContext)) {
//            RetailersDataSource mRetailersDataSource = new RetailersDataSource(mContext);
//            mRetailersDataSource.open();
//            int count = mRetailersDataSource.getInTrialRetailers().size();
//            mRetailersDataSource.close();
//            if(count > 0)
//                dashboardGridItems.add(new GridMenuItem(R.drawable.ic_kyc_pending, getResources().getString(R.string.kyc_pending), count));
//        }

        dashboardGridView = (GridView) findViewById(R.id.dashboardGridView);
        dashboardGridAdapter = new DashboardGridAdapter(mContext,
                R.layout.item_grid_dashboard, dashboardGridItems);
        dashboardGridView.setAdapter(dashboardGridAdapter);

        dashboardGridView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int pos,
                                    long arg3) {
                switch (pos) {
                    case 0:
                        startActivity(new Intent(mContext,
                                RetailerBalanceTransferActivity.class));
                        break;
                    case 1:
                        startActivity(new Intent(mContext,
                                TrialRetailerActivity.class));
                        break;
                    case 2:
                        startActivity(new Intent(mContext,
                                RetailersActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(mContext,
                                ReportBalanceTransferActivity.class));
                        break;
                    case 4:
                        startActivity(new Intent(mContext,
                                KYCPendingActivity.class));
                        break;
                }
            }
        });

//        Account syncAccount = Motley.CreateSyncAccount(mContext);
//        Motley.sync(syncAccount);
    }

    @Override
    public void onBackPressed() {
        String message = "Are you sure you want to exit?";

        Runnable confirmBlock = new Runnable() {
            @Override
            public void run() {
                Intent broadcastIntent = new Intent();
                broadcastIntent.setAction("com.mindsarray.pay1distributor.ACTION_LOGOUT");
                mContext.sendBroadcast(broadcastIntent);
                finish();
            }
        };

        if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        else
            Motley.showTwoButtonDialog(mContext, message, "PAY1", "YES", confirmBlock, null);
    }
}
