package com.mindsarray.pay1distributor;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Configuration;
import android.content.res.TypedArray;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;
import com.mindsarray.pay1distributor.adapters.NavDrawerListAdapter;
import com.mindsarray.pay1distributor.models.NavDrawerItem;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import java.util.ArrayList;

/**
 * Created by rohan on 14/7/15.
 */
public class BaseActivity extends AppCompatActivity {

    ActionBar actionBar;
    ViewGroup parentActivityLayout;

    public DrawerLayout mDrawerLayout;
    private FrameLayout mHostFragment;
    public ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private String[] navMenuTitles;
    private TypedArray navMenuIcons;
    private ArrayList<NavDrawerItem> navDrawerItems;
    private NavDrawerListAdapter adapter;

    private Context mContext = BaseActivity.this;
    private Activity mActivity = BaseActivity.this;
    protected TextView actionBarTitleTextView;

    protected BroadcastReceiver mFinishActivityReceiver;

    protected Tracker mTracker;
    String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        supportRequestWindowFeature(Window.FEATURE_ACTION_BAR_OVERLAY);
        super.onCreate(savedInstanceState);

        setContentView(R.layout.parent_activity);

        BaseApplication application = (BaseApplication) getApplication();
        mTracker = application.getDefaultTracker();

        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("com.mindsarray.pay1distributor.ACTION_LOGOUT");
        mFinishActivityReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.e("onReceive", "Logout in progress");
                finish();
            }
        };
        registerReceiver(mFinishActivityReceiver, intentFilter);

        actionBar = getSupportActionBar();
//        actionBar.setDisplayShowCustomEnabled(true);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        mTitle = mDrawerTitle = getTitle();
        if(Storage.is_session(mContext)) {
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_logged_in);
            navMenuTitles[0] = Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_USER_NAME);
        }
        else
            navMenuTitles = getResources().getStringArray(R.array.nav_drawer_items_logged_out);
        navMenuIcons = getResources()
                .obtainTypedArray(R.array.nav_drawer_icons);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mHostFragment = (FrameLayout) findViewById(R.id.frame_container);
        mDrawerList = (ListView) findViewById(R.id.list_slidermenu);

        navDrawerItems = new ArrayList<>();
        for(int i = 0; i < navMenuTitles.length; i++){
            navDrawerItems.add(new NavDrawerItem(navMenuTitles[i], navMenuIcons.getResourceId(i, -1)));
        }

        navMenuIcons.recycle();
        adapter = new NavDrawerListAdapter(getApplicationContext(),
                navDrawerItems);
        mDrawerList.setAdapter(adapter);

//        actionBar.setDisplayHomeAsUpEnabled(true);
//        actionBar.setHomeButtonEnabled(true);

        mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayout,
                R.string.app_name,
                R.string.app_name
        ){
            public void onDrawerClosed(View view) {
                actionBar.setTitle(mTitle);
                supportInvalidateOptionsMenu();
            }

            public void onDrawerOpened(View drawerView) {
                actionBar.setTitle(mDrawerTitle);
                supportInvalidateOptionsMenu();

                InputMethodManager inputMethodManager = (InputMethodManager)  mActivity.getSystemService(Activity.INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(mActivity.getCurrentFocus().getWindowToken(), 0);
            }

            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                super.onDrawerSlide(drawerView, slideOffset);

                float xPositionOpenDrawer = mDrawerList.getWidth();
                float xPositionWindowContent = (slideOffset * xPositionOpenDrawer);
                mHostFragment.setX(-xPositionWindowContent);
                getActionBarView().setX(-xPositionWindowContent);
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        mDrawerToggle.setDrawerIndicatorEnabled(false);

        try {
            actionBar.setDisplayShowCustomEnabled(true);
            actionBar.setDisplayShowTitleEnabled(false);
            actionBar.setDisplayHomeAsUpEnabled(true);
            LayoutInflater inflater = LayoutInflater.from(mContext);
            View actionBarTitleView = inflater.inflate(R.layout.action_bar_title, null);
            actionBarTitleTextView = (TextView) actionBarTitleView.findViewById(R.id.title);
            actionBar.setCustomView(actionBarTitleView);
        }
        catch(Exception e){
            e.printStackTrace();
        }

        mDrawerList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Motley.setNavigationDrawerAction(mActivity, position, mDrawerLayout);
            }
        });

        setBalance();
    }

    void setActivityView(int layoutResourceId){
        parentActivityLayout = (ViewGroup) findViewById(R.id.frame_container);
        ViewGroup.inflate(getApplicationContext(), layoutResourceId, parentActivityLayout);
    }

    void setActivityTitle(String title){
        actionBarTitleTextView.setText(title);
    }

    public View getActionBarView() {

        Window window = this.getWindow();
        final View decorView = window.getDecorView();
        final String packageName = getPackageName();
        final int resId = this.getResources().getIdentifier("action_bar_container", "id", packageName);
        final View actionBarView = decorView.findViewById(resId);
        return actionBarView;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        switch (item.getItemId()) {
            case R.id.action_side_panel:
                if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
                    mDrawerLayout.closeDrawer(Gravity.RIGHT);
                else {
                    mDrawerLayout.openDrawer(Gravity.RIGHT);
                    setBalance();
                }
                return true;
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.action_bar_menu, menu);

//        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//
//        MenuItem balanceItem = menu.findItem(R.id.action_balance);
//        View balanceItemView = inflater.inflate(R.layout.action_bar_balance_item, null);
//        balanceItem.setActionView(balanceItemView);
//
//        if(Storage.is_session(mContext))
//            balanceItem.setVisible(true);
//        else
//            balanceItem.setVisible(false);

        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mDrawerList);
        return super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        actionBar.setTitle(mTitle);
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggle
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Storage.REQUEST_LOGIN_SESSION && resultCode == Activity.RESULT_OK) {
            finish();
            startActivity(getIntent());
        }
    }

    public void setBalance(){
        LinearLayout balanceView = (LinearLayout) mDrawerLayout.findViewById(R.id.balanceView);
        if(balanceView != null) {
            if (Storage.is_session(mContext)) {
                TextView balanceTextView = (TextView) mDrawerLayout.findViewById(R.id.balance);

                balanceTextView.setText("Rs. " + Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_BALANCE));
                balanceView.setVisibility(View.VISIBLE);
                if(Storage.user_group(mContext) == Storage.SALESMAN){
                    TextView limitTextView = (TextView) mDrawerLayout.findViewById(R.id.balanceText);
                    limitTextView.setText("LIMIT: ");
                }
            } else {
                balanceView.setVisibility(View.GONE);
            }
        }
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName(TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }

    @Override
    public void onDestroy(){
        unregisterReceiver(mFinishActivityReceiver);
        super.onDestroy();
    }
}
