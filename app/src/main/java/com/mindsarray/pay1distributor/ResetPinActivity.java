package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ResetPinActivity extends BaseActivity {

    TextView otpMessageView;
    EditText otpView, pinView, confirmPinView;
    String mobile;
    Button resetButton, resendOTPButton;
    Context mContext = ResetPinActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_reset_pin);
        setActivityView(R.layout.activity_reset_pin);
        setActivityTitle("Reset Pin");

        otpMessageView = (TextView) findViewById(R.id.otp_message);
        otpView = (EditText) findViewById(R.id.otp);
        pinView = (EditText) findViewById(R.id.pin);
        confirmPinView = (EditText) findViewById(R.id.confirm_pin);
        resetButton = (Button) findViewById(R.id.reset);
        resendOTPButton = (Button) findViewById(R.id.resend_otp);

        Bundle extras = getIntent().getExtras();
        mobile = extras.getString("mobile");
        otpMessageView.setText("An OTP was sent to the mobile number " + mobile);

        resendOTPButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable responseBlock = new Runnable() {
                    @Override
                    public void run() {
                        Motley.showOneButtonDialog(mContext, "OTP was sent to mobile number " + mobile, "OTP SENT", "OK");
                    }
                };
                Motley.sendOTP(mContext, mobile, responseBlock);
            }
        });
        resetButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(otpView.getText().toString().equals("")){
                    Motley.showOneButtonDialog(mContext, "OTP field left empty", "VALIDATION ERROR", "OK");
                }
                else if(pinView.getText().toString().equals("")){
                    Motley.showOneButtonDialog(mContext, "Pin field left empty", "VALIDATION ERROR", "OK");
                }
                else if(pinView.getText().toString().length() < 4){
                    Motley.showOneButtonDialog(mContext, "Pin should be at least 4 digits long", "VALIDATION ERROR", "OK");
                }
                else if(!pinView.getText().toString().equals(confirmPinView.getText().toString())){
                    Motley.showOneButtonDialog(mContext, "Pins do not match", "VALIDATION ERROR", "OK");
                }
                else {
                    resetPin();
                }
            }
        });
    }

    void resetPin(){
        final Map resetPinParams = new HashMap();
        resetPinParams.put("method", "resetPin");
        resetPinParams.put("m", mobile);
        resetPinParams.put("otp", otpView.getText().toString());
        resetPinParams.put("pin", pinView.getText().toString());

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Resetting pin..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(mContext, LoginActivity.class);
                        intent.putExtra("mobile", mobile);
                        intent.putExtra("pin", pinView.getText().toString());
                        finish();
                        startActivity(intent);
                    } else {
                        Motley.showOneButtonDialog(mContext, jsonObjectResponse.getString("description"), "FAILED", "OK");
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(resetPinParams, responseListener, errorBlock);
    }
}
