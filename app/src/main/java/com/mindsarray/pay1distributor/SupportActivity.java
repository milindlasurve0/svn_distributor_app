package com.mindsarray.pay1distributor;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.mindsarray.pay1distributor.adapters.SupportListAdapter;
import com.mindsarray.pay1distributor.util.Storage;

public class SupportActivity extends BaseActivity {

    Context mContext = SupportActivity.this;
    SupportListAdapter mSupportListAdapter;
    String userType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_support);
        setActivityView(R.layout.activity_support);
        setActivityTitle("Support");

        if(Storage.user_group(mContext) == Storage.DISTRIBUTOR){
            userType = "Distributor";
        }
        else {
            userType = "Salesman";
        }
        String[] textArray = {
            "Simply write to us on channel@pay1.in",
            "Call us on: 02242932297",
            "Bank Details", "Request Limit"
        };

        int[] iconArray = {
            R.drawable.ic_mail,
            R.drawable.ic_misscall,
            R.drawable.ic_bankdetails, R.drawable.ic_bankdetails
        };

        ListView supportListView = (ListView) findViewById(R.id.supportListView);
        mSupportListAdapter = new SupportListAdapter(mContext, textArray, iconArray);
        supportListView.setAdapter(mSupportListAdapter);

        supportListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (position) {
                    case 0:
                        Intent emailIntent = new Intent(Intent.ACTION_SEND);
                        emailIntent.setData(Uri.parse("mailto:"));
                        emailIntent.setType("text/plain");
                        emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{"channel@pay1.in"});
                        emailIntent.putExtra(Intent.EXTRA_SUBJECT, userType + " Help - Mobile: " +
                                Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_MOBILE_NUMBER));
                        startActivity(emailIntent);
                        break;
                    case 1:
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse("tel:02242932297"));
                        startActivity(callIntent);
                        break;
                    case 2:
                        startActivity(new Intent(mContext, BankCashDepositActivity.class));
                        break;
                    case 3:
                        startActivity(new Intent(mContext, SendTopupRequestActivity.class));
                        break;
                }
            }
        });
    }
}
