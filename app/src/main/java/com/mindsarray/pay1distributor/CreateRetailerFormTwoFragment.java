package com.mindsarray.pay1distributor;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.mindsarray.pay1distributor.models.Document;
import com.mindsarray.pay1distributor.util.AttachImage;
import com.mindsarray.pay1distributor.util.DocumentButton;
import com.mindsarray.pay1distributor.util.ExpandableHeightGridView;
import com.mindsarray.pay1distributor.util.Motley;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;

public class CreateRetailerFormTwoFragment extends Fragment {

    private static final String ARG_PARAM1 = "param1";
    public AttachImage mAttachImageAddress;
    public AttachImage mAttachImageId;
    public AttachImage mAttachImageShop;

    private int position;

    private OnFragmentInteractionListener mListener;
    private Fragment mFragment = CreateRetailerFormTwoFragment.this;

    public static CreateRetailerFormTwoFragment newInstance(int position) {
        CreateRetailerFormTwoFragment fragment = new CreateRetailerFormTwoFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_PARAM1, position);
        fragment.setArguments(args);
        return fragment;
    }

    public CreateRetailerFormTwoFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            position = getArguments().getInt(ARG_PARAM1);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_create_retailer_form_two_1, container, false);

//        Motley.setSpinner(getActivity(), view, R.id.location_type_spinner, R.array.location_type_array);

        final RelativeLayout natureOfBusinessButtons = (RelativeLayout) view.findViewById(R.id.natureOfBusinessButtons);
        final EditText other_text = (EditText) view.findViewById(R.id.other_text);

        for(int i = 0; i < natureOfBusinessButtons.getChildCount() - 1; i++){
            Button child = (Button) natureOfBusinessButtons.getChildAt(i);
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int j = 0; j < natureOfBusinessButtons.getChildCount() - 1; j++) {
                        Button child = (Button) natureOfBusinessButtons.getChildAt(j);
                        child.setSelected(false);
                    }
                    v.setSelected(true);
                    other_text.setVisibility(View.GONE);
                    if(((Button) v).getText().toString().equals("Other")){
                        other_text.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        final RelativeLayout areaOfBusinessButtons = (RelativeLayout) view.findViewById(R.id.areaOfBusinessButtons);

        for(int i = 0; i < areaOfBusinessButtons.getChildCount(); i++){
            Button child = (Button) areaOfBusinessButtons.getChildAt(i);
            child.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for(int j = 0; j < areaOfBusinessButtons.getChildCount(); j++) {
                        Button child = (Button) areaOfBusinessButtons.getChildAt(j);
                        child.setSelected(false);
                    }
                    v.setSelected(true);
                }
            });
        }

        String addressMessage = "Electricity bill or Agreement proof";
        String shopPhotosMessage = "Upload max. 4 photos of your shop";

        mAttachImageId = uploadManager(view, R.id.uploadIDProofDialogButton, 1, "PAN CARD", "PAN card image", false);
        mAttachImageAddress = uploadManager(view, R.id.uploadAddressProofDialogButton, 1, "ADDRESS PROOF", addressMessage, false);
        mAttachImageShop = uploadManager(view, R.id.uploadShopPhotosDialogButton, 4, "SHOP PHOTOS", shopPhotosMessage, true);

        Button cancelButton = (Button) view.findViewById(R.id.cancel_button);
        Button confirmButton = (Button) view.findViewById(R.id.confirm_button);
        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateRetailerActivity) getActivity()).mViewPager.setCurrentItem(0, true);
            }
        });
        confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((CreateRetailerActivity) getActivity()).createRetailer();
            }
        });

        return view;
    }

    public AttachImage uploadManager(View view, int documentButtonResourceId,
                              int maxItems, final String title, final String message, boolean expanded){
        DocumentButton documentButton = (DocumentButton) view.findViewById(documentButtonResourceId);

        final Dialog uploadDialog = new Dialog(getActivity());
        uploadDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        uploadDialog.setContentView(R.layout.upload_dialog);

        Button buttonClose = (Button) uploadDialog
                .findViewById(R.id.buttonClose);
        buttonClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadDialog.dismiss();
            }
        });

        final TextView messageView = (TextView) uploadDialog
                .findViewById(R.id.message);
        final TextView titleView = (TextView) uploadDialog
                .findViewById(R.id.title);

        final ImageButton uploadButton = (ImageButton) uploadDialog.findViewById(R.id.uploadButton);
        final ExpandableHeightGridView ehGridView = (ExpandableHeightGridView) uploadDialog.findViewById(R.id.photos_gridview);
        if(expanded)
            ehGridView.setExpanded(true);
        if(maxItems == 1){
            ehGridView.setNumColumns(1);
        }
        AttachImage attachImage = new AttachImage(mFragment, getActivity(), ehGridView, maxItems, uploadButton, documentButton);

        documentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                messageView.setText("" + message);
                titleView.setText("" + title);

                uploadDialog.show();
            }
        });

        return attachImage;
    }

    public ArrayList<String> getAddressProofURIs(){
        return mAttachImageAddress.getImagesPathList();
    }

    public ArrayList<String> getShopPhotosURIs(){
        return mAttachImageShop.getImagesPathList();
    }

    public ArrayList<String> getIdProofURIs(){
        return mAttachImageId.getImagesPathList();
    }

    public ArrayList<String> getPhotoRemovalURIs(){
        ArrayList<String> mImageURIRemovalList = new ArrayList<String>();
        mImageURIRemovalList.addAll(mAttachImageId.mImageURIRemovalList);
        mImageURIRemovalList.addAll(mAttachImageAddress.mImageURIRemovalList);
        mImageURIRemovalList.addAll(mAttachImageShop.mImageURIRemovalList);
        return mImageURIRemovalList;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(mAttachImageAddress.intentRun)
            mAttachImageAddress.onActivityResult(requestCode, resultCode, data,1);
        else if(mAttachImageShop.intentRun)
            mAttachImageShop.onActivityResult(requestCode, resultCode, data,2);
        else if(mAttachImageId.intentRun)
            mAttachImageId.onActivityResult(requestCode, resultCode, data,0);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    private boolean edit(){
        return ((CreateRetailerActivity)getActivity()).edit();
    }

    public void loadImages(){
        if(edit()){
            Map documents = ((CreateRetailerActivity) getActivity()).retailer.documents();

            mAttachImageId.loadAndSave((ArrayList<Document>) documents.get("idProof"),0);
            mAttachImageAddress.loadAndSave((ArrayList<Document>) documents.get("addressProof"),1);
            mAttachImageShop.loadAndSave((ArrayList<Document>) documents.get("shopPhotos"),2);
        }
    }
}
