package com.mindsarray.pay1distributor;

import android.accounts.Account;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SyncInfo;
import android.content.SyncStatusObserver;
import android.os.Build;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mindsarray.pay1distributor.adapters.NameMobileAutoCompleteAdapter;
import com.mindsarray.pay1distributor.adapters.RetailersAdapter;
import com.mindsarray.pay1distributor.contenthelpers.NotificationsDataSource;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Map;
import java.util.Set;

public class RetailersActivity extends BaseActivity {

    private Context mContext = RetailersActivity.this;
    private RetailersDataSource mRetailersDataSource, retailer;
    private ArrayList<RetailersDataSource> retailers;
    private RetailersAdapter mRetailersAdapter;
    ListView retailersListView;

    private AutoCompleteTextView retailerMobileAutoCompleteTextView;

    Account syncAccount;

    MenuItem countMenuItem;

    String listType = "RETAILERS";

    private Object syncHandle;
    private SyncStatusObserver observer;
    boolean isSynchronizing;

    MenuItem syncItem;
    Boolean refreshRetailersInView = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_retailers);

        setActivityView(R.layout.activity_retailers);
        setActivityTitle("Retailer List");

        syncAccount = Motley.CreateSyncAccount(mContext);

        if(Storage.is_session(mContext)) {
            retailerMobileAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.retailer_mobile);

            retailerMobileAutoCompleteTextView.setThreshold(Storage.FILTER_LIST_THRESHOLD);
            retailerMobileAutoCompleteTextView.setAdapter(new NameMobileAutoCompleteAdapter(mContext));

            mRetailersDataSource = new RetailersDataSource(mContext);
            mRetailersDataSource.open();
            if (Storage.user_group(mContext) == Storage.DISTRIBUTOR)
                retailers = mRetailersDataSource.get_many_by(Storage.getPersistentString(mContext,
                        Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
            else
                retailers = mRetailersDataSource.get_many_by_salesman(Storage.getPersistentString(mContext,
                        Storage.SHAREDPREFERENCE_SALESMAN_ID));
            mRetailersDataSource.close();

            mRetailersAdapter = new RetailersAdapter(mContext, retailers, listType);
            retailersListView = (ListView) findViewById(R.id.retailers_list_view);
            retailersListView.setAdapter(mRetailersAdapter);

            retailerMobileAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                    retailer = (RetailersDataSource) adapterView.getItemAtPosition(position);
                    retailerMobileAutoCompleteTextView.setText(retailer.mobile);
                    mRetailersAdapter.clear();
                    mRetailersAdapter.addAll(new ArrayList<RetailersDataSource>() {
                        {
                            add(retailer);
                        }
                    });
                    mRetailersAdapter.notifyDataSetChanged();
                    refreshRetailersInView = false;
                }
            });

            retailerMobileAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
                public void afterTextChanged(Editable S) {

                }

                public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                              int arg3) {

                }

                public void onTextChanged(CharSequence s, int start, int before,
                                          int count) {
                    if(retailerMobileAutoCompleteTextView.length() == 0){
                        mRetailersAdapter.clear();
                        mRetailersDataSource.open();
                        if (Storage.user_group(mContext) == Storage.DISTRIBUTOR)
                            retailers = mRetailersDataSource.get_many_by(Storage.getPersistentString(mContext,
                                    Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
                        else
                            retailers = mRetailersDataSource.get_many_by_salesman(Storage.getPersistentString(mContext,
                                    Storage.SHAREDPREFERENCE_SALESMAN_ID));
                        mRetailersDataSource.close();
                        mRetailersAdapter.addAll(retailers);
                        mRetailersAdapter.notifyDataSetChanged();
                        refreshRetailersInView = true;
                    }
                }
            });
        }
        else {
            Intent intent = new Intent(mContext, LoginActivity.class);
            intent.putExtra("ACTION", "FINISH_LOGIN");
            ((Activity) mContext).startActivityForResult(intent, Storage.REQUEST_LOGIN_SESSION);
        }
        observer = new SyncStatusObserver(){
            @Override
            public void onStatusChanged(int which){
                runOnUiThread(new Runnable(){
                    @Override
                    public void run(){
                        Account account = syncAccount;
                        isSynchronizing =
                                isSyncActive(account, Storage.AUTHORITY);
                        if(syncItem != null) {
                            if (isSynchronizing) {
                                syncItem.setActionView(R.layout.circular_progress);
                            } else
                                syncItem.setActionView(null);
                        }
                    }
                });
            }
        };
    }

    @Override
    protected void onResume(){
        super.onResume();

        // Refresh synchronization status
        observer.onStatusChanged(0);

        // Watch for synchronization status changes
        final int mask = ContentResolver.SYNC_OBSERVER_TYPE_PENDING |
                ContentResolver.SYNC_OBSERVER_TYPE_ACTIVE;
        syncHandle = ContentResolver.addStatusChangeListener(mask, observer);

        mContext.registerReceiver(mRetailersReceiver, new IntentFilter("last_100_retailers"));
        mContext.registerReceiver(mSyncStatusReceiver, new IntentFilter("sync_running"));
    }

    @Override
    protected void onPause(){
        super.onPause();

        // Remove our synchronization listener if registered
        if (syncHandle != null){
            ContentResolver.removeStatusChangeListener(syncHandle);
            syncHandle = null;
        }
        mContext.unregisterReceiver(mRetailersReceiver);
        mContext.unregisterReceiver(mSyncStatusReceiver);
    }

    private BroadcastReceiver mRetailersReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals("last_100_retailers") && retailers != null && refreshRetailersInView){
                RetailersDataSource lastRetailer = null;
                if(retailers.size() > 0)
                    lastRetailer = retailers.get(retailers.size() - 1);

                mRetailersDataSource.open();
                ArrayList<RetailersDataSource> last100Retailers;
                if(lastRetailer != null)
                    last100Retailers = mRetailersDataSource.getNext100Retailers(lastRetailer.id);
                else
                    last100Retailers = mRetailersDataSource.getNext100Retailers(null);
                mRetailersDataSource.close();
                retailers.addAll(last100Retailers);
//                retailers = uniqueArrayList(retailers);
                mRetailersAdapter.notifyDataSetChanged();

                if(countMenuItem != null){
                    TextView countTextView = (TextView) countMenuItem.getActionView().findViewById(R.id.retailers_count);
                    countTextView.setText("" + retailers.size());
                    ((Activity) mContext).invalidateOptionsMenu();
                }
            }
        }
    };

    ArrayList<RetailersDataSource> uniqueArrayList(ArrayList<RetailersDataSource> retailers){
        LinkedHashMap retailersMap = new LinkedHashMap<String, RetailersDataSource>();
        for(RetailersDataSource retailer : retailers){
            retailersMap.put(retailer.id, retailer);
        }
        ArrayList<RetailersDataSource> uniqueRetailers = new ArrayList<RetailersDataSource>(retailersMap.values());
//        Iterator it = retailersMap.entrySet().iterator();
//        while (it.hasNext()) {
//            Map.Entry pair = (Map.Entry) it.next();
//            uniqueRetailers.add((RetailersDataSource) pair.getValue());
//        }

        return uniqueRetailers;
    }

    private BroadcastReceiver mSyncStatusReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(action.equals("sync_running") && syncItem != null){
                Boolean isSyncRunning = intent.getBooleanExtra("is_sync_running", false);
                if(isSyncRunning){
                    syncItem.setActionView(R.layout.circular_progress);
                }
                else
                    syncItem.setActionView(null);
            }
        }
    };

    private static boolean isSyncActive(Account account, String authority){
        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB){
            return isSyncActiveHoneycomb(account, authority);
        } else{
            SyncInfo currentSync = ContentResolver.getCurrentSync();
            return currentSync != null && currentSync.account.equals(account)
                    && currentSync.authority.equals(authority);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    private static boolean isSyncActiveHoneycomb(Account account,
                                                 String authority){
        for(SyncInfo syncInfo : ContentResolver.getCurrentSyncs())
        {
            if(syncInfo.account.equals(account) &&
                    syncInfo.authority.equals(authority)){
                return true;
            }
        }
        return false;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        syncItem = menu.findItem(R.id.action_sync);

        syncItem.setVisible(true);
        syncItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Motley.sync(syncAccount);
                Toast.makeText(mContext, "Syncing Retailers",
                        Toast.LENGTH_LONG).show();
                return false;
            }
        });

        if(retailers != null) {
            countMenuItem = menu.findItem(R.id.action_count);
            countMenuItem.setVisible(true);
            countMenuItem.setActionView(R.layout.action_bar_count);
            TextView countTextView = (TextView) countMenuItem.getActionView().findViewById(R.id.retailers_count);
            countTextView.setText("" + retailers.size());
            ((Activity) mContext).invalidateOptionsMenu();
        }

        if(Storage.getSyncPreferencesBoolean(mContext, Storage.IS_SYNC_RUNNING)){
            syncItem.setActionView(R.layout.circular_progress);
        }
        else
            syncItem.setActionView(null);

        return true;
    }
}
