package com.mindsarray.pay1distributor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Response;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1distributor.adapters.CreateRetailerPagerAdapter;
import com.mindsarray.pay1distributor.contenthelpers.NotificationsDataSource;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.models.Document;
import com.mindsarray.pay1distributor.util.DocumentButton;
import com.mindsarray.pay1distributor.util.GooglePlayServicesApiClient;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;


public class CreateRetailerActivity extends BaseActivity
        implements CreateRetailerFormOneFragment.OnFragmentInteractionListener,
        CreateRetailerFormTwoFragment.OnFragmentInteractionListener {

    private GoogleApiClient mGoogleApiClient;
    CreateRetailerPagerAdapter mCreateRetailerPagerAdapter;
    ViewPager mViewPager;
    public Context mContext = CreateRetailerActivity.this;
    public GooglePlayServicesApiClient mGooglePlayServicesApiClient;

    private EditText mNameView;
    private EditText mMobileView;
    private EditText mShopView;
    private Spinner mShopTypeView;
    private Spinner mLocationTypeView;
    private Spinner mShopStructureView;
    private EditText mAddressView;
    private EditText mAreaView;
    private EditText mCityView;
    private EditText mStateView;
    private EditText mPinCodeView;
    private EditText mOtherEditText;
    private TextView mSelectedNatureOfBusiness;
    private TextView mSelectedAreaOfBusiness;
    private RadioGroup mGroupNatureOfBusinessView;
    private RadioGroup mGroupAreaOfBusinessView;
    private RadioGroup mGroupRentalTypeView;
    private RadioButton mNatureOfBusinessView;
    private RadioButton mAreaOfBusinessView;
    private RadioButton mRentalTypeView;

    public ArrayList<String> addressProof = new ArrayList<String>();
    public ArrayList<String> idProof = new ArrayList<String>();
    public ArrayList<String> shopPhotos = new ArrayList<String>();
    public ArrayList<String> mImageURIRemovalList = new ArrayList<String>();

    private String retailer_id = null;
    private RetailersDataSource mRetailersDataSource;
    public RetailersDataSource retailer;

    private CreateRetailerFormOneFragment mCreateRetailerFormOneFragment;
    private CreateRetailerFormTwoFragment mCreateRetailerFormTwoFragment;

    private Boolean new_trial = false;

    String natureOfBusiness = "", areaOfBusiness = "";
    String kyc_action = "";

    DocumentButton mUploadIDProofdocumentButton, mUploadAddressProofdocumentButton, mUploadShopPhotosdocumentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_retailer);

        setActivityView(R.layout.activity_create_retailer);
        setActivityTitle("Create Retailer");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            retailer_id = extras.getString("RETAILER_ID");
            new_trial = extras.getBoolean("NEW_TRIAL");
        }

        mRetailersDataSource = new RetailersDataSource(mContext);

        Motley.promptForGPS((Activity) mContext);
        mGooglePlayServicesApiClient = new GooglePlayServicesApiClient(mContext, LocationServices.API, false);
        mGoogleApiClient = mGooglePlayServicesApiClient.mGoogleApiClient;
        Log.e("Google API Client: ", String.valueOf(mGoogleApiClient));

        mCreateRetailerPagerAdapter =
                new CreateRetailerPagerAdapter(
                        getSupportFragmentManager());
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setAdapter(mCreateRetailerPagerAdapter);

        mRetailersDataSource = new RetailersDataSource(mContext);

        if(edit()){
            setActivityTitle("Edit Retailer");
            getRetailer();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        Motley.promptForGPS((Activity) mContext);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        Log.e("Activity: ", "Inside");
    }

    public void onFragmentInteraction(Uri Uri){

    }

    public Fragment findFragmentByPosition(int position) {
        return getSupportFragmentManager().findFragmentByTag(
                "android:switcher:" + mViewPager.getId() + ":"
                        + mCreateRetailerPagerAdapter.getItemId(position));
    }

    public void createRetailer(){
        mNameView = (EditText) findViewById(R.id.retailer_name);
        mMobileView = (EditText) findViewById(R.id.retailer_mobile);
        mShopView = (EditText) findViewById(R.id.retailer_shop_name);
//        mRentalTypeView = (Spinner) findViewById(R.id.rental_type_spinner);
        mAddressView = (EditText) findViewById(R.id.retailer_address);
        mAreaView = (EditText) findViewById(R.id.retailer_area);
        mCityView = (EditText) findViewById(R.id.retailer_city);
        mStateView = (EditText) findViewById(R.id.retailer_state);
        mPinCodeView = (EditText) findViewById(R.id.retailer_pin_code);

        mGroupRentalTypeView = (RadioGroup) findViewById(R.id.radio_group_rental_type);
        if(mGroupRentalTypeView.getCheckedRadioButtonId() != -1)
            mRentalTypeView = (RadioButton) mGroupRentalTypeView.findViewById(mGroupRentalTypeView.getCheckedRadioButtonId());

        RelativeLayout natureOfBusinessButtons = (RelativeLayout) findViewById(R.id.natureOfBusinessButtons);
        mOtherEditText = (EditText) natureOfBusinessButtons.findViewById(R.id.other_text);
        for(int i = 0; i < natureOfBusinessButtons.getChildCount() - 1; i++) {
            Button child = (Button) natureOfBusinessButtons.getChildAt(i);
            if(child.isSelected()){
                natureOfBusiness = child.getText().toString();
                if(i == natureOfBusinessButtons.getChildCount() - 2){
                    natureOfBusiness = mOtherEditText.getText().toString();
                }
            }
        }

        RelativeLayout areaOfBusinessButtons = (RelativeLayout) findViewById(R.id.areaOfBusinessButtons);
        for(int i = 0; i < areaOfBusinessButtons.getChildCount(); i++) {
            Button child = (Button) areaOfBusinessButtons.getChildAt(i);
            if(child.isSelected()) {
                areaOfBusiness = child.getText().toString();
            }
        }

        if(validateForm()){
            mCreateRetailerFormOneFragment = (CreateRetailerFormOneFragment) findFragmentByPosition(0);
            mCreateRetailerFormTwoFragment = (CreateRetailerFormTwoFragment) findFragmentByPosition(1);

            final ProgressDialog progressDialog = new ProgressDialog(mContext);
            final Response.Listener responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Multipart Response: ", " " + response);

                    try{
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        String status = jsonObjectResponse.getString("status");

                        Log.e("Status: ", status);
                        if (status.equalsIgnoreCase("success")) {
                            JSONObject description = jsonObjectResponse.getJSONObject("description");
                            JSONObject jsonRetailer = description.getJSONObject("retailer");
                            mRetailersDataSource.open();
                            retailer = mRetailersDataSource.save(jsonRetailer);
                            mRetailersDataSource.close();

                            uploadRetailerDocuments(retailer);
                            if(!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty() || !mImageURIRemovalList.isEmpty() || !edit()) {
                                String message = "Thank you!";
                                String message2 = "This account is pending verification.";
                                Runnable confirmBlock = new Runnable() {
                                    @Override
                                    public void run() {
                                        progressDialog.dismiss();
                                    }
                                };

//                                Motley.showOneButtonBigDialog(mContext, message, message2, "SUCCESS", "OK", R.drawable.ic_success, confirmBlock);
                            }
                            else {
//                                uploadRetailerDocuments(retailer);
                            }
                        }
                        else
                            Motley.showOneButtonDialog(mContext, jsonObjectResponse.getString("description"), "FAILED", "OK");
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                    finally {
                        progressDialog.dismiss();
                    }
                }
            };
            final Runnable errorBlock = new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            };

            final Runnable executeBlock = new Runnable() {
                @Override
                public void run() {
                    progressDialog.show(mContext, "", "Updating Retailer");
                    NetworkConnection.getInstance(mContext).handleRequest(createRetailerParams(), responseListener, errorBlock);
                }
            };

            if (!mUploadIDProofdocumentButton.getPresent() || !mUploadAddressProofdocumentButton.getPresent() || !mUploadShopPhotosdocumentButton.getPresent()){
                Motley.showTwoButtonDialogForUpload(mContext, "Upload KYC documents and enjoy Toll-Free service.", "PAY1", "NOW", "LATER", null, executeBlock);
            }
            else
                executeBlock.run();
        }
    }

    private boolean validateForm(){
        String DIALOG_TITLE_INVALIDATE = "Incomplete form";
        mCreateRetailerFormOneFragment = (CreateRetailerFormOneFragment) findFragmentByPosition(0);
        mCreateRetailerFormTwoFragment = (CreateRetailerFormTwoFragment) findFragmentByPosition(1);

        if(mNameView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper name", DIALOG_TITLE_INVALIDATE, "OK");
            mNameView.requestFocus();
            return false;
        }
        else if(!Motley.isMobileValid(mMobileView.getText().toString())){
            Motley.showOneButtonDialog(mContext, "Enter a proper mobile number", DIALOG_TITLE_INVALIDATE, "OK");
            mMobileView.requestFocus();
            return false;
        }
        else if(mShopView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper shop name", DIALOG_TITLE_INVALIDATE, "OK");
            mShopView.requestFocus();
            return false;
        }
        else if(mAddressView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper address", DIALOG_TITLE_INVALIDATE, "OK");
            mAddressView.requestFocus();
            return false;
        }
        else if(mAreaView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper Area", DIALOG_TITLE_INVALIDATE, "OK");
            mAreaView.requestFocus();
            return false;
        }
        else if(mCityView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper City", DIALOG_TITLE_INVALIDATE, "OK");
            mCityView.requestFocus();
            return false;
        }
        else if(mStateView.getText().toString().length() < 2){
            Motley.showOneButtonDialog(mContext, "Enter a proper State", DIALOG_TITLE_INVALIDATE, "OK");
            mStateView.requestFocus();
            return false;
        }
        else if(mPinCodeView.getText().toString().length() != 6){
            Motley.showOneButtonDialog(mContext, "Pin Code should be 6 digits long", DIALOG_TITLE_INVALIDATE, "OK");
            mPinCodeView.requestFocus();
            return false;
        }
        else if(natureOfBusiness.equals("")){
            Motley.showOneButtonDialog(mContext, "Select the nature of your business", DIALOG_TITLE_INVALIDATE, "OK");
            return false;
        }
        else if(areaOfBusiness.equals("")){
            Motley.showOneButtonDialog(mContext, "Select the area of your business", DIALOG_TITLE_INVALIDATE, "OK");
            return false;
        }
        else if(Storage.SHAREDPREFERENCE_MOBILE_NUMBER.equals("")){
            startActivity(new Intent(mContext,
                    LoginActivity.class));
            return false;
        }
        else if(mCreateRetailerFormOneFragment.latitude == null || mCreateRetailerFormOneFragment.longitude == null){
            Motley.showOneButtonDialog(mContext, "Select shop location on map", DIALOG_TITLE_INVALIDATE, "OK");
            return false;
        }

        return true;
    }

    private Map createRetailerParams(){
        final Map createRetailerParams = new HashMap();

        if(edit()) {
            createRetailerParams.put("method", "editRetailer");
            createRetailerParams.put("r_id", retailer.retailer_id);
        }
        else {
            createRetailerParams.put("method", "createRetailer");
        }

        createRetailerParams.put("interest", "Retailer");
        createRetailerParams.put("r_m", mMobileView.getText().toString());
        createRetailerParams.put("r_n", mNameView.getText().toString());
        createRetailerParams.put("s_n", mShopView.getText().toString());
        createRetailerParams.put("d_uid", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_USER_ID));

        if(Storage.getPersistentInt(mContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID) == Storage.SALESMAN) {
            createRetailerParams.put("d_id", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID));
        }
        else {
            createRetailerParams.put("d_id", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
        }

        createRetailerParams.put("s_t", natureOfBusiness + "");

        createRetailerParams.put("l_t", areaOfBusiness + "");

        if(mRentalTypeView.getText().toString().equals("Free"))
            createRetailerParams.put("r_t", "1");
        else
            createRetailerParams.put("r_t", "0");
        createRetailerParams.put("r_add", mAddressView.getText().toString());
        createRetailerParams.put("r_a", mAreaView.getText().toString());
        createRetailerParams.put("r_c", mCityView.getText().toString());
        createRetailerParams.put("r_s", mStateView.getText().toString());
        createRetailerParams.put("r_pc", mPinCodeView.getText().toString());
        createRetailerParams.put("r_la", "" + mCreateRetailerFormOneFragment.latitude);
        createRetailerParams.put("r_lo", "" + mCreateRetailerFormOneFragment.longitude);

        addressProof.addAll(mCreateRetailerFormTwoFragment.getAddressProofURIs());
        idProof.addAll(mCreateRetailerFormTwoFragment.getIdProofURIs());
        shopPhotos.addAll(mCreateRetailerFormTwoFragment.getShopPhotosURIs());
        mImageURIRemovalList.addAll(mCreateRetailerFormTwoFragment.getPhotoRemovalURIs());

        return createRetailerParams;
    }

    public void uploadRetailerDocuments(RetailersDataSource retailer){
        final Map uploadDocumentsParams = new HashMap();

        uploadDocumentsParams.put("method", "uploadKYCDocuments");
        uploadDocumentsParams.put("r_id", retailer.retailer_id);
        uploadDocumentsParams.put("ADDRESS_PROOF[]", addressProof);
        uploadDocumentsParams.put("PAN_CARD[]", idProof);
        uploadDocumentsParams.put("SHOP_PHOTO[]", shopPhotos);
//        uploadDocumentsParams.put("remove[]", mImageURIRemovalList);
        for(int i = 0; i < mImageURIRemovalList.size(); i++){
            uploadDocumentsParams.put("remove[ "+ i + "]", mImageURIRemovalList.get(i));
        }

        if(!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty() || !mImageURIRemovalList.isEmpty()) {
            Response.Listener responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Multipart Response: ", " " + response);

                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        String status = jsonObjectResponse.getString("status");

                        Log.e("Status: ", status);
                        if (status.equalsIgnoreCase("success")) {
                            JSONObject description = jsonObjectResponse.getJSONObject("description");

                            NotificationsDataSource mNotificationsDataSource = new NotificationsDataSource(mContext);
                            if(!idProof.isEmpty()){
                                if(description.getJSONArray("PAN_CARD") != null){
                                    mNotificationsDataSource.open();
                                    if(description.getJSONArray("PAN_CARD").getJSONObject(0).getString("status").equals("success")){
                                        mNotificationsDataSource.save("PAN card uploaded successfully", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "PAN card uploaded successfully");
                                    }
                                    else {
                                        mNotificationsDataSource.save("PAN card uploaded failed", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "PAN card uploaded failed");
                                    }
                                    mNotificationsDataSource.close();
                                    Intent intent = new Intent("last_notification");
                                    mContext.sendBroadcast(intent);
                                }
                            }
                            if(!addressProof.isEmpty()){
                                if(description.getJSONArray("ADDRESS_PROOF") != null){
                                    mNotificationsDataSource.open();
                                    if(description.getJSONArray("ADDRESS_PROOF").getJSONObject(0).getString("status").equals("success")){
                                        mNotificationsDataSource.save("Address Proof uploaded successfully", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "Address Proof uploaded successfully");
                                    }
                                    else {
                                        mNotificationsDataSource.save("Address Proof uploaded failed", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "Address Proof uploaded failed");
                                    }
                                    mNotificationsDataSource.close();
                                    Intent intent = new Intent("last_notification");
                                    mContext.sendBroadcast(intent);
                                }
                            }
                            if(!shopPhotos.isEmpty()){
                                if(description.getJSONArray("SHOP_PHOTO") != null){
                                    mNotificationsDataSource.open();
                                    if(description.getJSONArray("SHOP_PHOTO").getJSONObject(0).getString("status").equals("success")){
                                        mNotificationsDataSource.save("Shop photos uploaded successfully", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "Shop photos uploaded successfully");
                                    }
                                    else {
                                        mNotificationsDataSource.save("Shop photos uploaded failed", "Upload", System.currentTimeMillis() + "");
                                        Motley.notify(mContext, "Pay1", "Shop photos uploaded failed");
                                    }
                                    mNotificationsDataSource.close();
                                    Intent intent = new Intent("last_notification");
                                    mContext.sendBroadcast(intent);
                                }
                            }
                        }
                        else {
                            Toast.makeText(mContext, "Image upload failed",
                                    Toast.LENGTH_LONG).show();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
            Runnable errorBlock = new Runnable() {
                @Override
                public void run() {
                    NotificationsDataSource mNotificationsDataSource = new NotificationsDataSource(mContext);
                    mNotificationsDataSource.open();
                    mNotificationsDataSource.save("Some error occurred. Photos could not be uploaded. Check your network bandwidth.", "Upload", System.currentTimeMillis() + "");
                    Motley.notify(mContext, "Pay1", "Some error occurred. Photos could not be uploaded. Check your network bandwidth.");
                    mNotificationsDataSource.close();
                    Intent intent = new Intent("last_notification");
                    mContext.sendBroadcast(intent);
                }
            };
            NetworkConnection.getInstance(mContext).handleRequest(uploadDocumentsParams, responseListener, errorBlock);

            if(!addressProof.isEmpty() || !idProof.isEmpty() || !shopPhotos.isEmpty()){
                Toast.makeText(mContext, R.string.uploading_documents,
                        Toast.LENGTH_LONG).show();
            }
            else {
                Toast.makeText(mContext, R.string.removing_documents,
                        Toast.LENGTH_LONG).show();
            }
        }
        else {
            Toast.makeText(mContext, R.string.retailer_updated,
                    Toast.LENGTH_LONG).show();
        }
        finish();
        Intent dashboardIntent = new Intent(mContext,
                RetailersActivity.class);
        startActivity(dashboardIntent);
    }

    public boolean edit(){
        if(retailer_id != null){
            if(retailer == null) {
                mRetailersDataSource.open();
                retailer = mRetailersDataSource.get(retailer_id);
                mRetailersDataSource.close();
            }
            if(retailer.retailer_id == null)
                return false;
            else
                return true;
        }
        return false;
    }

    public void getRetailer(){
        final Map editRetailerParams = new HashMap();

        editRetailerParams.put("method", "getRetailer");
        editRetailerParams.put("r_id", retailer_id);

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Fetching Retailer..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Multipart Response: ", " " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject jsonRetailer = jsonObjectResponse.getJSONObject("description");

                        mRetailersDataSource.open();
                        retailer = mRetailersDataSource.save(jsonRetailer);
                        mRetailersDataSource.close();

                        showRetailerDetails();
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };

        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(editRetailerParams, responseListener, errorBlock);
    }

    void showRetailerDetails(){
        if(!retailer.retailer_id.equals(null)) {

            LinearLayout radioLayout = (LinearLayout) findViewById(R.id.radioLayout);
            mNameView = (EditText) findViewById(R.id.retailer_name);
            mMobileView = (EditText) findViewById(R.id.retailer_mobile);
            mShopView = (EditText) findViewById(R.id.retailer_shop_name);
//            mRentalTypeView = (Spinner) findViewById(R.id.rental_type_spinner);
            mAddressView = (EditText) findViewById(R.id.retailer_address);
            mAreaView = (EditText) findViewById(R.id.retailer_area);
            mCityView = (EditText) findViewById(R.id.retailer_city);
            mStateView = (EditText) findViewById(R.id.retailer_state);
            mPinCodeView = (EditText) findViewById(R.id.retailer_pin_code);

            mMobileView.setEnabled(false);
            mMobileView.setFocusable(false);

            String[] shopTypeArray = getResources().getStringArray(R.array.shop_type_array);
            String[] locationTypeArray = getResources().getStringArray(R.array.location_type_array);

            RelativeLayout natureOfBusinessButtons = (RelativeLayout) findViewById(R.id.natureOfBusinessButtons);
            RelativeLayout areaOfBusinessButtons = (RelativeLayout) findViewById(R.id.areaOfBusinessButtons);

            mOtherEditText = (EditText) natureOfBusinessButtons.findViewById(R.id.other_text);

            mNameView.setText(retailer.name);
            mMobileView.setText(retailer.mobile);
            mShopView.setText(retailer.shop_name);

            mAddressView.setText(retailer.address);
            mAreaView.setText(retailer.area);
            mCityView.setText(retailer.city);
            mStateView.setText(retailer.state);
            mPinCodeView.setText(retailer.pin_code);

            if(!retailer.area.equals("")){
                mAreaView.setEnabled(false);
            }
            if(!retailer.city.equals("")){
                mCityView.setEnabled(false);
            }
            if(!retailer.state.equals("")){
                mStateView.setEnabled(false);
            }
            if(!retailer.pin_code.equals("")){
                mPinCodeView.setEnabled(false);
            }

            if(Arrays.asList(shopTypeArray).indexOf(retailer.shop_type) != -1) {
                (natureOfBusinessButtons.getChildAt(Arrays.asList(shopTypeArray).indexOf(retailer.shop_type))).setSelected(true);
                if(retailer.shop_type.equals("Others")){
                    mOtherEditText.setVisibility(View.VISIBLE);
                }
            }
            else {
                if(!retailer.shop_type.equals("0"))
                    mOtherEditText.setText(retailer.shop_type + "");
                mOtherEditText.setVisibility(View.VISIBLE);
                if(!retailer.shop_type.equals("") && !retailer.shop_type.equals("0")) {
                    (natureOfBusinessButtons.getChildAt(natureOfBusinessButtons.getChildCount() - 2)).setSelected(true);
                }
            }

            if(Arrays.asList(locationTypeArray).indexOf(retailer.location_type) != -1) {
                (areaOfBusinessButtons.getChildAt(Arrays.asList(locationTypeArray).indexOf(retailer.location_type))).setSelected(true);
            }

            mGroupRentalTypeView = (RadioGroup) findViewById(R.id.radio_group_rental_type);
            if(retailer.rental_flag.equals("0")){
//                radioLayout.setVisibility(View.VISIBLE);
                ((RadioButton) mGroupRentalTypeView.getChildAt(1)).setChecked(true);
            }else{
//                radioLayout.setVisibility(View.VISIBLE);
                ((RadioButton) mGroupRentalTypeView.getChildAt(0)).setChecked(true);
            }

            mCreateRetailerFormOneFragment = (CreateRetailerFormOneFragment) findFragmentByPosition(0);
            mCreateRetailerFormTwoFragment = (CreateRetailerFormTwoFragment) findFragmentByPosition(1);

            mCreateRetailerFormTwoFragment.loadImages();

            if(retailer.latitude.equals("null") || retailer.longitude.equals("null") || retailer.latitude.equals("0") || retailer.longitude.equals("0") || retailer.latitude.equals("") || retailer.longitude.equals("")) {

            }
            else {
                mCreateRetailerFormOneFragment.setLatLng();
            }

            Map documents = retailer.documents();
            mUploadIDProofdocumentButton = (DocumentButton) findViewById(R.id.uploadIDProofDialogButton);
            mUploadAddressProofdocumentButton = (DocumentButton) findViewById(R.id.uploadAddressProofDialogButton);
            mUploadShopPhotosdocumentButton = (DocumentButton) findViewById(R.id.uploadShopPhotosDialogButton);

            if(((ArrayList<Document>) documents.get("idProof")).size() > 0){
                mUploadIDProofdocumentButton.setPresent(true);
                if(((ArrayList<Document>) documents.get("idProof")).get(0).verify_flag.equals("1")){
                    mUploadIDProofdocumentButton.setVerified(true);
                }else  if(((ArrayList<Document>) documents.get("idProof")).get(0).verify_flag.equals("-1")){
                    mUploadIDProofdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_pan));

                }
            }
            if(((ArrayList<Document>) documents.get("addressProof")).size() > 0){
                mUploadAddressProofdocumentButton.setPresent(true);
                if(((ArrayList<Document>) documents.get("addressProof")).get(0).verify_flag.equals("1")){
                    mUploadAddressProofdocumentButton.setVerified(true);
                }else  if(((ArrayList<Document>) documents.get("addressProof")).get(0).verify_flag.equals("-1")){
                    mUploadAddressProofdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_addproof));

                }
            }
            if(((ArrayList<Document>) documents.get("shopPhotos")).size() > 0){
                mUploadShopPhotosdocumentButton.setPresent(true);
                if(((ArrayList<Document>) documents.get("shopPhotos")).get(0).verify_flag.equals("1")){
                    mUploadShopPhotosdocumentButton.setVerified(true);
                }else  if(((ArrayList<Document>) documents.get("shopPhotos")).get(0).verify_flag.equals("-1")){
                    mUploadShopPhotosdocumentButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_reject_shoppic));

                }
            }

            Bundle extras = getIntent().getExtras();
            if (extras != null) {
                kyc_action = extras.getString("ACTION");
                if(kyc_action != null) {
                    mViewPager.setCurrentItem(1, true);
                    switch (kyc_action) {
                        case "ID_UPLOAD":
                            mUploadIDProofdocumentButton.performClick();
                            break;
                        case "ADDRESS_UPLOAD":
                            mUploadAddressProofdocumentButton.performClick();
                            break;
                        case "SHOP_PHOTOS_UPLOAD":
                            mUploadShopPhotosdocumentButton.performClick();
                            break;
                    }
                }
            }
        }
    }

    @Override
    public void onBackPressed() {
        String message = "Are you sure you want to discard current changes and exit?";

        Runnable confirmBlock = new Runnable() {
            @Override
            public void run() {
                finish();
            }
        };

        if(mDrawerLayout.isDrawerOpen(Gravity.RIGHT))
            mDrawerLayout.closeDrawer(Gravity.RIGHT);
        else
            Motley.showTwoButtonDialog(mContext, message, "PAY1", "YES", confirmBlock, null);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                String message = "Are you sure you want to discard current changes and exit?";

                Runnable confirmBlock = new Runnable() {
                    @Override
                    public void run() {
                        ((Activity) mContext).finish();
                    }
                };

                Motley.showTwoButtonDialog(mContext, message, "PAY1", "YES", confirmBlock, null);
                break;
        }
        return true;
    }
}