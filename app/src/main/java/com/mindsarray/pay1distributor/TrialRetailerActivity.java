package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class TrialRetailerActivity extends BaseActivity {

    Button createButton;
    TextView mShopNameView, mMobileView;

    Context mContext = TrialRetailerActivity.this;
    private RetailersDataSource mRetailersDataSource;
    public RetailersDataSource retailer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_retailer);

        setActivityView(R.layout.activity_trial_retailer);
        setActivityTitle("Create Retailer");

        mRetailersDataSource = new RetailersDataSource(mContext);

        createButton = (Button) findViewById(R.id.create_button);

        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateForm()){
                    createTrialRetailer();
                }
            }
        });

        mShopNameView = (EditText) findViewById(R.id.retailer_shop_name);
        mMobileView = (EditText) findViewById(R.id.retailer_mobile);
    }

    private boolean validateForm() {
        String DIALOG_TITLE_INVALIDATE = "Information improper";
        if (mShopNameView.getText().toString().length() < 2) {
            Motley.showOneButtonDialog(mContext, "Enter a proper name", DIALOG_TITLE_INVALIDATE, "OK");
            mShopNameView.requestFocus();
            return false;
        } else if (!Motley.isMobileValid(mMobileView.getText().toString())) {
            Motley.showOneButtonDialog(mContext, "Enter a proper mobile number", DIALOG_TITLE_INVALIDATE, "OK");
            mMobileView.requestFocus();
            return false;
        }

        return true;
    }

    public void createTrialRetailer(){
        String group_id = Storage.getSyncPreferencesString(mContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID);

        final Map createTrialRetailerParams = new HashMap();

        createTrialRetailerParams.put("method", "createTrialRetailer");
        createTrialRetailerParams.put("mobile", mMobileView.getText().toString());
        createTrialRetailerParams.put("shop_name", mShopNameView.getText().toString());
        if (group_id.equals("" + Storage.SALESMAN)) {
            createTrialRetailerParams.put("distributor_id", Storage.getSyncPreferencesString(mContext, Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID));
        } else {
            createTrialRetailerParams.put("distributor_id", Storage.getSyncPreferencesString(mContext, Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
        }

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Creating Retailer..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Multipart Response: ", " " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject description = jsonObjectResponse.getJSONObject("description");
                        JSONObject jsonRetailer = description.getJSONObject("retailer");
                        mRetailersDataSource.open();
                        retailer = mRetailersDataSource.save(jsonRetailer);
                        mRetailersDataSource.close();

                        Runnable nowBlock = new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                Intent intent = new Intent(mContext,
                                        CreateRetailerActivity.class);
                                intent.putExtra("RETAILER_ID", String.valueOf(retailer.retailer_id));
                                intent.putExtra("NEW_TRIAL", true);
                                mContext.startActivity(intent);
                            }
                        };
                        Runnable laterBlock = new Runnable() {
                            @Override
                            public void run() {
                                progressDialog.dismiss();
                                startActivity(new Intent(mContext,
                                        DashboardActivity.class));
                            }
                        };
                        String message = "Retailer successfully created.\n" +
                                "Do you wish to complete KYC now?";
                        String title = "Successful";
                        Motley.showTwoButtonBigDialog(mContext, message, title, "YES", "NO", R.drawable.ic_shakehand, nowBlock, laterBlock);
                    }
                    else
                        Motley.showOneButtonDialog(mContext, jsonObjectResponse.getString("description"), "FAILED", "OK");
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };

        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(createTrialRetailerParams, responseListener, errorBlock);
    }
}
