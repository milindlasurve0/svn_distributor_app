package com.mindsarray.pay1distributor;

import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ListView;

import com.mindsarray.pay1distributor.adapters.NotificationsAdapter;
import com.mindsarray.pay1distributor.contenthelpers.NotificationsDataSource;

import java.util.ArrayList;

public class NotificationsActivity extends BaseActivity {

    ListView notificationsListView;
    NotificationsDataSource mNotificationsDataSource;
    Context mContext = NotificationsActivity.this;
    NotificationsAdapter mNotificationsAdapter;
    ArrayList<NotificationsDataSource> notifications;

    int page = 1;
    int RESULTS_PER_PAGE = 10;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_retailer);

        setActivityView(R.layout.activity_notifications);
        setActivityTitle("Notifications");

        mNotificationsDataSource = new NotificationsDataSource(mContext);
        notificationsListView = (ListView) findViewById(R.id.notificationsListView);
        mNotificationsDataSource.open();
        notifications = mNotificationsDataSource.getPaginatedNotifications(page, RESULTS_PER_PAGE);
        mNotificationsDataSource.close();
        mNotificationsAdapter = new NotificationsAdapter(mContext, notifications);
        notificationsListView.setAdapter(mNotificationsAdapter);

        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        notificationManager.cancelAll();

        notificationsListView.setOnScrollListener(new AbsListView.OnScrollListener() {

            private boolean loading = true;
            private int previousTotal = 0;
            private int visibleThreshold = 6;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                        page++;
                    }
                }

                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    loading = true;
                    mNotificationsDataSource.open();
                    notifications = mNotificationsDataSource.getPaginatedNotifications(page, RESULTS_PER_PAGE);
                    mNotificationsDataSource.close();

                    mNotificationsAdapter.addAll(notifications);
                    mNotificationsAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancelAll();
            mNotificationsDataSource.open();
            NotificationsDataSource notification = mNotificationsDataSource.getLastNotification();
            mNotificationsDataSource.close();
            mNotificationsAdapter.insert(notification, 0);
            mNotificationsAdapter.notifyDataSetChanged();
        }
    };

    @Override
    protected void onPause() {
        super.onPause();
        mContext.unregisterReceiver(mMessageReceiver);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mContext.registerReceiver(mMessageReceiver, new IntentFilter("last_notification"));
    }
}
