package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.models.Document;
import com.mindsarray.pay1distributor.util.DocumentButton;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class KYCPromptActivity extends BaseActivity {

    Context mContext = KYCPromptActivity.this;
    String retailer_id;
    String popUpMessage;
    RetailersDataSource retailer;
    RetailersDataSource mRetailersDataSource;

    DocumentButton mUploadIDProofdocumentButton;
    DocumentButton mUploadAddressProofdocumentButton;
    DocumentButton mUploadShopPhotosdocumentButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_create_retailer);

        setActivityView(R.layout.activity_kyc_prompt);
        setActivityTitle("KYC Documents");

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            retailer_id = extras.getString("RETAILER_ID");
            popUpMessage = extras.getString("MESSAGE");
        }

        Log.e("Retailer id", "" + retailer_id);

        mRetailersDataSource = new RetailersDataSource(mContext);
        mRetailersDataSource.open();
        retailer = mRetailersDataSource.get(retailer_id);
        mRetailersDataSource.close();

        Button buttonNow = (Button) findViewById(R.id.buttonNow);
        Button buttonLater = (Button) findViewById(R.id.buttonLater);
        TextView reminder = (TextView) findViewById(R.id.reminder);

        long timeLeft = Motley.daysSince(mContext, retailer.created_on);

        reminder.setText(timeLeft + " Days  left!");

        buttonNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        CreateRetailerActivity.class);
                intent.putExtra("RETAILER_ID", retailer_id);
                intent.putExtra("ACTION", "KYC_UPLOAD");
                mContext.startActivity(intent);
            }
        });

        buttonLater.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        getRetailer();

        Motley.showOneButtonDialog(mContext, popUpMessage, "SUCCESS", "OK");
    }

    public void getRetailer(){
        final Map editRetailerParams = new HashMap();

        editRetailerParams.put("method", "getRetailer");
        editRetailerParams.put("r_id", retailer_id);

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Fetching KYC Status..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Multipart Response: ", " " + response);

                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject jsonRetailer = jsonObjectResponse.getJSONObject("description");

                        mRetailersDataSource.open();
                        retailer = mRetailersDataSource.save(jsonRetailer);
                        mRetailersDataSource.close();

                        updateKYCStatus();
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };

        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(editRetailerParams, responseListener, errorBlock);
    }

    public void updateKYCStatus(){
        Map documents = retailer.documents();
        mUploadIDProofdocumentButton = (DocumentButton) findViewById(R.id.uploadIDProofDialogButton0);
        mUploadAddressProofdocumentButton = (DocumentButton) findViewById(R.id.uploadAddressProofDialogButton0);
        mUploadShopPhotosdocumentButton = (DocumentButton) findViewById(R.id.uploadShopPhotosDialogButton0);

        mUploadIDProofdocumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        CreateRetailerActivity.class);
                intent.putExtra("RETAILER_ID", retailer_id);
                intent.putExtra("ACTION", "ID_UPLOAD");
                mContext.startActivity(intent);
            }
        });

        mUploadAddressProofdocumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        CreateRetailerActivity.class);
                intent.putExtra("RETAILER_ID", retailer_id);
                intent.putExtra("ACTION", "ADDRESS_UPLOAD");
                mContext.startActivity(intent);
            }
        });

        mUploadShopPhotosdocumentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(mContext,
                        CreateRetailerActivity.class);
                intent.putExtra("RETAILER_ID", retailer_id);
                intent.putExtra("ACTION", "SHOP_PHOTOS_UPLOAD");
                mContext.startActivity(intent);
            }
        });

        if(((ArrayList<Document>) documents.get("idProof")).size() > 0){
            mUploadIDProofdocumentButton.setPresent(true);
            if(((ArrayList<Document>) documents.get("idProof")).get(0).verify_flag.equals("1")){
                mUploadIDProofdocumentButton.setVerified(true);
            }
        }
        if(((ArrayList<Document>) documents.get("addressProof")).size() > 0){
            mUploadAddressProofdocumentButton.setPresent(true);
            if(((ArrayList<Document>) documents.get("addressProof")).get(0).verify_flag.equals("1")){
                mUploadAddressProofdocumentButton.setVerified(true);
            }
        }
        if(((ArrayList<Document>) documents.get("shopPhotos")).size() > 0){
            mUploadShopPhotosdocumentButton.setPresent(true);
            if(((ArrayList<Document>) documents.get("shopPhotos")).get(0).verify_flag.equals("1")){
                mUploadShopPhotosdocumentButton.setVerified(true);
            }
        }
    }
}
