package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.android.volley.Response;
import com.google.android.gms.analytics.HitBuilders;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePinActivity extends BaseActivity {

    EditText pinView, newPinView,confirmPinView;
    Button changePin;
    Context mContext = ChangePinActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_change_pin);
        setActivityView(R.layout.activity_change_pin);
        setActivityTitle("Change Pin");

        pinView = (EditText) findViewById(R.id.pin);
        newPinView = (EditText) findViewById(R.id.new_pin);
        confirmPinView = (EditText) findViewById(R.id.confirm_pin);
        changePin = (Button) findViewById(R.id.change_pin);

        changePin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pinView.getText().toString().equals("")){
                    Motley.showOneButtonDialog(mContext, "Enter current pin", "VALIDATION ERROR", "OK");
                }
                else if(newPinView.getText().toString().equals("")){
                    Motley.showOneButtonDialog(mContext, "Enter your new pin", "VALIDATION ERROR", "OK");
                }
                else if(newPinView.getText().toString().length() < 4){
                    Motley.showOneButtonDialog(mContext, "Enter at least 4 characters", "VALIDATION ERROR", "OK");
                }
                else if(!newPinView.getText().toString().equals(confirmPinView.getText().toString())){
                    Motley.showOneButtonDialog(mContext, "Pins do not match", "VALIDATION ERROR", "OK");
                }
                else {
                    changePin();
                }
            }
        });
    }

    void changePin(){
        final Map changePinParams = new HashMap();
        changePinParams.put("method", "changePin");
        changePinParams.put("m", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_MOBILE_NUMBER));
        changePinParams.put("pin", pinView.getText().toString());
        changePinParams.put("new_pin", newPinView.getText().toString());

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Changing pin..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        Intent intent = new Intent(mContext, DashboardActivity.class);
                        finish();
                        startActivity(intent);
                    } else {
                        Motley.showOneButtonDialog(mContext, jsonObjectResponse.getString("description"), "FAILED", "OK");
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(changePinParams, responseListener, errorBlock);
    }
}
