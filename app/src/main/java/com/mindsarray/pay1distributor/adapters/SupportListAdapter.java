package com.mindsarray.pay1distributor.adapters;

/**
 * Created by rohan on 21/8/15.
 */
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;

public class SupportListAdapter extends BaseAdapter {

    Context mContext;
    String[] mTextArray;
    int[] mIconArray;

    public SupportListAdapter(Context context, String[] textArray, int[] iconArray) {
        mContext = context;
        mTextArray = textArray;
        mIconArray = iconArray;
    }

    @Override
    public int getCount() {
        return mTextArray.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater
                    .inflate(R.layout.support_list_view_item, null);
            viewHolder = new ViewHolder();
            viewHolder.supportText = (TextView) convertView
                    .findViewById(R.id.supportText);
            viewHolder.supportImage = (ImageView) convertView
                    .findViewById(R.id.supportImage);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.supportText.setText(mTextArray[position]);
        viewHolder.supportImage.setImageResource(mIconArray[position]);

        return convertView;
    }

    private class ViewHolder {
        TextView supportText;
        ImageView supportImage;
    }

}