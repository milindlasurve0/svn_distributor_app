package com.mindsarray.pay1distributor.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.mindsarray.pay1distributor.CreateRetailerFormOneFragment;
import com.mindsarray.pay1distributor.CreateRetailerFormTwoFragment;

/**
 * Created by rohan on 21/5/15.
 */
public class CreateRetailerPagerAdapter extends FragmentPagerAdapter {

    static final int NUM_ITEMS = 2;
    CreateRetailerFormOneFragment mCreateRetailerFormOneFragment;
    CreateRetailerFormTwoFragment mCreateRetailerFormTwoFragment;

    public CreateRetailerPagerAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }
    private String tabtitles[] = new String[] { "Step 1", "Step 2" };

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                mCreateRetailerFormOneFragment = CreateRetailerFormOneFragment.newInstance(0);
                return mCreateRetailerFormOneFragment;
            case 1:
                mCreateRetailerFormTwoFragment = CreateRetailerFormTwoFragment.newInstance(1);
                return mCreateRetailerFormTwoFragment;
            default:
                return null;
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabtitles[position];
    }

    public void setCurrentItem (int item, boolean smoothScroll) {
        setCurrentItem(item, smoothScroll);
    }
}

