package com.mindsarray.pay1distributor.adapters;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.models.NavDrawerItem;

import java.util.ArrayList;

/**
 * Created by rohan on 14/7/15.
 */
public class NavDrawerListAdapter extends BaseAdapter {

    private Context context;
    private ArrayList<NavDrawerItem> navDrawerItems;

    public NavDrawerListAdapter(Context context, ArrayList<NavDrawerItem> navDrawerItems){
        this.context = context;
        this.navDrawerItems = navDrawerItems;
    }

    @Override
    public int getCount() {
        return navDrawerItems.size();
    }

    @Override
    public Object getItem(int position) {
        return navDrawerItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater)
                context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

        if (convertView == null) {
            if(position == 0) {
                convertView = mInflater.inflate(R.layout.drawer_list_item_0, null);
                ImageView imgIcon = (ImageView) convertView.findViewById(R.id.icon);
                imgIcon.setImageResource(navDrawerItems.get(position).getIcon());
            }
            else
                convertView = mInflater.inflate(R.layout.drawer_list_item, null);
        }

        TextView txtTitle = (TextView) convertView.findViewById(R.id.title);
        txtTitle.setText(navDrawerItems.get(position).getTitle());

        if(navDrawerItems.get(position).getTitle().equalsIgnoreCase("LOGOUT")){
            txtTitle.setTextColor(Color.parseColor("#7f7f7f"));
        }
        //        // displaying count
        //        // check whether it set visible or not
        //        if(navDrawerItems.get(position).getCounterVisibility()){
        //            txtCount.setText(navDrawerItems.get(position).getCount());
        //        }else{
        //            // hide the counter view
        //            txtCount.setVisibility(View.GONE);
        //        }

        return convertView;
    }

}