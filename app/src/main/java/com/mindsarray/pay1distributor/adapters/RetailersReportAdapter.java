package com.mindsarray.pay1distributor.adapters;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.models.BalanceReportItem;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohan on 1/6/15.
 */
public class RetailersReportAdapter extends ArrayAdapter<BalanceReportItem> {

    private Context rContext;
    private ArrayList<BalanceReportItem> mBalanceReportList;

    public RetailersReportAdapter(Context context, ArrayList<BalanceReportItem> balanceReportList) {
        super(context, 0, balanceReportList);
        rContext = context;
        mBalanceReportList = balanceReportList;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final BalanceReportItem balanceReportItem = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_retailers_report_list_item, parent, false);
        }

        TextView retailerName = (TextView) convertView.findViewById(R.id.retailer_name);
        TextView retailerMobile = (TextView) convertView.findViewById(R.id.retailer_mobile);
        TextView balanceAmount = (TextView) convertView.findViewById(R.id.balance_amount);
        LinearLayout retailerRow = (LinearLayout) convertView.findViewById(R.id.retailer_row);
        Button pullbackButton = (Button) convertView.findViewById(R.id.pullback_button);

        if(Storage.getPersistentInt(rContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID) == Storage.SALESMAN) {
            pullbackButton.setVisibility(View.GONE);
        }

        retailerName.setText(balanceReportItem.retailer_shop_name);
        retailerMobile.setText(balanceReportItem.retailer_mobile);
        balanceAmount.setText("Rs. " + balanceReportItem.amount.substring(0, balanceReportItem.amount.length() - 1));

        retailerRow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transactionDetail(balanceReportItem);
            }
        });

        pullbackButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Runnable confirmBlock = new Runnable() {
                    @Override
                    public void run() {
                        pullback(balanceReportItem, position);
                    }
                };
                String message = "Do you wish to pullback your last transaction? \n\n" + balanceReportItem.retailer_shop_name +
                        ": Rs. " + balanceReportItem.amount + "\n" + balanceReportItem.retailer_mobile;
                Motley.showTwoButtonDialog(rContext, message, null, "CONFIRM", confirmBlock, null);
            }
        });

        return convertView;
    }

    @Override
    public long getItemId(int position){
        BalanceReportItem balanceReportItem = getItem(position);
        return Long.parseLong(balanceReportItem.transaction_id);
    }

    public void pullback(final BalanceReportItem balanceReportItem, final int position){
        final Map pullbackParams = new HashMap();
        pullbackParams.put("method", "pullback");
        pullbackParams.put("salesman_transid", balanceReportItem.salesman_transaction_id);
        pullbackParams.put("shop_transid", balanceReportItem.transaction_id);
        pullbackParams.put("request_from", "distributorApp");

        final ProgressDialog progressDialog = ProgressDialog.show(rContext, "", "Pulling back amount..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        mBalanceReportList.remove(position);
                        notifyDataSetChanged();
                        Motley.showOneButtonDialog(rContext, "Done", "PULLBACK", "OK");
                        if(jsonObjectResponse.has("balance")) {
                            String balance = jsonObjectResponse.getString("balance");
                            Storage.setPersistentString(rContext, Storage.SHAREDPREFERENCE_BALANCE, balance);
                        }
                    }
                    else
                        Motley.showOneButtonDialog(rContext, jsonObjectResponse.getString("description"), "Failed", "OK");
                }
                catch(Exception e) {
                    e.printStackTrace();
                    Motley.showOneButtonDialog(rContext, "Some error occurred.", "TRY AGAIN", "OK");
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(rContext).handleRequest(pullbackParams, responseListener, errorBlock);
    }

    public void transactionDetail(final BalanceReportItem balanceReportItem){
        try {
            final Dialog dialog = new Dialog(rContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_transaction_detail);

            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            TextView transactionIdView = (TextView) dialog
                    .findViewById(R.id.transaction_id);
            TextView openingAmountView = (TextView) dialog
                    .findViewById(R.id.opening_amount);
            TextView closingAmountView = (TextView) dialog
                    .findViewById(R.id.closing_amount);
            TextView transactionDateView = (TextView) dialog
                    .findViewById(R.id.date);
            TextView transactionTimeView = (TextView) dialog
                    .findViewById(R.id.time);

            messageView.setText("NOTE: " + balanceReportItem.note);
            transactionIdView.setText(balanceReportItem.transaction_id);
            if(balanceReportItem.opening.equals("null"))
                openingAmountView.setText("");
            else
                openingAmountView.setText("Rs. " + balanceReportItem.opening);
            if(balanceReportItem.closing.equals("null"))
                closingAmountView.setText("");
            else
                closingAmountView.setText("Rs. " + balanceReportItem.closing);

            if(!balanceReportItem.time.equals("")) {
                transactionDateView.setText(balanceReportItem.time.substring(0, 10));
                transactionTimeView.setText(balanceReportItem.time.substring(balanceReportItem.time.length() - 8));
            }

            Button button = (Button) dialog
                    .findViewById(R.id.button);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}