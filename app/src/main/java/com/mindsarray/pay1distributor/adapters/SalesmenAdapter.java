package com.mindsarray.pay1distributor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.contenthelpers.SalesmenDataSource;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohan on 15/7/15.
 */
public class SalesmenAdapter extends ArrayAdapter<SalesmenDataSource> {

    private Context mContext;
    private List<SalesmenDataSource> salesmen = new ArrayList<SalesmenDataSource>();

    public SalesmenAdapter(Context context,
                           ArrayList<SalesmenDataSource> salesmen) {
        super(context, 0, salesmen);
        this.salesmen = salesmen;
        mContext = context;
    }

    @Override
    public int getCount() {
        return salesmen.size();
    }

    @Override
    public SalesmenDataSource getItem(int index) {
        return salesmen.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.salesman_list_item, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.salesman_name)).setText(getItem(position).name);
        ((TextView) convertView.findViewById(R.id.salesman_mobile)).setText(getItem(position).mobile);

        return convertView;
    }
}
