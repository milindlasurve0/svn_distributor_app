package com.mindsarray.pay1distributor.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.models.GridMenuItem;

import java.util.ArrayList;

/**
 * Created by rohan on 12/5/15.
 */
public class DashboardGridAdapter extends ArrayAdapter<GridMenuItem> {
    Context context;
    int layoutResourceId;
    ArrayList<GridMenuItem> data = new ArrayList<>();

    public DashboardGridAdapter(Context context, int layoutResourceId,
                               ArrayList<GridMenuItem> data) {
        super(context, layoutResourceId, data);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.data = data;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        RecordHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);

            holder = new RecordHolder();
            holder.txtTitle = (TextView) row.findViewById(R.id.item_text);
            holder.imageItem = (ImageView) row.findViewById(R.id.item_image);
            holder.count = (TextView) row.findViewById(R.id.count);

            row.setTag(holder);
        } else {
            holder = (RecordHolder) row.getTag();
        }

        GridMenuItem item = data.get(position);
        holder.txtTitle.setText(item.getTitle());

        holder.imageItem.setImageResource(item.getImage());
        if(position == 3){
            ViewGroup.LayoutParams params = holder.txtTitle.getLayoutParams();
            params.width = 180;
            holder.txtTitle.setLayoutParams(params);
        }
        else if(position == 4){
            if(item.getCount() > 0) {
                holder.count.setVisibility(View.VISIBLE);
                holder.count.setText("" + item.getCount());
            }
        }

        return row;
    }

    static class RecordHolder {
        TextView txtTitle;
        TextView count;
        ImageView imageItem;
    }
}
