package com.mindsarray.pay1distributor.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.contenthelpers.NotificationsDataSource;
import com.mindsarray.pay1distributor.util.Motley;

import java.util.ArrayList;

/**
 * Created by rohan on 31/10/15.
 */
public class NotificationsAdapter extends ArrayAdapter<NotificationsDataSource> {

    Context mContext;

    public NotificationsAdapter(Context context, ArrayList<NotificationsDataSource> notifications) {
        super(context, 0, notifications);
        mContext = context;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final NotificationsDataSource notification = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.adapter_notifications_list_item, parent, false);
        }

        LinearLayout notificationWrapper = (LinearLayout) convertView.findViewById(R.id.notificationWrapper);
        TextView notificationText = (TextView) convertView.findViewById(R.id.notificationText);
        TextView notificationTime = (TextView) convertView.findViewById(R.id.notificationTime);

        String dateTime = Motley.convertMillisToDateTime(notification.time);

        notificationText.setText(notification.data);
        notificationTime.setText(dateTime);

        if (position % 2 == 1) {
            notificationWrapper.setBackgroundColor(Color.WHITE);
        } else {
            notificationWrapper.setBackgroundColor(Color.parseColor("#F3F3F3"));
        }

        return convertView;
    }
}
