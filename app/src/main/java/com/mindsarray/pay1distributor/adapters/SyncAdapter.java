package com.mindsarray.pay1distributor.adapters;

/**
 * Created by rohan on 29/5/15.
 */

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.SyncResult;
import android.os.Bundle;
import android.util.Log;

import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

public class SyncAdapter extends AbstractThreadedSyncAdapter {

    private Context syncContext;
    ContentResolver mContentResolver;

    public SyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        syncContext = context;
        Log.e("Sync constructor1", "true");
        mContentResolver = context.getContentResolver();
        Motley.serverLog(context, "Sync Adapter Constructor", "Sync Adapter initialized before sync");
    }

    public SyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        syncContext = context;
        Log.e("Sync constructor2", "true");
        mContentResolver = context.getContentResolver();
    }

    @Override
    public void onPerformSync(
            Account account,
            Bundle extras,
            String authority,
            ContentProviderClient provider,
            SyncResult syncResult) {
        Log.e("SyncAdapter", "Starting..");
        Motley.serverLog(syncContext, "Sync Adapter onPerformSync", "Inside sync");
//        Storage.setSyncPreferencesBoolean(syncContext, Storage.IS_SYNC_RUNNING, true);
//        Intent startSyncIntent = new Intent("sync_running");
//        startSyncIntent.putExtra("is_sync_running", true);
//        Log.e("Sync running", "true");
//        syncContext.sendBroadcast(startSyncIntent);

        Motley.getRetailers(syncContext);

        Log.e("SyncAdapter", "Done");
    }

    @Override
    public void onSyncCanceled(){
//        Storage.setSyncPreferencesBoolean(syncContext, Storage.IS_SYNC_RUNNING, false);
//        Intent startSyncIntent = new Intent("sync_running");
//        startSyncIntent.putExtra("is_sync_running", false);
//        syncContext.sendBroadcast(startSyncIntent);
//        Log.e("Sync running", "false");
        Motley.serverLog(syncContext, "Sync Adapter onSyncCanceled", "Sync canceled");
    }
}