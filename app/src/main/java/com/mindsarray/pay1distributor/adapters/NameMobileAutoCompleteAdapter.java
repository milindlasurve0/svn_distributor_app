package com.mindsarray.pay1distributor.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.Storage;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rohan on 15/7/15.
 */
public class NameMobileAutoCompleteAdapter extends BaseAdapter implements Filterable {

    private Context mContext;
    private List<RetailersDataSource> resultList = new ArrayList<RetailersDataSource>();
    private RetailersDataSource mRetailersDataSource;
    List<RetailersDataSource> retailers;

    public NameMobileAutoCompleteAdapter(Context context) {
        mContext = context;
        mRetailersDataSource = new RetailersDataSource(context);
    }

    @Override
    public int getCount() {
        return resultList.size();
    }

    @Override
    public RetailersDataSource getItem(int index) {
        return resultList.get(index);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.dropdown_list_item_0, parent, false);
        }
        ((TextView) convertView.findViewById(R.id.retailer_mobile)).setText(getItem(position).mobile);
        ((TextView) convertView.findViewById(R.id.retailer_shop_name)).setText(getItem(position).shop_name);
        return convertView;
    }

    @Override
    public Filter getFilter() {
        Filter filter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if (constraint != null) {
                    try {
                        mRetailersDataSource.open();
                        retailers = new ArrayList<>();
                        if(Storage.is_session(mContext)) {
                            if (Storage.user_group(mContext) == Storage.DISTRIBUTOR) {
                                retailers = mRetailersDataSource.get_many_by(Storage.getPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID), constraint.toString());
                            } else {
                                retailers = mRetailersDataSource.get_many_by_salesman(Storage.getPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_ID), constraint.toString());
                            }
                        }
                        mRetailersDataSource.close();

                        filterResults.values = retailers;
                        filterResults.count = retailers.size();
                    }
                    catch(Exception e){
                        e.printStackTrace();
                    }
                }
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if (results != null && results.count > 0) {
                    resultList = (List<RetailersDataSource>) results.values;
                    notifyDataSetChanged();
                } else {
                    notifyDataSetInvalidated();
                }
            }};
        return filter;
    }
}
