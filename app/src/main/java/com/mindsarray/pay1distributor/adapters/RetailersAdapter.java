package com.mindsarray.pay1distributor.adapters;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.CreateRetailerActivity;
import com.mindsarray.pay1distributor.DashboardActivity;
import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by rohan on 1/6/15.
 */
public class RetailersAdapter extends ArrayAdapter<RetailersDataSource> {

    private Context rContext;
    private String rListType;

    public RetailersAdapter(Context context, ArrayList<RetailersDataSource> retailers, String  listType) {
        super(context, 0, retailers);
        rContext = context;
        rListType = listType;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RetailersDataSource retailer = getItem(position);

        int layoutResource = 0;
        if(rListType.equals("KYC_PENDING")){
            layoutResource = R.layout.adapter_kyc_pending_item;
        }
        else {
            layoutResource = R.layout.adapter_retailers_list_item;
        }
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(layoutResource, parent, false);
        }

        TextView retailerShopName = (TextView) convertView.findViewById(R.id.retailer_shop_name);
        TextView retailerMobile = (TextView) convertView.findViewById(R.id.retailer_mobile);
        LinearLayout retailerDetail = (LinearLayout) convertView.findViewById(R.id.retailer_row);
        ImageView retailerEdit = (ImageView) convertView.findViewById(R.id.retailer_edit);

        retailerShopName.setText(retailer.shop_name);
        retailerMobile.setText(retailer.mobile);

        if(rListType.equals("KYC_PENDING")) {
            TextView daysLeft = (TextView) convertView.findViewById(R.id.days_left);
            if (Storage.is_session(rContext)) {
                long timeLeft = Motley.daysSince(rContext, retailer.created_on);
                String trial_period = Storage.getPersistentString(rContext, Storage.SHAREDPREFERENCE_TRIAL_PERIOD);
                if(trial_period.equals("")){
                    Runnable confirmBlock = new Runnable() {
                        @Override
                        public void run() {
                            Motley.logout(rContext);
                        }
                    };
                    Runnable cancelBlock = new Runnable() {
                        @Override
                        public void run() {
                            ((Activity) rContext).finish();
                            rContext.startActivity(new Intent(rContext, DashboardActivity.class));
                        }
                    };
                    String message = "To use this feature you need to Login again. Select OK to Logout Now or Cancel for Later.";
                    Motley.showTwoButtonDialog(rContext, message,
                            "LOGOUT?", "OK", confirmBlock, cancelBlock);
                }
                else if (timeLeft < Long.valueOf(trial_period) && retailer.trial_flag.equals("1")) {
                    daysLeft.setText(timeLeft + " Days");
                    daysLeft.setTextColor(Color.RED);
                    retailerEdit.setImageResource(R.drawable.ic_edit);
                }
                else if (retailer.trial_flag.equals("2")) {
                    daysLeft.setText("INACTIVE");
                    daysLeft.setTextColor(Color.BLACK);
                    retailerEdit.setImageResource(R.drawable.ic_edit_red);
                }
                else {
                    daysLeft.setText("");
                    retailerEdit.setImageResource(R.drawable.ic_edit);
                }
            }
        }

        retailerDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getRetailerBalance(retailer);
            }
        });

        retailerEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(rContext,
                        CreateRetailerActivity.class);
                intent.putExtra("RETAILER_ID", String.valueOf(getItemId(position)));
                rContext.startActivity(intent);
            }
        });

        return convertView;
    }

    @Override
    public long getItemId(int position){
        RetailersDataSource retailer = getItem(position);
        return Long.parseLong(retailer.retailer_id);
    }

    public void getRetailerBalance(final RetailersDataSource retailer){
        final Map retailerBalanceParams = new HashMap();
        retailerBalanceParams.put("method", "balance");
        retailerBalanceParams.put("id", retailer.retailer_id);
        retailerBalanceParams.put("g_id", Storage.RETAILER + "");

        final ProgressDialog progressDialog = ProgressDialog.show(rContext, "", "Inquiring balance..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        String balance = jsonObjectResponse.getString("description");
                        String message = retailer.shop_name + ": " + retailer.mobile + "\n Current Balance is Rs. " + balance;
                        Motley.showOneButtonDialog(rContext, message, "CURRENT BALANCE", "OK");
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                    Motley.showOneButtonDialog(rContext, "Some error occurred.", "TRY AGAIN", "OK");
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(rContext).handleRequest(retailerBalanceParams, responseListener, errorBlock);
    }
}