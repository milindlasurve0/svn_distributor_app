package com.mindsarray.pay1distributor.adapters;

/**
 * Created by rohan on 22/8/15.
 */
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.models.BankDetailsPair;

public class BankDetailsAdapter extends BaseExpandableListAdapter {

    private Context mActivity;
    private ArrayList<BankDetailsPair> mListDataHeader;
    private ArrayList<ArrayList<BankDetailsPair>> mListChildData;
    protected static final String[] BANKS = new String[] { "ICICI", "BOM", "SBI", "UNION" };

    public BankDetailsAdapter(Context activity, ArrayList<BankDetailsPair> listDataHeader, ArrayList<ArrayList<BankDetailsPair>> listChildData){
        super();
        mActivity = activity;
        mListDataHeader = listDataHeader;
        mListChildData = listChildData;
    }

    public void add(ArrayList<BankDetailsPair> listDataHeader, ArrayList<ArrayList<BankDetailsPair>> listChildData){
        mListDataHeader = listDataHeader;
        mListChildData = listChildData;
        notifyDataSetChanged();
    }

    @Override
    public int getGroupCount() {
        if(mListDataHeader != null){
            return mListDataHeader.size();
        }
        else
            return 0;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        if(mListChildData != null){
            return mListChildData.get(groupPosition)
                    .size();
        }
        else {
            return 0;
        }
    }

    @Override
    public Object getGroup(int groupPosition) {
        if(mListDataHeader != null){
            return mListDataHeader.get(groupPosition);
        }
        else {
            return new BankDetailsPair("", "");
        }
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        if(mListChildData != null){
            return mListChildData.get(groupPosition).get(childPosition);
        }
        else {
            return new BankDetailsPair("", "");
        }
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        BankDetailsPair headerBankDetails = (BankDetailsPair) getGroup(groupPosition);
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.bank_details, null);
        }

        TextView bankDetailHeaderView = (TextView) convertView
                .findViewById(R.id.bankDetailHeader);
        TextView bankDetailSubHeaderView = (TextView) convertView
                .findViewById(R.id.bankDetailSubHeader);
        ImageView indicator = (ImageView) convertView.findViewById(R.id.indicatorView);

        bankDetailHeaderView.setText(headerBankDetails.key);
        bankDetailSubHeaderView.setText(headerBankDetails.value);

        if (isExpanded) {
            indicator.setImageResource(R.drawable.ic_up_arrow);
        } else {
            indicator.setImageResource(R.drawable.ic_downarrow);
        }
        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        BankDetailsPair bankDetailPair = (BankDetailsPair) getChild(groupPosition, childPosition);

        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mActivity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.bank_details_child_item, null);
        }

        TextView bankDetailPairKeyView = (TextView) convertView.findViewById(R.id.bankDetailPairKey);
        TextView bankDetailPairValueView = (TextView) convertView.findViewById(R.id.bankDetailPairValue);

        bankDetailPairKeyView.setText(bankDetailPair.key);
        bankDetailPairValueView.setText(bankDetailPair.value);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {

        return false;
    }

    public class BankDetailsPairAdapter extends ArrayAdapter<BankDetailsPair> {

        public BankDetailsPairAdapter(Activity activity, ArrayList<BankDetailsPair> detailPairList) {
            super(activity, 0, detailPairList);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            BankDetailsPair detailPair = getItem(position);

            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.bank_details_child_item, parent, false);
            }

            TextView bankDetailPairKeyView = (TextView) convertView.findViewById(R.id.bankDetailPairKey);
            TextView bankDetailPairValueView = (TextView) convertView.findViewById(R.id.bankDetailPairValue);

            bankDetailPairKeyView.setText(detailPair.key);
            bankDetailPairValueView.setText(detailPair.value);

            return convertView;
        }

    }


}