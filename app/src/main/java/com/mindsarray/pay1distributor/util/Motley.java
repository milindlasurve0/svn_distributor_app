package com.mindsarray.pay1distributor.util;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.net.wifi.WifiManager;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.android.volley.Response;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.mindsarray.pay1distributor.ChangePinActivity;
import com.mindsarray.pay1distributor.CreateRetailerActivity;
import com.mindsarray.pay1distributor.DashboardActivity;
import com.mindsarray.pay1distributor.LoginActivity;
import com.mindsarray.pay1distributor.NotificationsActivity;
import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.ReportBalanceTransferActivity;
import com.mindsarray.pay1distributor.ResetPinActivity;
import com.mindsarray.pay1distributor.RetailerBalanceTransferActivity;
import com.mindsarray.pay1distributor.RetailersActivity;
import com.mindsarray.pay1distributor.SendTopupRequestActivity;
import com.mindsarray.pay1distributor.SupportActivity;
import com.mindsarray.pay1distributor.TrialRetailerActivity;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * Created by rohan on 13/5/15.
 */
public class Motley {

    public static boolean displayPromptForEnablingGPS(final Activity context) {
        AlertDialog.Builder dialog;
        LocationManager lm = null;
        boolean gps_enabled = false;
        if (lm == null)
            lm = (LocationManager) context
                    .getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch (Exception ex) {
        }

        if (!gps_enabled) {
            dialog = new AlertDialog.Builder(context);
            dialog.setMessage("Location Service is off");
            dialog.setPositiveButton("On",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(
                                DialogInterface paramDialogInterface,
                                int paramInt) {
                            Intent myIntent = new Intent(
                                    Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                            context.startActivity(myIntent);
                            // get gps
                        }
                    });
            dialog.setNegativeButton("Off",
                    new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(
                                DialogInterface paramDialogInterface,
                                int paramInt) {
                        }
                    });
            dialog.show();
        }
        return gps_enabled;
    }

    public static void getLocationDialog(final Activity context){
        LocationManager lm;
        WifiManager wm;
        boolean gps_enabled = false;
        boolean network_enabled = false;
        boolean wifi_enabled = false;

        lm = (LocationManager) context
                .getSystemService(Context.LOCATION_SERVICE);
        wm = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);

        try {
            gps_enabled = lm
                    .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            network_enabled = lm
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        try {
            wifi_enabled = wm
                    .isWifiEnabled();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        Log.e("gps_enabled", "" + gps_enabled);
        String dialogText = "";

        if(!gps_enabled) {
            dialogText = "To get your Shop Location\n\nTurn on GPS";
            if (!network_enabled) {
                dialogText = "To get your Shop Location\n\nTurn on GPS and Mobile Network";
                if (!wifi_enabled) {
                    dialogText = "To get your Shop Location:\n\n1.Turn on GPS and Mobile Network\n2.Turn on Wi-Fi";
                }
            }
        }

        if(dialogText.length() > 0) {
            showTurnGPSOnDialog(context, dialogText);
        }
    }

    public static void showTurnGPSOnDialog(final Context context, String dialogText){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(
                context);
        dialog.setMessage(dialogText);
        dialog.setPositiveButton("On",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface paramDialogInterface,
                            int paramInt) {
                        Intent sIntent = new Intent(
                                Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        context.startActivity(sIntent);
                    }
                });
        dialog.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(
                            DialogInterface paramDialogInterface,
                            int paramInt) {

                    }
                });
        dialog.show();
    }

    public static void promptForGPS(final Activity activity){
        LocationManager lm;
        boolean gps_enabled = false;

        lm = (LocationManager) activity
                .getSystemService(Context.LOCATION_SERVICE);
        try {
            gps_enabled = lm
                    .isProviderEnabled(LocationManager.PASSIVE_PROVIDER);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if(!gps_enabled){
            showTurnGPSOnDialog(activity, "Turn GPS on to get accurate location");
        }
    }

    public static boolean checkPlayServices(Activity activity) {
        int resultCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(activity);
        if (resultCode != ConnectionResult.SUCCESS) {
            if (GooglePlayServicesUtil.isUserRecoverableError(resultCode)) {
                GooglePlayServicesUtil.getErrorDialog(resultCode, activity,
                        Storage.PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Log.i("Check Play Services", "This device is not supported.");
                activity.finish();
            }
            return false;
        }
        return true;
    }

    public static void showOneButtonDialog(Context context,
                                                  String message, String title, String buttonText) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_one_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            titleView.setText(title);
            Button button = (Button) dialog
                    .findViewById(R.id.button);
            button.setText(buttonText);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Dialog", e.getMessage());
        }
    }

    public static void showOneButtonDialog(Context context,
                                           String message, String title, String buttonText, final Runnable afterBlock) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_one_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            titleView.setText(title);
            Button button = (Button) dialog
                    .findViewById(R.id.button);
            button.setText(buttonText);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(afterBlock != null){
                        afterBlock.run();
                    }
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Dialog", e.getMessage());
        }
    }

    public static void showTwoButtonDialog(Context context,
                                           String message, String title, String buttonText,
                                           final Runnable confirmBlock, final Runnable cancelBlock) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            if(title == null)
                titleView.setVisibility(View.GONE);
            else
                titleView.setText(title);

            Button buttonConfirm = (Button) dialog
                    .findViewById(R.id.buttonConfirm);
            buttonConfirm.setText(buttonText);
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    confirmBlock.run();
                }
            });

            Button buttonCancel = (Button) dialog
                    .findViewById(R.id.buttonCancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(cancelBlock != null)
                        cancelBlock.run();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static void showOneButtonBigDialog(Context context,
                                           String message, String title, String buttonText) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.big_dialog_one_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            titleView.setText(title);
            Button button = (Button) dialog
                    .findViewById(R.id.button);
            button.setText(buttonText);
            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });
            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Dialog", e.getMessage());
        }
    }

    public static void showTwoButtonBigDialog(Context context,
                                           String message, String title, String buttonText, String cancelText, int imageResource,
                                           final Runnable confirmBlock, final Runnable cancelBlock) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.setCancelable(false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.big_dialog_two_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageResource(imageResource);
            if(title == null)
                titleView.setVisibility(View.GONE);
            else
                titleView.setText(title);

            Button buttonConfirm = (Button) dialog
                    .findViewById(R.id.buttonConfirm);
            buttonConfirm.setText(buttonText);
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    confirmBlock.run();
                }
            });

            Button buttonCancel = (Button) dialog
                    .findViewById(R.id.buttonCancel);
            buttonCancel.setText(cancelText);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(cancelBlock != null)
                        cancelBlock.run();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


    public static void showTwoButtonDialogForUpload(Context context,
                                                    String message, String title, String leftButtonText,String rightButtonText,
                                                    final Runnable confirmBlock, final Runnable cancelBlock) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_two_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            if(title == null)
                titleView.setVisibility(View.GONE);
            else
                titleView.setText(title);

            Button buttonConfirm = (Button) dialog
                    .findViewById(R.id.buttonConfirm);
            buttonConfirm.setText(leftButtonText);
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(confirmBlock != null)
                        confirmBlock.run();
                }
            });

            Button buttonCancel = (Button) dialog
                    .findViewById(R.id.buttonCancel);
            buttonCancel.setText(rightButtonText);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    if(cancelBlock != null)
                        cancelBlock.run();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }



    public static void showOneButtonBigDialog(Context context,
                                              String message, String message2, String title, String buttonText, int imageResource,
                                              final Runnable confirmBlock) {
        try {
            final Dialog dialog = new Dialog(context);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.big_dialog_one_button);
            TextView messageView = (TextView) dialog
                    .findViewById(R.id.message);
            messageView.setText(message);
            TextView message2View = (TextView) dialog
                    .findViewById(R.id.message2);
            message2View.setText(message2);
            TextView titleView = (TextView) dialog
                    .findViewById(R.id.title);
            ImageView image = (ImageView) dialog.findViewById(R.id.image);
            image.setImageResource(imageResource);
            if(title == null)
                titleView.setVisibility(View.GONE);
            else
                titleView.setText(title);

            Button buttonConfirm = (Button) dialog
                    .findViewById(R.id.button);
            buttonConfirm.setText(buttonText);
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                    confirmBlock.run();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public static Spinner setSpinner(Context context, Spinner spinner, int spinnerArray){
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(context,
                spinnerArray, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        return spinner;
    }

    public static Dialog spinnerDialog(Context context, String title, BaseAdapter baseAdapter){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_spinner);

        TextView titleView = (TextView) dialog.findViewById(R.id.title);
        ListView listView = (ListView) dialog.findViewById(R.id.list_view);
        Button cancelButton = (Button) dialog.findViewById(R.id.cancel_button);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        titleView.setText(title);

        listView.setAdapter(baseAdapter);

        return dialog;
    }

    public static String convertDateFormatFromTo(String dateString, String oldFormat, String newFormat){
        Date date = null;
        DateFormat originalFormat = new SimpleDateFormat(oldFormat);
        DateFormat targetFormat = new SimpleDateFormat(newFormat);
        try {
            date = originalFormat.parse(dateString);
        }
        catch(Exception e){

        }
        return targetFormat.format(date);
    }

    public static void setNavigationDrawerAction(final Activity activity, int position, DrawerLayout drawerLayout){
        if(Storage.is_session(activity)) {
            switch (position) {
                case 0:
                    activity.startActivity(new Intent(activity,
                            DashboardActivity.class));
                    break;
                case 1:
                    activity.startActivity(new Intent(activity,
                            RetailerBalanceTransferActivity.class));
                    break;
                case 2:
                    activity.startActivity(new Intent(activity,
                            TrialRetailerActivity.class));
                    break;
                case 3:
                    activity.startActivity(new Intent(activity,
                            RetailersActivity.class));
                    break;
                case 4:
                    activity.startActivity(new Intent(activity,
                            ReportBalanceTransferActivity.class));
                    break;
                case 5:
                    activity.startActivity(new Intent(activity,
                            SendTopupRequestActivity.class));
                    break;
                case 6:
                    activity.startActivity(new Intent(activity,
                            NotificationsActivity.class));
                    break;
                case 7:
                    activity.startActivity(new Intent(activity,
                            ChangePinActivity.class));
                    break;
                case 8:
                    activity.startActivity(new Intent(activity,
                            SupportActivity.class));
                    break;
                case 9:
                    Runnable confirmBlock = new Runnable() {
                        @Override
                        public void run() {
                            logout(activity);
                        }
                    };

                    if(drawerLayout.isDrawerOpen(Gravity.RIGHT))
                        drawerLayout.closeDrawer(Gravity.RIGHT);
                    Motley.showTwoButtonDialog(activity, "Are you sure you want to Log Out?", "LOGOUT", "Confirm",
                            confirmBlock, null);
                    break;
                default:
                    break;
            }
            if(!(activity instanceof DashboardActivity) && position != 8)
                activity.finish();
        }
        else {
            switch (position) {
                case 0:
                    activity.startActivity(new Intent(activity,
                            DashboardActivity.class));
                    break;
                case 1:
                    Intent intent = new Intent(activity,
                            LoginActivity.class);
                    activity.startActivityForResult(intent, Storage.REQUEST_LOGIN_SESSION);
                    break;
                case 2:
                    activity.startActivity(new Intent(activity,
                            RetailerBalanceTransferActivity.class));
                    break;
                case 3:
                    activity.startActivity(new Intent(activity,
                            TrialRetailerActivity.class));
                    break;
                case 4:
                    activity.startActivity(new Intent(activity,
                            RetailersActivity.class));
                    break;
                case 5:
                    activity.startActivity(new Intent(activity,
                            ReportBalanceTransferActivity.class));
                    break;
                case 6:
                    activity.startActivity(new Intent(activity,
                            SendTopupRequestActivity.class));
                    break;
                case 7:
                    activity.startActivity(new Intent(activity,
                            NotificationsActivity.class));
                    break;
                case 8:
                    activity.startActivity(new Intent(activity,
                            ChangePinActivity.class));
                    break;
                case 9:
                    activity.startActivity(new Intent(activity,
                            SupportActivity.class));
                    break;
                default:
                    break;
            }
            if(!(activity instanceof DashboardActivity))
                activity.finish();
        }
    }

    public static void logout(final Context context){
        final Map logoutParams = new HashMap();

        logoutParams.put("method", "logout");
        logoutParams.put("device_type", "android");

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Logging Out..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    Storage.clear_session(context);

                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.mindsarray.pay1distributor.ACTION_LOGOUT");
                    context.sendBroadcast(broadcastIntent);

                    Intent intent = new Intent(context, LoginActivity.class);
                    ((Activity) context).finish();
                    context.startActivity(intent);

                    if (status.equalsIgnoreCase("success")) {

                    } else {

                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
                ((Activity) context).finish();
            }
        };
        NetworkConnection.getInstance(context).handleRequest(logoutParams, responseListener, errorBlock);
    }

    public static void logoutInBackground(final Context context){
        final Map logoutParams = new HashMap();

        logoutParams.put("method", "logout");
        logoutParams.put("device_type", "android");

        Storage.clear_session(context);
        Storage.setSyncPreferencesString(context, Storage.LAST_SYNC_RUN_TIME, "");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);

                    Intent broadcastIntent = new Intent();
                    broadcastIntent.setAction("com.mindsarray.pay1distributor.ACTION_LOGOUT");
                    context.sendBroadcast(broadcastIntent);

                    if (status.equalsIgnoreCase("success")) {

                    } else {

                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
            }
        };

        NetworkConnection.getInstance(context).handleRequest(logoutParams, responseListener, null);
    }

    public static boolean isDouble(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInteger(String s) {
        try {
            Integer.parseInt(s);
        } catch(NumberFormatException e) {
            return false;
        } catch(NullPointerException e) {
            return false;
        }

        return true;
    }

    public static void getRetailers(Context context){
        Motley.serverLog(context, "Sync Adapter getRetailers", "Inside getRetailers");
        String group_id = Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_USER_GROUP_ID);
        long timeMillis = System.currentTimeMillis();
        long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);
        if(!group_id.equals("")) {
            Log.d("Start Sync Time", "Start");
            final Map retailerParams = new HashMap();
            retailerParams.put("method", "getRetailers");
            retailerParams.put("modified", Storage.getSyncPreferencesString(context, Storage.LAST_SYNC_RUN_TIME));
            if (group_id.equals("" + Storage.SALESMAN)) {
                retailerParams.put("distributor_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID));
                retailerParams.put("salesman_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_SALESMAN_ID));
            } else {
                retailerParams.put("distributor_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
            }

            Log.e("retailerParams", "retailerParams " + retailerParams.toString() + "");
            Motley.serverLog(context, "Sync Adapter getRetailers", "" + retailerParams.toString());
            try {
                String response = NetworkConnection.httpURLConnection(retailerParams);
                Log.e("SyncAdapter Response", response + "");
                Motley.serverLog(context, "Sync Adapter getRetailers", "" + response + "");
                JSONObject jsonObjectResponse = new JSONObject(response);
                if (jsonObjectResponse.length() != 0) {
                    String status = jsonObjectResponse.getString("status");
                    Log.e("SyncAdapter Status", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray retailers = jsonObjectResponse.getJSONArray("description");
                        Motley.serverLog(context, "Sync Adapter getRetailers before save", "" + retailers + "");
                        RetailersDataSource mRetailersDataSource = new RetailersDataSource(context);
                        mRetailersDataSource.open();
                        mRetailersDataSource.save(retailers);
                        mRetailersDataSource.close();

                        Log.d("Start Sync Time", "Start");
                        Storage.setSyncPreferencesString(context, Storage.LAST_SYNC_RUN_TIME, timeSeconds + "");
                        Motley.serverLog(context, "Sync Adapter getRetailers after save setting sync time", timeSeconds + "");
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
                Log.e("SyncAdapter", "" + e.getMessage());
                Motley.serverLog(context, "Sync Adapter getRetailers Exception", e.getMessage() + "");
            }
            finally {
//                Storage.setSyncPreferencesBoolean(context, Storage.IS_SYNC_RUNNING, false);
//                Intent stopSyncIntent = new Intent("sync_running");
//                stopSyncIntent.putExtra("is_sync_running", false);
//                context.sendBroadcast(stopSyncIntent);
//                Log.e("Sync running", "false");
            }
        }
    }

    public static void getAllRetailers(final Context context, final Runnable afterResponseBlock){
        long timeMillis = System.currentTimeMillis();
        final long timeSeconds = TimeUnit.MILLISECONDS.toSeconds(timeMillis);

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Loading Retailers...This may take some time.");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Multipart Response: ", " " + response);

                try {
                    Log.e("SyncAdapter Response",  "SyncAdapter Response " + response);
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    if (jsonObjectResponse.length() != 0) {
                        String status = jsonObjectResponse.getString("status");
                        Log.e("SyncAdapter Status", status);
                        if (status.equalsIgnoreCase("success")) {
                            JSONArray retailers = jsonObjectResponse.getJSONArray("description");

                            RetailersDataSource mRetailersDataSource = new RetailersDataSource(context);
                            mRetailersDataSource.open();
                            mRetailersDataSource.save(retailers);
                            mRetailersDataSource.close();

                            Storage.setSyncPreferencesString(context, Storage.LAST_SYNC_RUN_TIME, timeSeconds + "");
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("SyncAdapter", "" + e.getMessage());
                }
                finally {
                    afterResponseBlock.run();
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };

        String group_id = Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_USER_GROUP_ID);

        final Map retailerParams = new HashMap();
        if(!group_id.equals("")) {
            retailerParams.put("method", "getRetailers");
            retailerParams.put("modified", Storage.getSyncPreferencesString(context, Storage.LAST_SYNC_RUN_TIME));
            if (group_id.equals("" + Storage.SALESMAN)) {
                retailerParams.put("distributor_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID));
                retailerParams.put("salesman_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_SALESMAN_ID));
            } else {
                retailerParams.put("distributor_id", Storage.getSyncPreferencesString(context, Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
            }
        }
        NetworkConnection.getInstance(context).handleRequest(retailerParams, responseListener, errorBlock);
    }

    public static Account getAccount(Context context) {
        AccountManager accManager = AccountManager.get(context);
        Account accounts[] =  accManager.getAccountsByType(Storage.ACCOUNT_TYPE);
        return accounts[0];
    }

    public static Account CreateSyncAccount(Context context) {
        Account newAccount = new Account(Storage.ACCOUNT, Storage.ACCOUNT_TYPE);

        AccountManager accountManager =
                (AccountManager) context.getSystemService(
                        Context.ACCOUNT_SERVICE);

        if (accountManager.addAccountExplicitly(newAccount, null, null)) {

        } else {
            /*
             * The account exists or some other error occurred. Log this, report it,
             * or handle it internally.
             */
        }
        return newAccount;
    }

    public static void sync(Account sAccount){
        Bundle settingsBundle = new Bundle();
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_MANUAL, true);
        settingsBundle.putBoolean(
                ContentResolver.SYNC_EXTRAS_EXPEDITED, true);

        ContentResolver.requestSync(sAccount, Storage.AUTHORITY, settingsBundle);
    }

    public static void sendOTP(final Context context, String mobile, final Runnable responseBlock){
        final Map sendOTPParams = new HashMap();
        sendOTPParams.put("method", "sendOTP");
        sendOTPParams.put("m", mobile);

        final ProgressDialog progressDialog = ProgressDialog.show(context, "", "Sending One Time Password..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    if (status.equalsIgnoreCase("success")) {
                        if(responseBlock != null)
                            responseBlock.run();
                    } else {
                        Motley.showOneButtonDialog(context, jsonObjectResponse.getString("description"), "FAILED", "OK");
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(context).handleRequest(sendOTPParams, responseListener, errorBlock);
    }

    public static boolean isMobileValid(String mobile) {
        return mobile.matches("\\d{10}") && (mobile.startsWith("7") || mobile.startsWith("8") || mobile.startsWith("9"));
    }

    public static boolean isPinValid(String pin) {
        return pin.length() > 3;
    }

    public static void notify(Context context, String title, String message){
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.launcher_icon)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true);

        Intent resultIntent = new Intent(context, NotificationsActivity.class);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        stackBuilder.addParentStack(DashboardActivity.class);
        stackBuilder.addNextIntent(resultIntent);
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                    0,
                    PendingIntent.FLAG_UPDATE_CURRENT
                );
        mBuilder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(0, mBuilder.build());
    }

    public static long daysSince(Context context, String created_on){
        String trial_period = Storage.getPersistentString(context, Storage.SHAREDPREFERENCE_TRIAL_PERIOD);
        Date date;
        Date today = new Date();
        DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        try {
            date = format.parse(created_on);

            long interval = date.getTime() + (Long.valueOf(trial_period) * 24 * 60 * 60 * 1000) - today.getTime();
            long daysInterval = 0;
            if(interval > 0)
                daysInterval = TimeUnit.DAYS.convert(interval, TimeUnit.MILLISECONDS);
            else
                daysInterval = 0;

            return daysInterval;
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return Long.parseLong(trial_period);
    }

    public static String convertMillisToDateTime(String milliseconds){
        long ms = Long.valueOf(milliseconds);
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(ms);

        int mYear = calendar.get(Calendar.YEAR);
        int mMonth = calendar.get(Calendar.MONTH);
        int mDay = calendar.get(Calendar.DAY_OF_MONTH);
        int mHour = calendar.get(Calendar.HOUR_OF_DAY);
        int mMinute = calendar.get(Calendar.MINUTE);

        return mDay + "-" + mMonth + "-" + mYear + " " + mHour + ":" + mMinute;
    }

    public static long datesDiff(String format, String date1, String date2){
        DateFormat dateFormat = new SimpleDateFormat(format);
        long interval = 0;
        try {
            Date dateOne = dateFormat.parse(date1);
            Date dateTwo = dateFormat.parse(date2);

            interval = dateOne.getTime() - dateTwo.getTime();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        return interval;
    }

    public static void setBackground(Context context, View view, int resourceID){
        final int sdk = android.os.Build.VERSION.SDK_INT;
        if(sdk < android.os.Build.VERSION_CODES.JELLY_BEAN) {
            view.setBackgroundDrawable( context.getResources().getDrawable(resourceID) );
        } else {
            view.setBackground( context.getResources().getDrawable(resourceID));
        }
    }

    public static void serverLog(Context context, String title, String message){
        if(Storage.is_session(context)){
            String mobile = Storage.getPersistentString(context, Storage.SHAREDPREFERENCE_MOBILE_NUMBER);
            Map loggingParams = new HashMap();
            loggingParams.put("method", "serverLog");
            loggingParams.put("title", title);
            loggingParams.put("message", message);
            loggingParams.put("mobile", mobile);
            loggingParams.put("time", "" + convertMillisToDateTime("" + System.currentTimeMillis()));

            Response.Listener responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Response: ", response);
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        String status = jsonObjectResponse.getString("status");

                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                }
            };
//            NetworkConnection.getInstance(context).handleRequest(loggingParams, responseListener, null);
        }
    }
}
