package com.mindsarray.pay1distributor.util;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkError;
import com.android.volley.NetworkResponse;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mindsarray.pay1distributor.LoginActivity;

import org.apache.http.entity.ContentType;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.CookiePolicy;
import java.net.HttpCookie;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by rohan on 12/5/15.
 */
public class NetworkConnection {

    private static NetworkConnection nInstance = null;
    private static Context nContext;
    private RequestQueue nRequestQueue;
    private static final int SOCKET_TIMEOUT_MS = 60000;
    private boolean multipartRequest = false;
    private DefaultRetryPolicy retryPolicy;

    public NetworkConnection(Context context){
        nContext = context;
        nRequestQueue = getRequestQueue();
    }

    public static synchronized NetworkConnection getInstance(Context context) {
        if (nInstance == null) {
            nInstance = new NetworkConnection(context);
        }
        return nInstance;
    }

    public RequestQueue getRequestQueue() {
        if (nRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            nRequestQueue = Volley.newRequestQueue(nContext.getApplicationContext());
        }
        return nRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }

    public static boolean isOnline(Context context) {
        try {
            ConnectivityManager connectivityManager = (ConnectivityManager) context
                    .getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo activeNetworkInfo = connectivityManager
                    .getActiveNetworkInfo();
            return activeNetworkInfo != null && activeNetworkInfo.isConnected();
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void handleRequest(final Map keyValueParams, Response.Listener responseListener, Runnable errorBlock){
        if (isOnline(nContext)) {
            Map fileParams = new HashMap();
            Map stringParams = new HashMap();
            VolleyLog.e(keyValueParams + "");

            CookieManager cookieManager = new CookieManager(new PersistentCookieStore(nContext), CookiePolicy.ACCEPT_ALL);
            CookieHandler.setDefault(cookieManager);

            retryPolicy = new DefaultRetryPolicy(
                    SOCKET_TIMEOUT_MS,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);

            Iterator it = keyValueParams.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                if(pair.getValue() instanceof ArrayList<?>){
                    if(!((ArrayList<?>) pair.getValue()).isEmpty()) {
//                        if(pair.getKey().equals("remove[]")){
//                            for (String fileURI : (ArrayList<String>) pair.getValue()) {
//                                stringParams.put(pair.getKey(), fileURI);
//                            }
//                        }
//                        else {
                        multipartRequest = true;
                        fileParams.put(pair.getKey(), pair.getValue());
//                        }
                    }
                }
                else
                    stringParams.put(pair.getKey(), pair.getValue());
            }
            VolleyLog.e("MultipartRequest: " + multipartRequest);
            if(multipartRequest){
                enqueueMultipartRequest(fileParams, stringParams, responseListener, errorBlock);
            }
            else
                enqueueRequest(stringParams, responseListener, errorBlock);
        }
        else {
            errorBlock.run();
            if(!((Activity) nContext).isFinishing()){
                Motley.showOneButtonDialog(nContext, Storage.ERROR_INTERNET, "NETWORK ERROR", "OK");
            }
            else
                Motley.showOneButtonDialog(nContext, Storage.ERROR_INTERNET, "NETWORK ERROR", "OK");
        }
    }

    public void enqueueRequest(final Map keyValueParams, Response.Listener responseListener, Runnable errorBlock) {
        String url = Storage.API_URL;
        Log.e("api url", url);
        RequestQueue queue = NetworkConnection.getInstance(nContext.getApplicationContext()).
                getRequestQueue();
        StringRequest stringRequest = new StringRequest(Request.Method.POST, url,
                responseListener, errorListener(errorBlock)) {

            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                VolleyLog.e(keyValueParams + "");
                return keyValueParams;
            }
            @Override
            protected Response parseNetworkResponse(NetworkResponse response) {
                String responseString;
                VolleyError volleyServerError = new ServerError();
                VolleyError volleyParseError = new ParseError();
                try {
                    responseString = new String(response.data,
                            HttpHeaderParser.parseCharset(response.headers));
                    VolleyLog.e("Headers: " + response.headers + "");
                    VolleyLog.e("Response: " + responseString);
                    JsonObject responseJsonObject = NetworkConnection.parseNetworkResponse(responseString);
                    VolleyLog.e(responseJsonObject + "");
                    JsonElement jsonElement = responseJsonObject.get("status");
                    if(jsonElement == null)
                        return Response.error(volleyServerError);
                    else
                        return Response.success(responseJsonObject.toString(),
                                HttpHeaderParser.parseCacheHeaders(response));
                } catch (Exception e) {
                    VolleyLog.e(e.getMessage() + "");
                    e.printStackTrace();
                    return Response.error(volleyParseError);
                }
            }
        };
        stringRequest.setRetryPolicy(retryPolicy);

        try {
            stringRequest.getHeaders().put("Cookie", setCookies());
        }
        catch(Exception e){

        }
        NetworkConnection.getInstance(nContext).addToRequestQueue(stringRequest);
    }

    public String setCookies() {
        List<HttpCookie> cookies = new PersistentCookieStore(nContext).getCookies();

        StringBuilder sb = new StringBuilder();
        for (HttpCookie cookie : cookies) {
            sb.append(cookie.getName() + "=" + cookie.getValue()).append("; ");
        }

        return sb.toString();
    }

    public void enqueueMultipartRequest(final Map fileParams, final Map stringParams, Response.Listener responseListener, Runnable errorBlock){
        String url = Storage.API_URL;

        Request mMultipartRequest = new MultipartRequest(url, errorListener(errorBlock), responseListener, fileParams, stringParams);
        mMultipartRequest.setRetryPolicy(retryPolicy);
        NetworkConnection.getInstance(nContext).addToRequestQueue(mMultipartRequest);
    }

    public static JsonObject parseNetworkResponse(String response){
        JsonObject responseObject = new JsonObject();
        VolleyLog.e(response);
        String jsonString = "";
        Gson gson = new Gson();
        response = response.trim();
        if(response.startsWith("Error")){
            Motley.showOneButtonDialog(nContext, Storage.ERROR_SERVER, "SERVER ERROR", "OK");
        }
        else {
            if (response.startsWith("(") && response.endsWith(");")) {
                jsonString = response.substring(1, response.length() - 2);
                VolleyLog.e(jsonString);
                responseObject = gson.fromJson(jsonString, JsonElement.class).getAsJsonArray().get(0).getAsJsonObject();
            } else {
                jsonString = response;
                VolleyLog.e(jsonString);
                responseObject = gson.fromJson(jsonString, JsonElement.class).getAsJsonObject();
            }
            if(responseObject.has("code") && responseObject.get("code").getAsString().equals("403")){
                Storage.setPersistentBoolean(nContext, Storage.SHAREDPREFERENCE_IS_LOGIN, false);
                Log.e("Response Error", "Not logged in");
                Intent intent = new Intent(nContext, LoginActivity.class);
                intent.putExtra("ACTION", "FINISH_LOGIN");
                ((Activity) nContext).startActivityForResult(intent, Storage.REQUEST_LOGIN_SESSION);
            }
        }
        return responseObject;
    }

    public Response.ErrorListener errorListener(final Runnable errorBlock){
        return new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if(errorBlock != null){
                    errorBlock.run();
                }
                NetworkResponse response = error.networkResponse;
                if (response != null && response.data != null) {
                    switch (response.statusCode) {
                        case 403:
                            Log.e("Response Error", "Not logged in");
                            Intent intent = new Intent(nContext, LoginActivity.class);
                            intent.putExtra("ACTION", "FINISH_LOGIN");
                            ((Activity) nContext).startActivityForResult(intent, Storage.REQUEST_LOGIN_SESSION);
                            break;
                    }
                }
                Log.e("That didn't work:", error.toString());
                if (error instanceof NoConnectionError) {
                    Motley.showOneButtonDialog(nContext, Storage.ERROR_SERVER, "CONNECTION ERROR", "OK");
                } else if (error instanceof NetworkError) {
                    Motley.showOneButtonDialog(nContext, Storage.ERROR_INTERNET, "NETWORK ERROR", "OK");
                } else if (error instanceof ServerError) {
                    Motley.showOneButtonDialog(nContext, Storage.ERROR_SERVER, "SERVER ERROR", "OK");
                } else
                    Motley.showOneButtonDialog(nContext, "Some error occurred. Try again.", "ERROR", "OK");
            }
        };
    }

    public static String httpURLConnection(final Map keyValueParams) throws IOException {
        InputStream is = null;

        try {
            URL url = new URL(Storage.API_URL);
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();

            conn.setDoOutput(true);
            conn.setInstanceFollowRedirects(false);
            conn.setRequestMethod("POST");
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(SOCKET_TIMEOUT_MS);
            conn.setUseCaches(false);

            OutputStream os = conn.getOutputStream();
            BufferedWriter writer = new BufferedWriter(
                    new OutputStreamWriter(os, "UTF-8"));
            writer.write(getQuery(keyValueParams));
            writer.flush();
            writer.close();
            os.close();
            conn.connect();
            int response = conn.getResponseCode();
            Log.e("HttpURLConnection", "The response is: " + response);
            is = conn.getInputStream();

            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            StringBuilder result = new StringBuilder();
            String line;
            while ((line = reader.readLine()) != null) {
                result.append(line);
            }

            return parseNetworkResponse(result.toString()).toString();
        }
        catch(Exception e){
            e.printStackTrace();
            return new JsonObject().toString();
        } finally {
            if (is != null) {
                is.close();
            }
        }
    }

    private static String getQuery(final Map<String, String> keyValueParams) throws UnsupportedEncodingException{
        StringBuilder result = new StringBuilder();
        boolean first = true;

        for (Map.Entry<String, String> entry : keyValueParams.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();

            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(key, "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(value, "UTF-8"));
        }

        return result.toString();
    }
}
