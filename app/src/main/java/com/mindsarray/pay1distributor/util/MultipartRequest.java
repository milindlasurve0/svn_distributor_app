package com.mindsarray.pay1distributor.util;

import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.Response.ErrorListener;
import com.android.volley.Response.Listener;
import com.android.volley.ServerError;
import com.android.volley.VolleyError;
import com.android.volley.VolleyLog;
import com.android.volley.toolbox.HttpHeaderParser;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class MultipartRequest<T> extends Request<T> {

    private MultipartEntityBuilder mBuilder = MultipartEntityBuilder.create();
    private final Response.Listener<T> mListener;
    protected Map<String, String> headers;
    private final Map mFileParams;
    private final Map mStringParams;

    public MultipartRequest(String url, ErrorListener errorListener, Listener<T> listener, final Map fileParams, final Map stringParams){
        super(Method.POST, url, errorListener);

        mListener = listener;

        mFileParams = fileParams;
        mStringParams = stringParams;
        Log.e("File Params: ", mFileParams.toString());

        buildMultipartEntity();
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = super.getHeaders();

        if (headers == null
                || headers.equals(Collections.emptyMap())) {
            headers = new HashMap<String, String>();
        }

        headers.put("Accept", "application/json");
        return headers;
    }

    private void buildMultipartEntity(){
        try {
            Iterator itF = mFileParams.entrySet().iterator();
            while (itF.hasNext()) {
                Map.Entry pair = (Map.Entry) itF.next();
                for (String fileString : (ArrayList<String>) pair.getValue()) {
                    File file = new File(fileString);
                    mBuilder.addBinaryBody((String) pair.getKey(), file, ContentType.create("image/jpeg"), file.getName());
                }
            }
            mBuilder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
            mBuilder.setLaxMode().setBoundary("xx").setCharset(Charset.forName("UTF-8"));

            Iterator itS = mStringParams.entrySet().iterator();
            while (itS.hasNext()) {
                Map.Entry pair = (Map.Entry) itS.next();
                mBuilder.addTextBody((String) pair.getKey(), (String) pair.getValue());
            }
            Log.e("Multipart Request: ", "Entity built");
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public String getBodyContentType(){
        String contentTypeHeader = mBuilder.build().getContentType().getValue();
        return contentTypeHeader;
    }

    @Override
    public byte[] getBody() throws AuthFailureError{
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        try {
            mBuilder.build().writeTo(bos);
        } catch (IOException e) {
            VolleyLog.e(e.getMessage());
            VolleyLog.e("IOException writing to ByteArrayOutputStream bos, building the multipart request.");
        }

        return bos.toByteArray();
    }

    @Override
    protected Response<T> parseNetworkResponse(NetworkResponse response) {
        String responseString;
        VolleyError volleyServerError = new ServerError();
        VolleyError volleyParseError = new ParseError();
        try {
            responseString = new String(response.data,
                    HttpHeaderParser.parseCharset(response.headers));
            VolleyLog.e("Headers: " + response.headers + "");
            VolleyLog.e(responseString);
            JsonObject responseJsonObject = NetworkConnection.parseNetworkResponse(responseString);
            VolleyLog.e(responseJsonObject + "");
            JsonElement jsonElement = responseJsonObject.get("status");
            if(jsonElement == null)
                return Response.error(volleyServerError);
            else
                return Response.success((T) responseJsonObject.toString(),
                        HttpHeaderParser.parseCacheHeaders(response));
//                return Response.success((T) responseJsonObject,
//                        HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            VolleyLog.e(e.getMessage());
            return Response.error(volleyParseError);
        }
    }

    @Override
    protected void deliverResponse(T response) {
        mListener.onResponse(response);
    }
}