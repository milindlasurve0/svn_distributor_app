package com.mindsarray.pay1distributor.util;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;

import com.mindsarray.pay1distributor.R;

/**
 * Created by rohan on 13/5/15.
 */
public class Storage {

    public final static int PLAY_SERVICES_RESOLUTION_REQUEST = 9000;
    private static final String SHAREDPREFERENCE_APP_VERSION = "appVersion";
    public static final String SENDER_ID = "1039344451096";
    public static final String REGISTRATION_COMPLETE = "registrationComplete";
    public static final int DISTRIBUTOR = 5;
    public static final int SALESMAN = 11;
    public static final int RETAILER = 6;

    public static String URL = "http://cc.pay1.in/";
//    public static String URL = "http://192.168.0.189/";
//    public static String URL = "http://192.168.0.158/";
    public static String API_URL = URL + "distributors/api?";

    public static final String SHAREDPREFERENCE_GCMID = "gcm_registration_id";
    public static final String SHAREDPREFERENCE_UUID = "uuid";
    public static final String SHAREDPREFERENCE_IS_LOGIN = "isLogin";
    public static final String SHAREDPREFERENCE_USER_ID = "user_id";
    public static final String SHAREDPREFERENCE_USER_NAME = "user_name";
    public static final String SHAREDPREFERENCE_MOBILE_NUMBER = "mobile_number";
    public static final String SHAREDPREFERENCE_USER_GROUP_ID = "user_group_id";
    public static final String SHAREDPREFERENCE_BALANCE = "balance";

    public static final String SHAREDPREFERENCE_TRIAL_PERIOD = "trial_period";

    public static final String SHAREDPREFERENCE_DISTRIBUTOR_ID = "distributor_id";
    public static final String SHAREDPREFERENCE_DISTRIBUTOR_NAME = "distributor_name";

    public static final String SHAREDPREFERENCE_SALESMAN_ID = "salesman_id";
    public static final String SHAREDPREFERENCE_SALESMAN_NAME = "salesman_name";
    public static final String SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID = "salesman_distributor_id";

    public static final String SHAREDPREFERENCE_LOGOUT_ON_APP_UPDATE_FLAG = "logout_on_app_update_flag";

    public static final String LAST_SYNC_RUN_TIME = "last_sync_time";
    public static final String IS_SYNC_RUNNING = "is_sync_running";
    public static final String AUTHORITY = "com.mindsarray.pay1distributor.provider";
    public static final String ACCOUNT_TYPE = "Pay1 Distributor";
    public static final String ACCOUNT = "Pay1 Distributor";
    public static final long SECONDS_PER_HOUR = 3600L;
    public static final long SYNC_INTERVAL_IN_HOURS = 4L;
    public static final long SYNC_INTERVAL = SYNC_INTERVAL_IN_HOURS * SECONDS_PER_HOUR;

    public static final String ERROR_INTERNET = "Check your internet connection";
    public static final String ERROR_SERVER = "Error connecting to server. Please try again.";

    public static final int FILTER_LIST_THRESHOLD = 3;

    public static final int REQUEST_LOGIN_SESSION = 124;
    public static final int REQUEST_GET_MAP_LOCATION = 123;
    public static final int REQUEST_IMAGE_CAPTURE = 1;
    public static final int SELECT_PHOTO = 100;
    public static final int SUCCESS_RESULT = 0;
    public static final int FAILURE_RESULT = 1;
    public static final int REQUEST_DANGEROUS_PERMISSIONS = 11;
    public static final String PACKAGE_NAME = "com.mindsarray.pay1distributor";
    public static final String RECEIVER = PACKAGE_NAME + ".RECEIVER";
    public static final String RESULT_DATA_KEY = PACKAGE_NAME + ".RESULT_DATA_KEY";
    public static final String LOCATION_DATA_EXTRA = PACKAGE_NAME + ".LOCATION_DATA_EXTRA";

    private static String SESSION_PREFERENCES = "session_preferences";
    private static String SYNC_PREFERENCES = "sync_preferences";

    public static void setPersistentString(Context context, String key,
                                           String value) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPersistentString(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getString(key, "");
    }

    public static void setPersistentBoolean(Context context, String key,
                                            Boolean value) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static Boolean getPersistentBoolean(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getBoolean(key, false);
    }

    public static void setPersistentFloat(Context context, String key,
                                           Float value) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putFloat(key, value);
        editor.apply();
    }

    public static Float getPersistentFloat(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getFloat(key, 0);
    }

    public static void setPersistentInt(Context context, String key,
                                           int value) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key, value);
        editor.apply();
    }

    public static int getPersistentInt(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getInt(key, 0);
    }

    public static void setPersistentLong(Context context, String key,
                                           Long value) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key, value);
        editor.apply();
    }

    public static Long getPersistentLong(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        return settings.getLong(key, 0);
    }

    public static void setUUID(Context context){
        try {
            String uuid;
            TelephonyManager tManager = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            uuid = tManager.getDeviceId();
            if (uuid == null) {
                uuid = Settings.Secure.getString(
                        context.getContentResolver(),
                        Settings.Secure.ANDROID_ID);
            }
            if (getPersistentString(context,
                    SHAREDPREFERENCE_UUID).equals(""))
                setPersistentString(context,
                        SHAREDPREFERENCE_UUID, uuid);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public static String getRegistrationId(Context context) {
        String registrationId = getPersistentString(context, SHAREDPREFERENCE_GCMID);
        if (registrationId.isEmpty()) {
            Log.i("GCM", "Registration not found.");
            return "";
        }

        int registeredVersion = getPersistentInt(context, SHAREDPREFERENCE_APP_VERSION);
        int currentVersion = getAppVersion(context);
        if (registeredVersion != currentVersion) {
            Log.i("GCM Registration", "App version changed.");
            return "";
        }
        return registrationId;
    }

    public static int getAppVersion(Context context) {
        try {
            PackageInfo packageInfo = context.getPackageManager()
                    .getPackageInfo(context.getPackageName(), 0);
            return packageInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            // should never happen
            throw new RuntimeException("Could not get package name: " + e);
        }
    }

    public static Boolean is_session(Context context){
        return getPersistentBoolean(context, SHAREDPREFERENCE_IS_LOGIN);
    }

    public static int user_group(Context context){
        return getPersistentInt(context, SHAREDPREFERENCE_USER_GROUP_ID);
    }

    public static void clear_session(Context context){
        SharedPreferences settings = context
                .getSharedPreferences(SESSION_PREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = settings.edit();
        editor.clear();
        editor.apply();
    }

    public static String[] shopTypeArray(Context context){
        String[] shopTypeArray = context.getResources().getStringArray(R.array.shop_type_array);
        String[] shopTypeArrayFull = context.getResources().getStringArray(R.array.shop_type_array_full);

        String[] shopType = new String[25];
        for(int i = 0; i < shopTypeArrayFull.length; i++){
            for(int j = 0; j < shopTypeArray.length; j++){
                if(shopTypeArray[j].equals(shopTypeArrayFull[i])){
                    shopType[i] = shopTypeArrayFull[i];
                }
            }
        }
        return shopType;
    }

    public static String[] locationTypeArray(Context context){
        String[] locationTypeArray = context.getResources().getStringArray(R.array.location_type_array);

        String[] locationType = new String[7];
        locationType[4] = locationTypeArray[4];
        locationType[6] = locationTypeArray[6];
        locationType[7] = locationTypeArray[7];

        return locationType;
    }

    public static void setSyncPreferencesString(Context context, String key,
                                           String value) {
        SharedPreferences settings = context
                .getSharedPreferences(SYNC_PREFERENCES, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getSyncPreferencesString(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SYNC_PREFERENCES, Context.MODE_MULTI_PROCESS);
        return settings.getString(key, "");
    }

    public static void setSyncPreferencesBoolean(Context context, String key,
                                            Boolean value) {
        SharedPreferences settings = context
                .getSharedPreferences(SYNC_PREFERENCES, Context.MODE_MULTI_PROCESS);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key, value);
        editor.apply();
    }

    public static Boolean getSyncPreferencesBoolean(Context context, String key) {
        SharedPreferences settings = context
                .getSharedPreferences(SYNC_PREFERENCES, Context.MODE_MULTI_PROCESS);
        return settings.getBoolean(key, false);
    }
}
