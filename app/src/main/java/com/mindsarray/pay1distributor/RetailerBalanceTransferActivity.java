package com.mindsarray.pay1distributor;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.provider.SyncStateContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.adapters.NameMobileAutoCompleteAdapter;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.EnglishNumberToWords;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONObject;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class RetailerBalanceTransferActivity extends BaseActivity {

    private String retailer_id;
    private RetailersDataSource retailer;

    private AutoCompleteTextView retailerMobileAutoCompleteTextView;
    private TextView retailerShopNameTextView, amountInWordsTextView, lastTransferTextView;
    private EditText balanceAmountEditText, transferNoteEditText;
    private Button transferConfirmButton, transferCancelButton;

    private Context mContext = RetailerBalanceTransferActivity.this;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

//        setContentView(R.layout.activity_retailer_balance_transfer);
        setActivityView(R.layout.activity_retailer_balance_transfer);
        setActivityTitle("Balance Transfer");

        retailerMobileAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.retailer_mobile);
        retailerShopNameTextView = (TextView) findViewById(R.id.retailer_shop_name);
        balanceAmountEditText = (EditText) findViewById(R.id.balance_amount);
        amountInWordsTextView = (TextView) findViewById(R.id.amount_in_words);
        transferNoteEditText = (EditText) findViewById(R.id.transfer_note);
        lastTransferTextView = (TextView) findViewById(R.id.last_transfer);
        transferConfirmButton = (Button) findViewById(R.id.transfer_confirm_button);
        transferCancelButton = (Button) findViewById(R.id.transfer_cancel_button);

        retailerMobileAutoCompleteTextView.setThreshold(Storage.FILTER_LIST_THRESHOLD);
        retailerMobileAutoCompleteTextView.setAdapter(new NameMobileAutoCompleteAdapter(mContext));
        retailerMobileAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                retailer = (RetailersDataSource) adapterView.getItemAtPosition(position);
                retailerMobileAutoCompleteTextView.setText(retailer.mobile);
                retailerShopNameTextView.setText(retailer.shop_name);
                retailer_id = retailer.retailer_id;

                balanceAmountEditText.requestFocus();

                lastBalanceTransfer();
            }
        });

        balanceAmountEditText.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {

            }

            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {


            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (balanceAmountEditText.getText().toString().equals("")) {
                    amountInWordsTextView.setText("");
                } else {
                    amountInWordsTextView.setText(EnglishNumberToWords.convert(
                            Long.parseLong(balanceAmountEditText.getText().toString())) + " Rupees Only");
                }
                if (balanceAmountEditText.length() == 6) {
                    transferNoteEditText.requestFocus();
                }
            }
        });

        transferConfirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(Storage.is_session(mContext)) {
                    if (validateForm())
                        transferBalance();
                }
                else {
                    Intent intent = new Intent(mContext, LoginActivity.class);
                    intent.putExtra("ACTION", "FINISH_LOGIN");
                    ((Activity) mContext).startActivityForResult(intent, Storage.REQUEST_LOGIN_SESSION);
                }
            }
        });

        transferCancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        retailerMobileAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                try {
                    if (retailerMobileAutoCompleteTextView.length() == 10) {
                        balanceAmountEditText.requestFocus();
                    }
                    if (retailerMobileAutoCompleteTextView.length() < 10) {
                        retailerShopNameTextView.setText("");
                        balanceAmountEditText.setText("");
                        amountInWordsTextView.setText("");
                        transferNoteEditText.setText("");
                        lastTransferTextView.setText("");
                        retailer = new RetailersDataSource(mContext);
                        retailer_id = null;
                    }
                }
                catch(Exception e){
                    e.printStackTrace();
                }
            }
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    public Boolean validateForm(){
        String message = "";
        if(!retailerMobileAutoCompleteTextView.getText().toString().matches("\\d{10}")){
            message = "Enter a proper mobile number";
        }
        else if(balanceAmountEditText.getText().toString().equals("")){
            message = "Enter correct balance amount";
        }
        else if(retailer == null || retailer_id == null){
            message = "This number is not a retailer or is not assigned to you or update your retailer list";
        }
        if(message.equals("")){
            return true;
        }
        else {
            Motley.showOneButtonDialog(mContext, message, "FORM INCOMPLETE", "OK");
            return false;
        }
    }

    public void transferBalance(){
        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Transferring balance..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");
                    String message = jsonObjectResponse.getString("description");
                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        int balance = (int) Double.parseDouble(Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_BALANCE));
                        Storage.setPersistentString(mContext, Storage.SHAREDPREFERENCE_BALANCE,
                                "" + (balance - (int) Double.parseDouble(balanceAmountEditText.getText().toString())));

                        retailerMobileAutoCompleteTextView.setText("");
                        retailerShopNameTextView.setText("");
                        balanceAmountEditText.setText("");
                        amountInWordsTextView.setText("");
                        transferNoteEditText.setText("");
                        lastTransferTextView.setText("");

//                        if(retailer.kycPendingReminderFlag()){
//                            Intent intent = new Intent(mContext,
//                                    KYCPromptActivity.class);
//                            intent.putExtra("RETAILER_ID", retailer_id + "");
//                            intent.putExtra("MESSAGE", message + "");
//                            mContext.startActivity(intent);
//                        }
//                        else
                        Motley.showOneButtonDialog(mContext, message, "SUCCESS", "OK");
                    }
                    else
                        Motley.showOneButtonDialog(mContext, message, "FAILURE", "OK");
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(transferBalanceParams(), responseListener, errorBlock);
    }

    private Map transferBalanceParams(){
        final Map transferBalanceParams = new HashMap();
        transferBalanceParams.put("method", "transferBalance");
        transferBalanceParams.put("r_id", retailer_id + "");
        transferBalanceParams.put("t_t", "1");
        transferBalanceParams.put("note", transferNoteEditText.getText().toString());
        transferBalanceParams.put("a", balanceAmountEditText.getText().toString());
        if(Storage.user_group(mContext) == Storage.SALESMAN) {
            transferBalanceParams.put("s_id", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_SALESMAN_ID));
            transferBalanceParams.put("s_n", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_USER_NAME));
        }
        transferBalanceParams.put("g_id", "" + Storage.getPersistentInt(mContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID));

        return transferBalanceParams;
    }

    private void lastBalanceTransfer(){
        final Map lastBalanceTransferParams = new HashMap();
        lastBalanceTransferParams.put("method", "lastBalanceTransfer");
        lastBalanceTransferParams.put("r_id", retailer_id);

        if(retailer_id != null){
            final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Fetching last transaction..");
            Response.Listener responseListener = new Response.Listener<String>() {
                @Override
                public void onResponse(String response) {
                    Log.e("Response: ", response);
                    try {
                        JSONObject jsonObjectResponse = new JSONObject(response);
                        String status = jsonObjectResponse.getString("status");
                        JSONObject description = jsonObjectResponse.getJSONObject("description");
                        Log.e("Status: ", status);
                        if (status.equalsIgnoreCase("success")) {
                            String amount = description.getString("amount");
                            String time_stamp = description.getString("time_stamp");
                            lastTransferTextView.setText("Last balance transfer done: Rs." + amount + " " + time_stamp);
                            lastTransferTextView.setVisibility(View.VISIBLE);
                        }
                    }
                    catch(Exception e) {
                        e.printStackTrace();
                    }
                    finally {
                        progressDialog.dismiss();
                    }
                }
            };
            Runnable errorBlock = new Runnable() {
                @Override
                public void run() {
                    progressDialog.dismiss();
                }
            };
            NetworkConnection.getInstance(mContext).handleRequest(lastBalanceTransferParams, responseListener, errorBlock);
        }
    }
}
