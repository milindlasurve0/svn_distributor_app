package com.mindsarray.pay1distributor;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;

import com.google.android.gms.analytics.HitBuilders;
import com.google.android.gms.analytics.Tracker;

/**
 * Created by rohan on 21/1/16.
 */
public class ChildActivity extends Activity {

    protected Tracker mTracker;
    String TAG = this.getClass().getSimpleName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        BaseApplication application = (BaseApplication) getApplication();
        mTracker = application.getDefaultTracker();
    }

    @Override
    protected void onResume(){
        super.onResume();
        Log.i(TAG, "Setting screen name: " + TAG);
        mTracker.setScreenName(TAG);
        mTracker.send(new HitBuilders.ScreenViewBuilder().build());
    }
}
