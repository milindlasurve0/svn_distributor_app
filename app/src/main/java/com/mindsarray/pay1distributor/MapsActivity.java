package com.mindsarray.pay1distributor;

/**
 * Created by rohan on 21/7/15.
 */
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.Button;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mindsarray.pay1distributor.util.Motley;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private String latitude;
    private String longitude;

    LocationManager locationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
     /* GoogleMap googleMap=  mapFragment.getMap();
        googleMap.setMyLocationEnabled(true);*/
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            latitude = extras.getString("latitude");
            longitude = extras.getString("longitude");
        }

        Button setLocationButton = (Button) findViewById(R.id.button_setLocation);
        setLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setResult(Activity.RESULT_OK,
                        new Intent().putExtra("latitude", latitude).putExtra("longitude", longitude));
                finish();
            }
        });

        if(latitude == null || longitude == null){
            latitude = "18.9750";
            longitude = "72.8258";
        }
    }

    @Override
    public void onMapReady(final GoogleMap map) {
        LatLng currentLocation = new LatLng(18.9750, 72.8258);
        if(Motley.isDouble(latitude) && Motley.isDouble(longitude)){
            currentLocation = new LatLng(Double.parseDouble(latitude), Double.parseDouble(longitude));
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            //DO CHECK PERMISSION

        if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                || checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            //DO OP WITH LOCATION SERVICE
            map.setMyLocationEnabled(true);
        }
        }else{
            map.setMyLocationEnabled(true);
        }
        map.addMarker(new MarkerOptions().position(currentLocation).title("Shop Location"));
        map.moveCamera(CameraUpdateFactory.newLatLngZoom(currentLocation, 15));
//        map.animateCamera(CameraUpdateFactory.zoomTo(10), 2000, null);
        map.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                map.clear();
                latitude = point.latitude + "";
                longitude = point.longitude + "";
                map.addMarker(markerOptions(point));
            }
        });

        map.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {

            @Override
            public void onMarkerDragStart(Marker marker) {

            }

            @Override
            public void onMarkerDragEnd(Marker marker) {
                map.clear();
                LatLng point = marker.getPosition();
                latitude = point.latitude + "";
                longitude = point.longitude + "";
                map.addMarker(markerOptions(point));
            }

            @Override
            public void onMarkerDrag(Marker marker) {

            }
        });
    }

    MarkerOptions markerOptions(LatLng point){
        MarkerOptions options = new MarkerOptions();
        options.position(point);
        options.title("Shop Location");
        options.draggable(true);
        options.icon(BitmapDescriptorFactory
                .defaultMarker(BitmapDescriptorFactory.HUE_BLUE));
        return options;
    }
}
