package com.mindsarray.pay1distributor;

import com.mindsarray.pay1distributor.contenthelpers.DatabaseHelper;
import com.mindsarray.pay1distributor.services.RegistrationIntentService;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageInfo;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

public class SplashScreenActivity extends ChildActivity {

    private Context mContext = SplashScreenActivity.this;
    private Activity mActivity = SplashScreenActivity.this;

    private BroadcastReceiver mRegistrationBroadcastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        int SPLASH_TIME_OUT = 4000;

        setContentView(R.layout.activity_splash_screen);
        DatabaseHelper dbHelper = new DatabaseHelper(mContext);
        dbHelper.getWritableDatabase();
        dbHelper.close();
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            String version = pInfo.versionName;
            int verCode = pInfo.versionCode;
            if(!Storage.getSyncPreferencesBoolean(mContext, Storage.SHAREDPREFERENCE_LOGOUT_ON_APP_UPDATE_FLAG+verCode) ){
                Motley.logoutInBackground(mContext);
                Storage.setSyncPreferencesBoolean(mContext, Storage.SHAREDPREFERENCE_LOGOUT_ON_APP_UPDATE_FLAG+verCode, true);
            }
        }catch (Exception ec){

        }


        if (Motley.checkPlayServices(mActivity)) {

            mRegistrationBroadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if(intent.getExtras() != null) {
                        String regId = intent.getExtras().getString("registration_id");
                        Log.e("BroadcastReceiver", "" + regId);
                    }
                }
            };

            Intent intent = new Intent(mContext, RegistrationIntentService.class);
            startService(intent);

            new Handler().postDelayed(new Runnable() {

                @Override
                public void run() {
                    startActivity(new Intent(mContext,
                                DashboardActivity.class));
                    finish();
                }
            }, SPLASH_TIME_OUT);
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        Motley.checkPlayServices(mActivity);
        LocalBroadcastManager.getInstance(this).registerReceiver(mRegistrationBroadcastReceiver,
                new IntentFilter(Storage.REGISTRATION_COMPLETE));
    }

    @Override
    protected void onPause() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(mRegistrationBroadcastReceiver);
        super.onPause();
    }
}
