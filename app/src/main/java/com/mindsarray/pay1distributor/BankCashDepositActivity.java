package com.mindsarray.pay1distributor;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.widget.ExpandableListView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.adapters.BankDetailsAdapter;
import com.mindsarray.pay1distributor.models.BankDetailsPair;
import com.mindsarray.pay1distributor.util.NetworkConnection;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BankCashDepositActivity extends ChildActivity {

    private Context mContext = BankCashDepositActivity.this;
    private ExpandableListView mExpandableListView;
    private BankDetailsAdapter mBankDetailsAdapter;
    private ArrayList<BankDetailsPair> listDataHeader;
    private ArrayList<ArrayList<BankDetailsPair>> listChildData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_bank_cash_deposit);

        mExpandableListView = (ExpandableListView) findViewById(R.id.bankDetailsList);
//        prepareListData();
//        mBankDetailsAdapter = new BankDetailsAdapter(this, listDataHeader, listChildData);
//        mExpandableListView.setAdapter(mBankDetailsAdapter);

        loadBankAccounts();
    }

    private void adjustGroupSizes0(){
        int count = listChildData.size();

        int[] GEI = new int[count];
        float scale = mContext.getResources().getDisplayMetrics().density;
        int TGEI = 0;

        for(int i : GEI){
            if(mExpandableListView.isGroupExpanded(i)) {
                GEI[i] = 1;
                TGEI += 1;
            }
        }

        int pixels = (int) ((300 + TGEI * 250) * scale + 0.6f);
        mExpandableListView.getLayoutParams().height = pixels;
    }

    private void adjustGroupSizes(){
        int GEI1 = 0;
        int GEI2 = 0;
        int GEI3 = 0;
        int GEI4 = 0;
        float scale = mContext.getResources().getDisplayMetrics().density;
        if(mExpandableListView.isGroupExpanded(0))
            GEI1 = 1;
        if(mExpandableListView.isGroupExpanded(1))
            GEI2 = 1;
        if(mExpandableListView.isGroupExpanded(2))
            GEI3 = 1;
        if(mExpandableListView.isGroupExpanded(3))
            GEI4 = 1;
        int TGEI = GEI1 + GEI2 + GEI3 + GEI4;
        int pixels = (int) (300 * scale + 0.5f);
        switch(TGEI){
            case 0:
                pixels = (int) (300 * scale + 0.5f);
                break;
            case 1:
                pixels = (int) (550 * scale + 0.5f);
                break;
            case 2:
                pixels = (int) (800 * scale + 0.5f);
                break;
            case 3:
                pixels = (int) (1000 * scale + 0.5f);
                break;
            case 4:
                pixels = (int) (1250 * scale + 0.5f);
                break;
        }
        mExpandableListView.getLayoutParams().height = pixels;
    }

    @Override
    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        mExpandableListView.setIndicatorBounds(mExpandableListView.getRight()- 40, mExpandableListView.getWidth());
    }

    private void prepareListData(){
        listDataHeader = new ArrayList<BankDetailsPair>();
        listChildData = new ArrayList<ArrayList<BankDetailsPair>>();

        String iciciBankAccountNo = "054405006714";
        String bomBankAccountNo = "60117424079";
        String sbiBankAccountNo = "33619466476";
        String unionBankAccountNo = "582401010050258";

        String detailsKey = "BANK DETAILS: ";
        String detailsValue = "Cash Deposit, NEFT & IMPS";
        String accountName = "ACCOUNT NAME: ";
        String accountNo = "ACCOUNT No.: ";
        String accountType = "A/C TYPE: ";
        String accountTypeValue = "Current";
        String ifscCode = "IFSC Code: ";
        String branchName = "Branch Name: ";

        listDataHeader.add(new BankDetailsPair("ICICI BANK", "ACCOUNT NO.: " + iciciBankAccountNo));
        listDataHeader.add(new BankDetailsPair("BANK OF MAHARASHTRA", "ACCOUNT NO.: " + bomBankAccountNo));
        listDataHeader.add(new BankDetailsPair("STATE BANK OF INDIA", "ACCOUNT NO.: " + sbiBankAccountNo));
        listDataHeader.add(new BankDetailsPair("UNION BANK OF INDIA", "ACCOUNT NO.: " + unionBankAccountNo));

        ArrayList<BankDetailsPair> iciciListChildDataArray = new ArrayList<BankDetailsPair>();
        ArrayList<BankDetailsPair> bomListChildDataArray = new ArrayList<BankDetailsPair>();
        ArrayList<BankDetailsPair> sbiListChildDataArray = new ArrayList<BankDetailsPair>();
        ArrayList<BankDetailsPair> unionListChildDataArray = new ArrayList<BankDetailsPair>();

        iciciListChildDataArray.add(new BankDetailsPair(detailsKey, detailsValue));
        iciciListChildDataArray.add(new BankDetailsPair(accountName, "Mindsarray Technologies Pvt. Ltd."));
        iciciListChildDataArray.add(new BankDetailsPair(accountNo, iciciBankAccountNo));
        iciciListChildDataArray.add(new BankDetailsPair(accountType, accountTypeValue));
        iciciListChildDataArray.add(new BankDetailsPair(ifscCode, "ICIC0000544"));
        iciciListChildDataArray.add(new BankDetailsPair(branchName, "MIDC"));

        bomListChildDataArray.add(new BankDetailsPair(detailsKey, detailsValue));
        bomListChildDataArray.add(new BankDetailsPair(accountName, "Mindsarray Technologies Pvt. Ltd."));
        bomListChildDataArray.add(new BankDetailsPair(accountNo, bomBankAccountNo));
        bomListChildDataArray.add(new BankDetailsPair(accountType, accountTypeValue));
        bomListChildDataArray.add(new BankDetailsPair(ifscCode, "MAHB0000117"));
        bomListChildDataArray.add(new BankDetailsPair(branchName, "Malad (W)"));

        sbiListChildDataArray.add(new BankDetailsPair(detailsKey, detailsValue));
        sbiListChildDataArray.add(new BankDetailsPair(accountName, "Pay1"));
        sbiListChildDataArray.add(new BankDetailsPair(accountNo, sbiBankAccountNo));
        sbiListChildDataArray.add(new BankDetailsPair(accountType, accountTypeValue));
        sbiListChildDataArray.add(new BankDetailsPair(ifscCode, "SBIN0006055"));
        sbiListChildDataArray.add(new BankDetailsPair(branchName, "Gokuldham (Goregaon - E)"));

        unionListChildDataArray.add(new BankDetailsPair(detailsKey, detailsValue));
        unionListChildDataArray.add(new BankDetailsPair(accountName, "Mindsarray Technologies Pvt. Ltd."));
        unionListChildDataArray.add(new BankDetailsPair(accountNo, unionBankAccountNo));
        unionListChildDataArray.add(new BankDetailsPair(accountType, accountTypeValue));
        unionListChildDataArray.add(new BankDetailsPair(ifscCode, "UBIN0558249"));
        unionListChildDataArray.add(new BankDetailsPair(branchName, "Link Road, Malad West"));

        listChildData.add(iciciListChildDataArray);
        listChildData.add(bomListChildDataArray);
        listChildData.add(sbiListChildDataArray);
        listChildData.add(unionListChildDataArray);
    }

    public void loadBankAccounts(){
        Map requestParams = new HashMap();

        requestParams.put("method", "bankAccounts");

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Fetching bank accounts..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray description = jsonObjectResponse.getJSONArray("description");
                        Log.e("description", description.toString());

                        listDataHeader = new ArrayList<BankDetailsPair>();
                        listChildData = new ArrayList<ArrayList<BankDetailsPair>>();

                        for (int i = 0; i < description.length(); i++) {
                            JSONObject jObj = description.getJSONObject(i);
                            String bankName = jObj.getString("bank");
                            String Account_No = jObj.getString("account_no");
                            String AccountName = jObj.getString("account_name");
                            String BranchName = jObj.getString("branch");
                            String IFSCCode = jObj.getString("ifsc");
                            String ACType = jObj.getString("account_type");

                            listDataHeader.add(new BankDetailsPair(bankName,
                                    "ACCOUNT NO.: " + Account_No));

                            String detailsKey = "BANK DETAILS: ";
                            String detailsValue = "Cash Deposit, NEFT & IMPS";
                            String accountName = "ACCOUNT NAME: ";
                            String accountNo = "ACCOUNT No.: ";
                            String accountType = "A/C TYPE: ";
                            String accountTypeValue = "Current";
                            String ifscCode = "IFSC Code: ";
                            String branchName = "Branch Name: ";

                            ArrayList<BankDetailsPair> listChildDataArray = new ArrayList<BankDetailsPair>();

                            listChildDataArray.add(new BankDetailsPair(
                                    detailsKey, detailsValue));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountName, AccountName));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountNo, Account_No));
                            listChildDataArray.add(new BankDetailsPair(
                                    accountType, ACType));
                            listChildDataArray.add(new BankDetailsPair(
                                    ifscCode, IFSCCode));
                            listChildDataArray.add(new BankDetailsPair(
                                    branchName, BranchName));

                            listChildData.add(listChildDataArray);
                        }
                        Log.e("BankCashActivity", "listChildData generated");
                        mBankDetailsAdapter = new BankDetailsAdapter(mContext, listDataHeader, listChildData);
                        mExpandableListView.setAdapter(mBankDetailsAdapter);
                        mExpandableListView.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener(){

                            @Override
                            public void onGroupExpand(int groupPosition) {
                                adjustGroupSizes0();
                            }

                        });

                        mExpandableListView.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener(){

                            @Override
                            public void onGroupCollapse(int groupPosition) {
                                adjustGroupSizes0();
                            }

                        });
                    }

                }
                catch(Exception e) {
                    Log.e("bankAccounts", e.getMessage() + "");
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };

        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(requestParams, responseListener, errorBlock);
    }
}
