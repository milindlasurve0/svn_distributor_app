package com.mindsarray.pay1distributor.contenthelpers;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Created by rohan on 29/5/15.
 */
public class DatabaseHelper extends SQLiteAssetHelper {

    private static final String TAG = "DatabaseHelper";

    private static final String DATABASE_NAME = "distributor.db";
    private static final String SYNC_DATABASE_NAME = "distributor_sync.db";
    private static final int DATABASE_VERSION = 3;

    private Context dbContext;

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);

        setForcedUpgrade();
        dbContext = context;
        Log.i(TAG, "db constructor");
    }



    public boolean cloneDatabase(String inputDBName, String outputDBName) {
        try{
            File dbFile = dbContext.getDatabasePath(inputDBName);//new File(dbPath + "/" + inputDBName);
            InputStream input = new FileInputStream(dbFile);

            OutputStream output = new FileOutputStream(dbFile.getAbsoluteFile().getParentFile().getAbsolutePath() + "/" + outputDBName);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = input.read(buffer)) > 0){
                output.write(buffer, 0, length);
            }

            output.flush();
            output.close();
            input.close();
            return true;
        }
        catch(IOException e){
            e.printStackTrace();
            return false;
        }
    }
}