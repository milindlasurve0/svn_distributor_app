package com.mindsarray.pay1distributor.contenthelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by rohan on 31/10/15.
 */
public class NotificationsDataSource {

    private static final String TAG = "NotificationsDataSource";

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private Context mContext;

    public static final String TABLE_NOTIFICATIONS = "Notifications";

    public static final String NOTIFICATION_DATA = "data";
    public static final String NOTIFICATION_TYPE = "type";
    public static final String NOTIFICATION_TIME = "time";

    public String data;
    public String type;
    public String time;

    public NotificationsDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
        mContext = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.e(TAG, "db opened");
    }

    public void close() {
        dbHelper.close();
    }

    public String data(){
        return data;
    }

    public String type(){
        return type;
    }

    public String time(){
        return time;
    }

    public void set_data(String data){
        this.data = data;
    }

    public void set_type(String type){
        this.type = type;
    }

    public void set_time(String time){
        this.time = time;
    }

    public NotificationsDataSource notification(Cursor cursor){
        NotificationsDataSource notification = new NotificationsDataSource(mContext);
        Log.e("Notification cursor", cursor.getCount() + "");

        if(cursor.getCount() != 0) {
            notification.set_data(cursor.getString(cursor.getColumnIndex("data")));
            notification.set_type(cursor.getString(cursor.getColumnIndex("type")));
            notification.set_time(cursor.getString(cursor.getColumnIndex("time")));
        }
        else{
            notification.set_data(null);
            notification.set_type(null);
            notification.set_time(null);
        }
        return notification;
    }

    public NotificationsDataSource save(String data, String type, String time){
        try{
            ContentValues values = new ContentValues();
            values.put(NOTIFICATION_DATA, data);
            values.put(NOTIFICATION_TYPE, type);
            values.put(NOTIFICATION_TIME, time);

            long insertId = database.insert(TABLE_NOTIFICATIONS, null, values);

            Cursor cursor = database.query(TABLE_NOTIFICATIONS, null,
                    " _id = '" + insertId + "'",
                    null, null, null, null);
            cursor.moveToFirst();

            return notification(cursor);
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<NotificationsDataSource> getPaginatedNotifications(int page, int resultsPerPage){
        ArrayList<NotificationsDataSource> notifications = new ArrayList<>();

        Cursor cursor = database.query(false, TABLE_NOTIFICATIONS,
                null, null, null, null, null, NOTIFICATION_TIME
                        + " DESC ", (page - 1) * resultsPerPage + ","
                        + resultsPerPage);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            notifications.add(notification(cursor));
            cursor.moveToNext();
        }

        return notifications;
    }

    public NotificationsDataSource getLastNotification(){
        Cursor cursor = database.query(false, TABLE_NOTIFICATIONS,
                null, null, null, null, null, NOTIFICATION_TIME
                        + " DESC ", "1");
        cursor.moveToFirst();

        return notification(cursor);
    }
}
