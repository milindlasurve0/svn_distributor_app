package com.mindsarray.pay1distributor.contenthelpers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by rohan on 29/5/15.
 */
public class SalesmenDataSource {

    private static final String TAG = "SalesmenDataSource";

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private Context mContext;

    public static final String TABLE_SALESMEN = "Salesmen";

    public static final String SALESMAN_ID = "salesman_id";
    public static final String SALESMAN_DISTRIBUTOR_ID = "distributor_id";
    public static final String SALESMAN_MOBILE = "mobile";
    public static final String SALESMAN_NAME = "name";

    public String salesman_id;
    public String distributor_id;
    public String mobile;
    public String name;

    public SalesmenDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
        mContext = context;
    }

    public String salesman_id(){
        return salesman_id;
    }

    public String distributor_id(){
        return distributor_id;
    }

    public String mobile(){
        return mobile;
    }

    public String name(){
        return name;
    }

    public void set_salesman_id(String salesman_id){
        this.salesman_id = salesman_id;
    }

    public void set_distributor_id(String distributor_id){
        this.distributor_id = distributor_id;
    }

    public void set_mobile(String mobile){
        this.mobile = mobile;
    }

    public void set_name(String name){
        this.name = name;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.e(TAG, "db opened");
    }

    public void close() {
        dbHelper.close();
    }

    public ArrayList<SalesmenDataSource> get_many_by(String distributor_id){
        ArrayList<SalesmenDataSource> salesmen = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_SALESMEN, null,
                    SALESMAN_DISTRIBUTOR_ID + " = '" + distributor_id + "'",
                    null, null, null,
                    SALESMAN_ID + " DESC");
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                salesmen.add(salesman(cursor));
                cursor.moveToNext();
            }

        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
        return salesmen;
    }

    public SalesmenDataSource salesman(Cursor cursor){
        SalesmenDataSource salesman = new SalesmenDataSource(mContext);
        Log.e("Salesman cursor", cursor.getCount() + "");
        if(cursor.getCount() != 0) {
            salesman.set_salesman_id(cursor.getString(1));
            salesman.set_distributor_id(cursor.getString(2));
            salesman.set_name(cursor.getString(3));
            salesman.set_mobile(cursor.getString(4));
        }
        else{
            salesman.set_salesman_id(null);
            salesman.set_distributor_id(null);
            salesman.set_name(null);
            salesman.set_mobile(null);
        }

        return salesman;
    }

    public SalesmenDataSource get(String salesman_id){
        Cursor cursor;

        cursor = database.query(TABLE_SALESMEN, null,
                SALESMAN_ID + " = '" + salesman_id + "'",
                null, null, null, null);
        cursor.moveToFirst();

        return salesman(cursor);
    }

    public SalesmenDataSource save(JSONObject salesman) {
        try {
            Iterator<?> keys = salesman.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                if (salesman.get(key).toString().equals("null")) {
                    salesman.put(key, "");
                }
            }
            if (get(salesman.getString("id")).salesman_id == null) {
                ContentValues values = new ContentValues();
                values.put(SALESMAN_ID, salesman.getString("id"));
                values.put(SALESMAN_DISTRIBUTOR_ID, salesman.getString("dist_id"));
                values.put(SALESMAN_MOBILE, salesman.getString("mobile"));
                values.put(SALESMAN_NAME, salesman.getString("name"));

                database.insert(TABLE_SALESMEN, null, values);
            } else {
                String updateQuery = "update " + TABLE_SALESMEN + " set " +
                        SALESMAN_DISTRIBUTOR_ID + " = '" + salesman.getString("dist_id") + "', " +
                        SALESMAN_MOBILE + " = '" + salesman.getString("mobile") + "', " +
                        SALESMAN_NAME + " = '" + salesman.getString("name") + "' " +
                        " where " + SALESMAN_ID + " = '" + salesman.getString("id") + "'";
                Log.e("Salesman update query", updateQuery);
                database.execSQL(updateQuery);
            }
            return get(salesman.getString("id"));
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("Salesman update", e.getMessage() + "");
            return null;
        }
    }

    public void save(JSONArray salesmen){
        try {
            for (int i = 0; i < salesmen.length(); i++) {
                JSONObject salesman = salesmen.getJSONObject(i).getJSONObject("s");

                save(salesman);
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }
}
