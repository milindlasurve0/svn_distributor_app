package com.mindsarray.pay1distributor.contenthelpers;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.mindsarray.pay1distributor.models.Document;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 * Created by rohan on 29/5/15.
 */
public class RetailersDataSource {

    private static final String TAG = "RetailersDataSource";

    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private Context mContext;

    public static final String TABLE_RETAILERS = "Retailers";
    public static final String TABLE_RETAILER_DOCUMENTS = "Retailer_documents";

    public static final String ID = "_id";
    public static final String RETAILER_ID = "retailer_id";
    public static final String RETAILER_DISTRIBUTOR_ID = "distributor_id";
    public static final String RETAILER_SALESMAN_ID = "salesman_id";
    public static final String RETAILER_MOBILE = "mobile";
    public static final String RETAILER_NAME = "name";
    public static final String RETAILER_SHOP_NAME = "shop_name";
    public static final String RETAILER_SHOP_TYPE = "shop_type";
    public static final String RETAILER_LOCATION_TYPE = "location_type";
    public static final String RETAILER_RENTAL_FLAG = "rental_flag";
    public static final String RETAILER_ADDRESS = "address";
    public static final String RETAILER_AREA = "area";
    public static final String RETAILER_CITY = "city";
    public static final String RETAILER_STATE = "state";
    public static final String RETAILER_PIN_CODE = "pin_code";
    public static final String RETAILER_LATITUDE = "latitude";
    public static final String RETAILER_LONGITUDE = "longitude";
    public static final String RETAILER_VERIFY_FLAG = "verify_flag";
    public static final String RETAILER_TRIAL_FLAG = "trial_flag";
    public static final String RETAILER_CREATED_ON = "created_on";

    public static final String DOCUMENT_TYPE = "type";
    public static final String DOCUMENT_URI = "uri";
    public static final String DOCUMENT_VERIFY_FLAG = "verify_flag";
    public static final String DOCUMENT_CREATED_ON = "created_on";
    public static final String DOCUMENT_COMMENT = "comment";

    public String retailer_id;
    public String id;
    public String distributor_id;
    public String salesman_id;
    public String mobile;
    public String name;
    public String shop_name;
    public String shop_type;
    public String location_type;
    public String rental_flag;
    public String address;
    public String area;
    public String city;
    public String state;
    public String pin_code;
    public String latitude;
    public String longitude;
    public String verify_flag;
    public String trial_flag;
    public String created_on;
    public String update_insert_flag;

    public Map documents;

    public RetailersDataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
        mContext = context;
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
        Log.e(TAG, "db opened");
    }

    public void close() {
        dbHelper.close();
    }

    public String retailer_id(){
        return retailer_id;
    }

    public String distributor_id(){
        return distributor_id;
    }

    public String salesman_id(){
        return salesman_id;
    }

    public String mobile(){
        return mobile;
    }

    public String name(){
        return name;
    }

    public String shop_name(){
        return shop_name;
    }

    public String shop_type(){
        return shop_type;
    }

    public String location_type(){
        return location_type;
    }

    public String rental_flag(){
        return rental_flag;
    }

    public String address(){
        return address;
    }

    public String area(){
        return area;
    }

    public String city(){
        return city;
    }

    public String state(){
        return state;
    }

    public String pin_code(){
        return pin_code();
    }

    public String latitude(){
        return latitude;
    }

    public String longitude(){
        return longitude;
    }

    public String verify_flag(){
        return verify_flag;
    }

    public String trial_flag(){
        return trial_flag;
    }

    public String created_on(){
        return created_on;
    }

    public String update_insert_flag(){
        return update_insert_flag;
    }

    public Map documents(){
        Map documents = new HashMap<String, ArrayList<Document>>();
        ArrayList<Document> idProofList = new ArrayList<>();
        ArrayList<Document> addressProofList = new ArrayList<>();
        ArrayList<Document> shopPhotosList = new ArrayList<>();
        try {
            Cursor cursor;
            open();
            cursor = database.query(TABLE_RETAILER_DOCUMENTS, null,
                    RETAILER_ID + " = '" + this.retailer_id + "'",
                    null, null, null,
                    RETAILER_ID + " DESC");

            Log.e("Query: ", cursor.toString());
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                if (cursor.getCount() != 0) {
                    if (cursor.getString(2).equals("PAN_CARD"))
                        idProofList.add(new Document(cursor.getInt(0), "PAN_CARD", cursor.getString(3), cursor.getString(4), cursor.getString(5),cursor.getString(6)));
                    if (cursor.getString(2).equals("ADDRESS_PROOF"))
                        addressProofList.add(new Document(cursor.getInt(0), "ADDRESS_PROOF", cursor.getString(3), cursor.getString(4), cursor.getString(5),cursor.getString(6)));
                    if (cursor.getString(2).equals("SHOP_PHOTO"))
                        shopPhotosList.add(new Document(cursor.getInt(0), "SHOP_PHOTO", cursor.getString(3), cursor.getString(4), cursor.getString(5),cursor.getString(6)));
                }
                cursor.moveToNext();
            }
            close();
        }
        catch(Exception e){
            e.printStackTrace();
        }
        documents.put("idProof", idProofList);
        documents.put("addressProof", addressProofList);
        documents.put("shopPhotos", shopPhotosList);

        return documents;
    };

    public void set_id(String id){
        this.id = id;
    }

    public void set_retailer_id(String retailer_id){
        this.retailer_id = retailer_id;
    }

    public void set_distributor_id(String distributor_id){
        this.distributor_id = distributor_id;
    }

    public void set_salesman_id(String salesman_id){
        this.salesman_id = salesman_id;
    }

    public void set_mobile(String mobile){
        this.mobile = mobile;
    }

    public void set_name(String name){
        this.name = name;
    }

    public void set_shop_name(String shop_name){
        this.shop_name = shop_name;
    }

    public void set_shop_type(String shop_type){
        this.shop_type = shop_type;
    };

    public void set_location_type(String location_type){
        this.location_type = location_type;
    };

    public void set_rental_flag(String rental_flag){
        this.rental_flag = rental_flag;
    };

    public void set_address(String address){
        this.address = address;
    };

    public void set_area(String area){
        this.area = area;
    };

    public void set_city(String city){
        this.city = city;
    };

    public void set_state(String state){
        this.state = state;
    };

    public void set_pin_code(String pin_code){
        this.pin_code = pin_code;
    };

    public void set_latitude(String latitude){
        this.latitude = latitude;
    };

    public void set_longitude(String longitude){
        this.longitude = longitude;
    };

    public void set_verify_flag(String verify_flag){
        this.verify_flag = verify_flag;
    };

    public void set_trial_flag(String trial_flag){
        this.trial_flag = trial_flag;
    };

    public void set_created_on(String created_on){
        this.created_on = created_on;
    };

    public void set_update_insert_flag(String update_insert_flag){
        this.update_insert_flag = update_insert_flag;
    };

    public void set_documents(Map documents){
        this.documents = documents;
    }

    public ArrayList<RetailersDataSource> get_many_by(String distributor_id){
        ArrayList<RetailersDataSource> retailers = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_DISTRIBUTOR_ID + " = '" + distributor_id + "'",
                    null, null, null,
                    ID + " ASC");
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                retailers.add(retailer(cursor));
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
        return retailers;
    }

    public ArrayList<RetailersDataSource> get_many_by(String distributor_id, String searchTerm){
        ArrayList<RetailersDataSource> retailers = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_DISTRIBUTOR_ID + " = '" + distributor_id + "'" +
                            " and (" + RETAILER_MOBILE + " like '%" + searchTerm + "%'" +
                            //                " or " + RETAILER_NAME + " like '%" + searchTerm + "%'" +
                            " or " + RETAILER_SHOP_NAME + " like '%" + searchTerm + "%')",
                    null, null, null,
                    ID + " ASC");
            Log.e("Query: ", cursor.toString());
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                retailers.add(retailer(cursor));
                cursor.moveToNext();
            }
        }
        catch(Exception e) {
            e.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
        return retailers;
    }

    public ArrayList<RetailersDataSource> get_many_by_salesman(String salesman_id){
        ArrayList<RetailersDataSource> retailers = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_SALESMAN_ID + " = '" + salesman_id + "'",
                    null, null, null,
                    ID + " ASC");
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                retailers.add(retailer(cursor));
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
        return retailers;
    }

    public ArrayList<RetailersDataSource> get_many_by_salesman(String salesman_id, String searchTerm){
        ArrayList<RetailersDataSource> retailers = new ArrayList();
        Cursor cursor = null;
        try {
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_SALESMAN_ID + " = '" + salesman_id + "'" +
                            " and (" + RETAILER_MOBILE + " like '%" + searchTerm + "%'" +
                            //                " or " + RETAILER_NAME + " like '%" + searchTerm + "%'" +
                            " or " + RETAILER_SHOP_NAME + " like '%" + searchTerm + "%')",
                    null, null, null,
                    ID + " ASC");
            Log.e("Query: ", cursor.toString());
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                retailers.add(retailer(cursor));
                cursor.moveToNext();
            }
        }
        catch(Exception e){
            e.printStackTrace();
        }
        finally {
            if(cursor != null)
                cursor.close();
        }
        return retailers;
    }

    public RetailersDataSource retailer(Cursor cursor){
        RetailersDataSource retailer = new RetailersDataSource(mContext);

        if(cursor.getCount() != 0) {
            retailer.set_id(cursor.getString(0));
            retailer.set_retailer_id(cursor.getString(1));
            retailer.set_distributor_id(cursor.getString(2));
            retailer.set_salesman_id(cursor.getString(3));
            retailer.set_mobile(cursor.getString(4));
            retailer.set_name(cursor.getString(5));
            retailer.set_shop_name(cursor.getString(6));
            retailer.set_shop_type(cursor.getString(7));
            retailer.set_location_type(cursor.getString(8));
            retailer.set_rental_flag(cursor.getString(9));
            retailer.set_address(cursor.getString(10));
            retailer.set_area(cursor.getString(11));
            retailer.set_city(cursor.getString(12));
            retailer.set_state(cursor.getString(13));
            retailer.set_pin_code(cursor.getString(14));
            retailer.set_latitude(cursor.getString(15));
            retailer.set_longitude(cursor.getString(16));
            retailer.set_verify_flag(cursor.getString(17));
            retailer.set_trial_flag(cursor.getString(18));
            retailer.set_created_on(cursor.getString(19));
            retailer.set_update_insert_flag(null);
        }
        else{
            retailer.set_id(null);
            retailer.set_retailer_id(null);
            retailer.set_distributor_id(null);
            retailer.set_salesman_id(null);
            retailer.set_mobile(null);
            retailer.set_name(null);
            retailer.set_shop_name(null);
            retailer.set_location_type(null);
            retailer.set_rental_flag(null);
            retailer.set_address(null);
            retailer.set_area(null);
            retailer.set_city(null);
            retailer.set_state(null);
            retailer.set_pin_code(null);
            retailer.set_latitude(null);
            retailer.set_longitude(null);
            retailer.set_verify_flag(null);
            retailer.set_trial_flag(null);
            retailer.set_created_on(null);
            retailer.set_update_insert_flag(null);
        }

        return retailer;
    }

    public RetailersDataSource get(String retailer_id){
        Cursor cursor;

        cursor = database.query(TABLE_RETAILERS, null,
                RETAILER_ID + " = '" + retailer_id + "'",
                null, null, null, null);
        cursor.moveToFirst();

        RetailersDataSource retailer = retailer(cursor);
        if(cursor != null)
            cursor.close();
        return retailer;
    }

    public RetailersDataSource get_by_mobile(String retailer_mobile){
        Cursor cursor;

        cursor = database.query(TABLE_RETAILERS, null,
                RETAILER_MOBILE + " = '" + retailer_mobile + "'",
                null, null, null, null);
        cursor.moveToFirst();

        RetailersDataSource retailer = retailer(cursor);
        if(cursor != null)
            cursor.close();
        return retailer;
    }

    public void save(JSONArray retailers){
        try {
            int j = 0;
            for (int i = 0; i < retailers.length(); i++) {
                j++;
                JSONObject retailer = retailers.getJSONObject(i);

                RetailersDataSource savedRetailer = save(retailer, "");
                if(savedRetailer.update_insert_flag.equals("0")){
                    j--;
                }
                if(j % 100 == 0){
                    Intent intent = new Intent("last_100_retailers");
                    mContext.sendBroadcast(intent);
                }
            }
            Intent intent = new Intent("last_100_retailers");
            mContext.sendBroadcast(intent);
        }
        catch(Exception e){
            e.printStackTrace();
        }
    }

    public RetailersDataSource save(JSONObject retailer){
        return save(retailer, null);
    }

    public RetailersDataSource save(JSONObject retailer, String updateOrInsert){
        try {
            Iterator<?> keys = retailer.keys();

            while( keys.hasNext() ) {
                String key = (String)keys.next();
                if (retailer.get(key).toString().equals("null")) {
                    retailer.put(key, "");
                }
            }
            int insert = 0;
            if(get(retailer.getString("id")).retailer_id == null) {
                ContentValues values = new ContentValues();
                values.put(RETAILER_ID, retailer.getString("id"));
                values.put(RETAILER_DISTRIBUTOR_ID, retailer.getString("parent_id"));
                values.put(RETAILER_SALESMAN_ID, retailer.getString("maint_salesman"));
                values.put(RETAILER_MOBILE, retailer.getString("mobile"));
                values.put(RETAILER_NAME, retailer.getString("name"));
                values.put(RETAILER_SHOP_NAME, retailer.getString("shopname"));
                if(retailer.has("shop_type")) {
                    values.put(RETAILER_SHOP_TYPE, retailer.getString("shop_type"));
                    values.put(RETAILER_LOCATION_TYPE, retailer.getString("location_type"));
                    values.put(RETAILER_RENTAL_FLAG, retailer.getString("rental_flag"));
                    values.put(RETAILER_ADDRESS, retailer.getString("address"));
                    values.put(RETAILER_AREA, retailer.getString("area"));
                    values.put(RETAILER_CITY, retailer.getString("city"));
                    values.put(RETAILER_STATE, retailer.getString("state"));
                    values.put(RETAILER_PIN_CODE, retailer.getString("pin"));
                    values.put(RETAILER_LATITUDE, retailer.getString("latitude"));
                    values.put(RETAILER_LONGITUDE, retailer.getString("longitude"));
                }
                values.put(RETAILER_VERIFY_FLAG, retailer.getString("verify_flag"));
                values.put(RETAILER_TRIAL_FLAG, retailer.getString("trial_flag"));
                values.put(RETAILER_CREATED_ON, retailer.getString("created"));

                database.insert(TABLE_RETAILERS, null, values);

                insert = 1;
            }
            else {
                String updateQuery = "update " + TABLE_RETAILERS + " set " +
                        RETAILER_DISTRIBUTOR_ID + " = '" + retailer.getString("parent_id") + "', " +
                        RETAILER_SALESMAN_ID + " = '" + retailer.getString("maint_salesman") + "', " +
                        RETAILER_MOBILE + " = '" + retailer.getString("mobile") + "', " +
                        RETAILER_NAME + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("name")) + ", " +
                        RETAILER_SHOP_NAME + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("shopname")) + ", ";
                if(retailer.has("shop_type")) {
                    updateQuery += RETAILER_SHOP_TYPE + " = '" + retailer.getString("shop_type") + "', " +
                            RETAILER_LOCATION_TYPE + " = '" + retailer.getString("location_type") + "', " +
                            RETAILER_RENTAL_FLAG + " = '" + retailer.getString("rental_flag") + "', " +
                            RETAILER_ADDRESS + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("address")) + ", " +
                            RETAILER_AREA + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("area")) + ", " +
                            RETAILER_CITY + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("city")) + ", " +
                            RETAILER_STATE + " = " + DatabaseUtils.sqlEscapeString(retailer.getString("state")) + ", " +
                            RETAILER_PIN_CODE + " = '" + retailer.getString("pin") + "', " +
                            RETAILER_LATITUDE + " = '" + retailer.getString("latitude") + "', " +
                            RETAILER_LONGITUDE + " = '" + retailer.getString("longitude") + "', ";
                }
                updateQuery += RETAILER_VERIFY_FLAG + " = '" + retailer.getString("verify_flag") + "', " +
                            RETAILER_TRIAL_FLAG + " = '" + retailer.getString("trial_flag") + "', " +
                            RETAILER_CREATED_ON + " = '" + retailer.getString("created") + "' " +
                            " where " + RETAILER_ID + " = '" + retailer.getString("id") + "'";

                database.execSQL(updateQuery);

                database.execSQL("delete from " + TABLE_RETAILER_DOCUMENTS
                        + " where " + RETAILER_ID + " = '" + retailer.getString("id") + "'");
            }

            if(retailer.has("kyc_states")){
                JSONArray retailerKYCStates = retailer
                        .getJSONArray("kyc_states");

                String document_state = "", verify_state = "", reason = "";
                for (int i = 0; i < retailerKYCStates.length(); i++) {
                    JSONArray images = retailerKYCStates.getJSONObject(i).getJSONObject("rks").getJSONArray("documents");
                    document_state = retailerKYCStates.getJSONObject(i)
                            .getJSONObject("rks").getString("document_state");
                    if(document_state.equals("1")){
                        verify_state = "-1";
                    }
                    else if(document_state.equals("0")){
                        verify_state = "0";
                    }
                    else if(document_state.equals("2")){
                        verify_state = "1";
                    }

                    reason = retailerKYCStates.getJSONObject(i)
                            .getJSONObject("rks").getString("comment");

                    String type = "", url = "";
                    for (int j = 0; j < images.length(); j++) {
                        type = images.getJSONObject(j)
                                .getJSONObject("rd").getString("type");
                        url = images.getJSONObject(j)
                                .getJSONObject("rd").getString("image_name");

                        ContentValues documentValues = new ContentValues();
                        documentValues.put(RETAILER_ID, retailer.getString("id"));
                        documentValues.put(DOCUMENT_TYPE, type);
                        documentValues.put(DOCUMENT_URI, url);
                        documentValues.put(DOCUMENT_VERIFY_FLAG, verify_state);
                        documentValues.put(DOCUMENT_CREATED_ON, "");
                        documentValues.put(DOCUMENT_COMMENT, reason);

                        database.insert(TABLE_RETAILER_DOCUMENTS, null, documentValues);
                    }
                }
            }

//            if(retailer.has("image_detail")) {
//                Object imageDetails = retailer.get("image_detail");
//                if(imageDetails instanceof JSONArray) {
//                    JSONArray retailerDetails = retailer.getJSONArray("image_detail");
//                    String type = "", url = "", verify_flag = "", created = "",comment="";
//
//                    for (int i = 0; i < retailerDetails.length(); i++) {
//                        type = retailerDetails.getJSONObject(i).getJSONObject("rd").getString("type");
//                        url = retailerDetails.getJSONObject(i).getJSONObject("rd").getString("image_name");
//                        verify_flag = retailerDetails.getJSONObject(i).getJSONObject("rd").getString("verify_flag");
//                        created = retailerDetails.getJSONObject(i).getJSONObject("rd").getString("created");
//                        comment = retailerDetails.getJSONObject(i).getJSONObject("rd").getString("comment");
//
//                        ContentValues documentValues = new ContentValues();
//                        documentValues.put(RETAILER_ID, retailer.getString("id"));
//                        documentValues.put(DOCUMENT_TYPE, type);
//                        documentValues.put(DOCUMENT_URI, url);
//                        documentValues.put(DOCUMENT_VERIFY_FLAG, verify_flag);
//                        documentValues.put(DOCUMENT_CREATED_ON, created);
//                        documentValues.put(DOCUMENT_COMMENT, comment);
//
//
//                        database.insert(TABLE_RETAILER_DOCUMENTS, null, documentValues);
//                    }
//                }
//                else if(imageDetails instanceof JSONObject){
//                    JSONObject retailerDetails = retailer.getJSONObject("image_detail");
//                    String type = "", url = "", verify_flag = "", created = "",comment="";
//
//                    Iterator<?> documentKeys = retailerDetails.keys();
//
//                    while( documentKeys.hasNext() ) {
//                        String key = (String) documentKeys.next();
//                        JSONObject documents = retailerDetails.getJSONObject(key).getJSONObject("rd");
//                        type = documents.getString("type");
//                        url = documents.getString("image_name");
//                        verify_flag = documents.getString("verify_flag");
//                        created = documents.getString("created");
//                        comment=documents.getString("comment");
//                        ContentValues documentValues = new ContentValues();
//                        documentValues.put(RETAILER_ID, retailer.getString("id"));
//                        documentValues.put(DOCUMENT_TYPE, type);
//                        documentValues.put(DOCUMENT_URI, url);
//                        documentValues.put(DOCUMENT_VERIFY_FLAG, verify_flag);
//                        documentValues.put(DOCUMENT_CREATED_ON, created);
//                        documentValues.put(DOCUMENT_COMMENT, comment);
//
//                        database.insert(TABLE_RETAILER_DOCUMENTS, null, documentValues);
//                    }
//                }
//            }

            RetailersDataSource savedRetailer = get(retailer.getString("id"));
            if(updateOrInsert != null){
                savedRetailer.set_update_insert_flag("" + insert);
            }
            return savedRetailer;
        }
        catch(Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public ArrayList<RetailersDataSource> getInTrialRetailers(){
        ArrayList<RetailersDataSource> retailers = new ArrayList<>();
        Cursor cursor;
        if (Storage.user_group(mContext) == Storage.DISTRIBUTOR){
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_DISTRIBUTOR_ID + " = '" + Storage.getPersistentString(mContext,
                            Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID) + "'" +
                            " AND " + RETAILER_TRIAL_FLAG + " IN ('1', '2')" +
                            " AND " + RETAILER_VERIFY_FLAG + " = '0'",
                    null, null, null, RETAILER_TRIAL_FLAG + " DESC, " + RETAILER_CREATED_ON + " ASC");
        }
        else if (Storage.user_group(mContext) == Storage.SALESMAN){
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_SALESMAN_ID + " = '" + Storage.getPersistentString(mContext,
                            Storage.SHAREDPREFERENCE_SALESMAN_ID) + "'" +
                            " AND " + RETAILER_TRIAL_FLAG + " IN ('1', '2')" +
                            " AND " + RETAILER_VERIFY_FLAG + " = '0'",
                    null, null, null, RETAILER_TRIAL_FLAG + " DESC, " + RETAILER_CREATED_ON + " ASC");
        }
        else
            return retailers;

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            retailers.add(retailer(cursor));
            cursor.moveToNext();
        }
        cursor.close();

        return retailers;
    }

    public ArrayList<RetailersDataSource> getNext100Retailers(String id){
        ArrayList<RetailersDataSource> retailers = new ArrayList<>();
        Cursor cursor;
        if(id == null)
            id = "0";

        if (Storage.user_group(mContext) == Storage.DISTRIBUTOR){
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_DISTRIBUTOR_ID + " = '" + Storage.getPersistentString(mContext,
                            Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID) + "'" +
                            " AND " + ID + " > " + id,
                    null, null, null, ID + " ASC", "100");
        }
        else if (Storage.user_group(mContext) == Storage.SALESMAN){
            cursor = database.query(TABLE_RETAILERS, null,
                    RETAILER_SALESMAN_ID + " = '" + Storage.getPersistentString(mContext,
                            Storage.SHAREDPREFERENCE_SALESMAN_ID) + "'" +
                            " AND " + ID + " > " + id,
                    null, null, null, ID + " ASC", "100");
        }
        else
            return retailers;
        Log.e("Fetching", "100 retailers");
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            retailers.add(retailer(cursor));
            cursor.moveToNext();
        }
        cursor.close();

        return retailers;
    }

    public boolean kycPendingReminderFlag(){
        long trial_period = Long.parseLong(Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_TRIAL_PERIOD));
        long daysLeft = Motley.daysSince(mContext, created_on);
        if(verify_flag.equals("0") && trial_flag.equals("1") && daysLeft < trial_period / 2)
            return true;
        else
            return false;
    }
}
