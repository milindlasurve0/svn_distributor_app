package com.mindsarray.pay1distributor;

import android.accounts.Account;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.mindsarray.pay1distributor.adapters.NameMobileAutoCompleteAdapter;
import com.mindsarray.pay1distributor.adapters.RetailersAdapter;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.Storage;

import java.util.ArrayList;

public class KYCPendingActivity extends BaseActivity {

    private Context mContext = KYCPendingActivity.this;
    private RetailersDataSource mRetailersDataSource, retailer;
    private ArrayList<RetailersDataSource> retailers;

    private RetailersAdapter mRetailersAdapter;
    ListView retailersListView;

    private AutoCompleteTextView retailerMobileAutoCompleteTextView;

    Account syncAccount;

    MenuItem countMenuItem;

    String listType = "KYC_PENDING";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_retailers);

        setActivityView(R.layout.activity_kyc_pending);
        setActivityTitle("KYC Pending");

        syncAccount = Motley.CreateSyncAccount(mContext);

        retailerMobileAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.retailer_mobile);

        retailerMobileAutoCompleteTextView.setThreshold(Storage.FILTER_LIST_THRESHOLD);
        retailerMobileAutoCompleteTextView.setAdapter(new NameMobileAutoCompleteAdapter(mContext));

        mRetailersDataSource = new RetailersDataSource(mContext);
        mRetailersDataSource.open();
        retailers = mRetailersDataSource.getInTrialRetailers();
        mRetailersDataSource.close();

        mRetailersAdapter = new RetailersAdapter(mContext, retailers, listType);
        retailersListView = (ListView) findViewById(R.id.retailers_list_view);
        retailersListView.setAdapter(mRetailersAdapter);

        retailerMobileAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                retailer = (RetailersDataSource) adapterView.getItemAtPosition(position);
                retailerMobileAutoCompleteTextView.setText(retailer.mobile);
                mRetailersAdapter.clear();
                mRetailersAdapter.addAll(new ArrayList<RetailersDataSource>() {
                    {
                        add(retailer);
                    }
                });
                mRetailersAdapter.notifyDataSetChanged();
            }
        });
        retailerMobileAutoCompleteTextView.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable S) {

            }

            public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
                                          int arg3) {

            }

            public void onTextChanged(CharSequence s, int start, int before,
                                      int count) {
                if (retailerMobileAutoCompleteTextView.length() == 0) {
                    mRetailersAdapter.clear();
                    mRetailersDataSource.open();
                    retailers = mRetailersDataSource.getInTrialRetailers();
                    mRetailersDataSource.close();
                    mRetailersAdapter.addAll(retailers);
                    mRetailersAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuItem syncItem = menu.findItem(R.id.action_sync);

        syncItem.setVisible(true);
        syncItem.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                Motley.sync(syncAccount);
                Toast.makeText(mContext, "Syncing Retailers",
                        Toast.LENGTH_LONG).show();
                return false;
            }
        });

        if(retailers != null) {
            countMenuItem = menu.findItem(R.id.action_count);
            countMenuItem.setVisible(true);
            countMenuItem.setActionView(R.layout.action_bar_count);
            TextView countTextView = (TextView) countMenuItem.getActionView().findViewById(R.id.retailers_count);
            countTextView.setText("" + retailers.size());
            ((Activity) mContext).invalidateOptionsMenu();
        }

        return true;
    }
}
