package com.mindsarray.pay1distributor;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.adapters.NameMobileAutoCompleteAdapter;
import com.mindsarray.pay1distributor.adapters.RetailersReportAdapter;
import com.mindsarray.pay1distributor.adapters.SalesmenAdapter;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.contenthelpers.SalesmenDataSource;
import com.mindsarray.pay1distributor.models.BalanceReportItem;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ReportBalanceTransferActivity extends BaseActivity {

    private Context mContext = ReportBalanceTransferActivity.this;
    private RetailersDataSource mRetailersDataSource, retailer;
    private ArrayList<RetailersDataSource> retailers;
    private RetailersReportAdapter mRetailersReportAdapter;
    ListView retailersListView;

    private AutoCompleteTextView retailerMobileAutoCompleteTextView;
    private TextView salesmanSelectTextView, noDataView;
    private SalesmenDataSource mSalesmenDataSource, salesman;
    private Dialog salesmanDialogSpinner;
    private ArrayList<SalesmenDataSource> salesmen;
    private SalesmenAdapter salesmenAdapter;

    protected static String selectedDate = "";
    private String reportDate = "";
    private String salesman_id = "";
    private String retailer_id = "";

    private ArrayList<BalanceReportItem> balanceReportList;
    private BalanceReportItem balanceReportItem;

    static SimpleDateFormat sdfApi;
    TextView reportDateTextView;

    ImageView prevDateImageView;
    ImageView nextDateImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_retailers);

        setActivityView(R.layout.activity_report_balance_transfer);
        setActivityTitle("Report: Balance Transfer");

        Calendar cal = Calendar.getInstance();
        sdfApi = new SimpleDateFormat("dd-MM-yyyy");
        selectedDate = sdfApi.format(cal.getTime());

        reportDateTextView = (TextView) findViewById(R.id.mDateDisplay);
        reportDateTextView.setText(Motley.convertDateFormatFromTo(selectedDate, "dd-MM-yyyy", "dd-MMM-yyyy"));

        noDataView = (TextView) findViewById(R.id.no_item_found);

        balanceReportList = new ArrayList<BalanceReportItem>();
        mRetailersDataSource = new RetailersDataSource(mContext);
        mSalesmenDataSource = new SalesmenDataSource(mContext);

        retailerMobileAutoCompleteTextView = (AutoCompleteTextView) findViewById(R.id.retailer_mobile);
        salesmanSelectTextView = (TextView) findViewById(R.id.salesman_select);
        if(Storage.user_group(mContext) == Storage.DISTRIBUTOR) {
            salesmanSelectTextView.setVisibility(View.VISIBLE);

            mSalesmenDataSource.open();
            salesmen = mSalesmenDataSource.get_many_by(Storage.getPersistentString(mContext,
                    Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID));
            mSalesmenDataSource.close();

            if(salesmen.size() == 0){
                salesmanSelectTextView.setText("NO SALESMAN");
            }
            salesmenAdapter = new SalesmenAdapter(mContext, salesmen);
            salesmanDialogSpinner = Motley.spinnerDialog(
                    mContext, "ALL SALESMAN", salesmenAdapter);

            TextView allSalesmenTextView = (TextView) salesmanDialogSpinner.findViewById(R.id.title);
            allSalesmenTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    salesman_id = "";
                    salesmanSelectTextView.setText("SELECT SALESMEN");
                    salesmanDialogSpinner.dismiss();

                    loadBalanceReport();
                }
            });
            salesmanSelectTextView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(salesmen.size() > 0)
                        salesmanDialogSpinner.show();
                }
            });

            ListView salesmenListView = (ListView) salesmanDialogSpinner.findViewById(R.id.list_view);
            salesmenListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    salesman = (SalesmenDataSource) parent.getItemAtPosition(position);
                    salesmanSelectTextView.setText(salesman.name.toUpperCase() + " " + salesman.mobile);
                    salesmanDialogSpinner.dismiss();

                    salesman_id = salesman.salesman_id;
                    retailerMobileAutoCompleteTextView.setText("");
                    retailer_id = "";

                    loadBalanceReport();
                }
            });
        }
        else if(Storage.user_group(mContext) == Storage.SALESMAN) {
            salesman_id = Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_SALESMAN_ID);
        }

        retailerMobileAutoCompleteTextView.setThreshold(Storage.FILTER_LIST_THRESHOLD);
        retailerMobileAutoCompleteTextView.setAdapter(new NameMobileAutoCompleteAdapter(mContext));

        retailerMobileAutoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                retailer = (RetailersDataSource) adapterView.getItemAtPosition(position);
                retailerMobileAutoCompleteTextView.setText(retailer.mobile);
                retailer_id = retailer.retailer_id;
                if (Storage.user_group(mContext) == Storage.DISTRIBUTOR)
                    salesman_id = "";

                loadBalanceReport();
            }
        });

        prevDateImageView = (ImageView) findViewById(R.id.prevDate);
        nextDateImageView = (ImageView) findViewById(R.id.nextDate);

        prevDateImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date sDate = null;
                    sDate = sdfApi.parse(selectedDate);
                    Date nDate = new Date(sDate.getTime() + TimeUnit.DAYS.toMillis( -1 ));
                    selectedDate = sdfApi.format(nDate);
                    loadBalanceReport();
                }
                catch(Exception e){

                }
            }
        });
        nextDateImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Date sDate = null;
                    sDate = sdfApi.parse(selectedDate);
                    Date nDate = new Date(sDate.getTime() + TimeUnit.DAYS.toMillis(1));
                    selectedDate = sdfApi.format(nDate);
                    loadBalanceReport();
                } catch (Exception e) {

                }
            }
        });

        Button calendarButton = (Button) findViewById(R.id.btnCalendar);
        calendarButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        reportDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePickerDialog();
            }
        });

        loadBalanceReport();
    }

    void loadBalanceReport(){
        Calendar cal = Calendar.getInstance();
        String todaysDate = sdfApi.format(cal.getTime());
        if(todaysDate.equals(selectedDate)){
            nextDateImageView.setVisibility(View.GONE);
        }
        else
            nextDateImageView.setVisibility(View.VISIBLE);
        reportDate = Motley.convertDateFormatFromTo(selectedDate, "dd-MM-yyyy", "dd-MMM-yyyy");
        reportDateTextView.setText(reportDate);
        if(mRetailersReportAdapter != null){
            mRetailersReportAdapter.clear();
        }
        final Map balanceReportParams = new HashMap();
        balanceReportParams.put("method", "retailerBalanceReport");
        balanceReportParams.put("d", selectedDate);
        balanceReportParams.put("s_id", salesman_id);
        balanceReportParams.put("r_id", retailer_id);
        balanceReportParams.put("request_from", "distributorApp");

        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Loading report..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONArray description = jsonObjectResponse.getJSONArray("description");
                        if(description.length() == 0){
                            balanceReportList = new ArrayList<BalanceReportItem>();
                            noDataView.setVisibility(View.VISIBLE);
                        }
                        else {
                            for (int i = 0; i < description.length(); i++) {
                                JSONObject balanceReportObject = description.getJSONObject(i);
                                mRetailersDataSource.open();
                                retailer = mRetailersDataSource.get(balanceReportObject.getString("r_id"));
                                mRetailersDataSource.close();
                                balanceReportList.add(new BalanceReportItem(balanceReportObject.getString("s_t_id"),
                                        balanceReportObject.getString("a"), balanceReportObject.getString("o"),
                                        balanceReportObject.getString("c"), balanceReportObject.getString("t"),
                                        balanceReportObject.getString("n"), balanceReportObject.getString("r_id"),
                                        balanceReportObject.getString("r_sn"), balanceReportObject.getString("r_m"),
                                        balanceReportObject.getString("s_id"), balanceReportObject.getString("sm_t_id")));
                                noDataView.setVisibility(View.GONE);
                            }
                        }

                        mRetailersReportAdapter = new RetailersReportAdapter(mContext, balanceReportList);
                        retailersListView = (ListView) findViewById(R.id.retailers_list_view);
                        retailersListView.setAdapter(mRetailersReportAdapter);
                    }
                }
                catch(Exception e) {
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(balanceReportParams, responseListener, errorBlock);
    }

    public void showDatePickerDialog() {
        DialogFragment newFragment = new DatePickerFragment();
        newFragment.show(getSupportFragmentManager(), "datePicker");
    }

    public static class DatePickerFragment extends DialogFragment
            implements DatePickerDialog.OnDateSetListener {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            Date sDate = new Date();
            try{
                 sDate = sdfApi.parse(selectedDate);
            }
            catch(Exception e){

            }
            final Calendar c = Calendar.getInstance();
            c.setTime(sDate);
            int year = c.get(Calendar.YEAR);
            int month = c.get(Calendar.MONTH);
            int day = c.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
            dialog.getDatePicker().setMaxDate(new Date().getTime());

            return dialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            final Calendar c = Calendar.getInstance();
            c.set(year, month, day);
            selectedDate = sdfApi.format(c.getTime());
            ((ReportBalanceTransferActivity) getActivity()).loadBalanceReport();
        }
    }
}
