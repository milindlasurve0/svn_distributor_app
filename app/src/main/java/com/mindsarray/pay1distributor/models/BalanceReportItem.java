package com.mindsarray.pay1distributor.models;

/**
 * Created by rohan on 18/7/15.
 */
public class BalanceReportItem {

    public String transaction_id;
    public String amount;
    public String opening;
    public String closing;
    public String time;
    public String note;
    public String retailer_id;
    public String retailer_shop_name;
    public String retailer_mobile;
    public String salesman_id;
    public String salesman_transaction_id;

    public BalanceReportItem(String transaction_id, String amount, String opening, String closing, String time, String note,
                                String retailer_id, String retailer_shop_name, String retailer_mobile, String salesman_id,
                                String salesman_transaction_id){
        this.transaction_id = transaction_id;
        this.amount = amount;
        this.opening = opening;
        this.closing = closing;
        this.time = time;
        this.note = note;
        this.retailer_id = retailer_id;
        this.retailer_shop_name = retailer_shop_name;
        this.retailer_mobile = retailer_mobile;
        this.salesman_id = salesman_id;
        this.salesman_transaction_id = salesman_transaction_id;
    }

}
