package com.mindsarray.pay1distributor.models;

/**
 * Created by rohan on 15/4/16.
 */
public class BankDetailsPair {
    public String key;
    public String value;

    public BankDetailsPair(String key, String value) {
        this.key = key;
        this.value = value;
    }
}
