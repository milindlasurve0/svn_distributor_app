package com.mindsarray.pay1distributor.models;

/**
 * Created by rohan on 12/5/15.
 */
public class GridMenuItem {
    int image;
    String title;
    int count;

    public GridMenuItem(int image, String title, int count) {
        super();
        this.image = image;
        this.title = title;
        this.count = count;
    }

    public GridMenuItem() {

    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }
}
