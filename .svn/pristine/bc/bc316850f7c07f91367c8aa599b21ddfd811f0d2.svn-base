package com.mindsarray.pay1distributor.util;

import android.content.Context;
import android.content.Intent;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.Api;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.mindsarray.pay1distributor.R;
import com.mindsarray.pay1distributor.services.FetchAddressIntentService;
import com.mindsarray.pay1distributor.receivers.AddressResultReceiver;

/**
 * Created by rohan on 21/5/15.
 */
public class GooglePlayServicesApiClient implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    public GoogleApiClient mGoogleApiClient;
    private Context mContext;
    public Location mLastLocation;
    public String latitude;
    public String longitude;
    private AddressResultReceiver mResultReceiver;
    private Boolean mReverseGeocode = false;

    public GooglePlayServicesApiClient(Context context, Api<Api.ApiOptions.NoOptions> api, Boolean reverseGeocode){
        mContext = context;
        mReverseGeocode = reverseGeocode;
        buildGoogleApiClient(api);
    }

    public synchronized void buildGoogleApiClient(Api<Api.ApiOptions.NoOptions> api) {
        mGoogleApiClient = new GoogleApiClient.Builder(mContext)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(api)
                .build();
        mGoogleApiClient.connect();
        Log.e("API: ", String.valueOf(mGoogleApiClient));
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Connected to Google Play services!
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(
                mGoogleApiClient);
        Log.e("Last location: ", String.valueOf(mLastLocation));
        if (mLastLocation != null) {
            latitude = String.valueOf(mLastLocation.getLatitude());
            longitude = String.valueOf(mLastLocation.getLongitude());
            Log.e("Lat/ Long: ", latitude + " " + longitude);
            if (!Geocoder.isPresent()) {
                Toast.makeText(mContext, R.string.no_geocoder_available,
                        Toast.LENGTH_LONG).show();
                return;
            }

            if(mReverseGeocode)
                startIntentService();
        }
    }

    protected void startIntentService() {
        Log.e("Intent service: ", "starting..");
        mResultReceiver = new AddressResultReceiver(new Handler(), mContext);
        Intent intent = new Intent(mContext, FetchAddressIntentService.class);
        intent.putExtra(Storage.RECEIVER, mResultReceiver);
        intent.putExtra(Storage.LOCATION_DATA_EXTRA, mLastLocation);
        mContext.startService(intent);
    }


    @Override
    public void onConnectionSuspended(int cause) {
        // The connection has been interrupted.
        // Disable any UI components that depend on Google APIs
        // until onConnected() is called.
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        // This callback is important for handling errors that
        // may occur while attempting to connect with Google.
        //
        // More about this in the next section.
        Log.e("Google API services: ", "Connection failed");
    }

}
