package com.mindsarray.pay1distributor;

import android.accounts.Account;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;

import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.Response;
import com.mindsarray.pay1distributor.contenthelpers.RetailersDataSource;
import com.mindsarray.pay1distributor.contenthelpers.SalesmenDataSource;
import com.mindsarray.pay1distributor.services.RegistrationIntentService;
import com.mindsarray.pay1distributor.util.Motley;
import com.mindsarray.pay1distributor.util.NetworkConnection;
import com.mindsarray.pay1distributor.util.Storage;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends ChildActivity {

    private EditText mMobileView;
    private EditText mPinView;
    private TextView mForgotPinView;

    private Context mContext = LoginActivity.this;

    RetailersDataSource mRetailersDataSource;
    SalesmenDataSource mSalesmenDataSource;

    Account syncAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        mRetailersDataSource = new RetailersDataSource(mContext);
        mSalesmenDataSource = new SalesmenDataSource(mContext);

        syncAccount = Motley.CreateSyncAccount(mContext);

        mMobileView = (EditText) findViewById(R.id.mobile);
        mPinView = (EditText) findViewById(R.id.pin);
        mForgotPinView = (TextView) findViewById(R.id.forgot_pin);

        Intent intent = new Intent(mContext, RegistrationIntentService.class);
        startService(intent);

        mPinView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int id, KeyEvent keyEvent) {
                if (id == R.id.login || id == EditorInfo.IME_NULL) {
                    attemptLogin();
                    return true;
                }
                return false;
            }
        });

        Button mSignInButton = (Button) findViewById(R.id.sign_in_button);
        mSignInButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                attemptLogin();
            }
        });

        mMobileView.addTextChangedListener(new TextWatcher() {
            @Override
            public void onTextChanged(CharSequence s, int start,
                                      int before, int count) {
                if (mMobileView.length() == 10) {
                    mPinView.requestFocus();
                }
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start,
                                          int count, int after) {

            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        mMobileView.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (!hasFocus && mMobileView.getText().toString().length() < 10)
                    mMobileView.setError("Enter 10 digits for mobile number");
                else
                    mMobileView.setError(null);
            }
        });

        mForgotPinView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                forgotPinDialog();
            }
        });

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            String mobile = extras.getString("mobile");
            String pin = extras.getString("pin");

            if (mobile != null && !mobile.equals("") && !pin.equals("")) {
                mMobileView.setText(mobile);
                mPinView.setText(pin);

                mSignInButton.performClick();
            }
        }

        if (android.os.Build.VERSION.SDK_INT >= 23 && ContextCompat.checkSelfPermission(mContext,
                Manifest.permission.READ_PHONE_STATE)
                != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) mContext,
                    new String[]{
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.CAMERA,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.READ_CONTACTS
                    },
                    Storage.REQUEST_DANGEROUS_PERMISSIONS);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case Storage.REQUEST_DANGEROUS_PERMISSIONS: {
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.

                } else {
                    Runnable afterBlock = new Runnable() {
                        @Override
                        public void run() {
                            finish();
                            startActivity(new Intent(mContext, LoginActivity.class));
                        }
                    };
                    String message = "You need to give specific permissions to use this app. Kindly, give required permissions";
                    Motley.showOneButtonDialog(mContext, message, "PERMISSIONS REQUIRED", "OK", afterBlock);
                }
                return;
            }
        }
    }
    public void forgotPinDialog(){
        try {
            final Dialog dialog = new Dialog(mContext);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(R.layout.dialog_forgot_pin);

            final EditText mobileView = (EditText) dialog.findViewById(R.id.mobileView);
            mobileView.setText(mMobileView.getText().toString());

            Button buttonConfirm = (Button) dialog
                    .findViewById(R.id.buttonConfirm);
            buttonConfirm.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final String mobile = mobileView.getText().toString();
                    if (TextUtils.isEmpty(mobile)) {
                        mobileView.setError(getString(R.string.error_field_required));
                    } else if (!Motley.isMobileValid(mobile)) {
                        mobileView.setError(getString(R.string.error_invalid_mobile));
                    }
                    else {
                        dialog.dismiss();
                        Runnable responseBlock = new Runnable() {
                            @Override
                            public void run() {
                                Intent intent = new Intent(mContext, ResetPinActivity.class);
                                intent.putExtra("mobile", mobile);
                                startActivity(intent);
                            }
                        };
                        Motley.sendOTP(mContext, mobile, responseBlock);
                    }
                }
            });

            Button buttonCancel = (Button) dialog
                    .findViewById(R.id.buttonCancel);
            buttonCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dialog.dismiss();
                }
            });

            dialog.show();
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }

    public void attemptLogin() {

        mMobileView.setError(null);
        mPinView.setError(null);

        String mobile = mMobileView.getText().toString();
        String pin = mPinView.getText().toString();

        boolean cancel = false;
        View focusView = null;

        if (!Motley.isPinValid(pin)) {
            mPinView.setError(getString(R.string.error_invalid_pin));
            focusView = mPinView;
            cancel = true;
        }
        if (TextUtils.isEmpty(mobile)) {
            mMobileView.setError(getString(R.string.error_field_required));
            focusView = mMobileView;
            cancel = true;
        } else if (!Motley.isMobileValid(mobile)) {
            mMobileView.setError(getString(R.string.error_invalid_mobile));
            focusView = mMobileView;
            cancel = true;
        }

        if (cancel) {
            focusView.requestFocus();
        } else {
            enqueueLogin(mobile, pin);
        }
    }

    private Map loginParams(String mobile, String pin){
        Storage.setUUID(mContext);

        final Map loginParams = new HashMap();
        loginParams.put("method", "authenticate");
        loginParams.put("mob", mobile);
        loginParams.put("pin", pin);
        loginParams.put("d_t", "android");
        loginParams.put("d_id", Storage.getPersistentString(mContext, Storage.SHAREDPREFERENCE_UUID));
        loginParams.put("gcm_reg_id", Storage.getSyncPreferencesString(mContext, Storage.SHAREDPREFERENCE_GCMID));
        loginParams.put("v", android.os.Build.VERSION.RELEASE);
        loginParams.put("man", android.os.Build.MANUFACTURER);

        return loginParams;
    }

    private void enqueueLogin(String mobile, String pin) {
        final ProgressDialog progressDialog = ProgressDialog.show(mContext, "", "Authenticating..");
        Response.Listener responseListener = new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("Response: ", response);
                try {
                    JSONObject jsonObjectResponse = new JSONObject(response);
                    String status = jsonObjectResponse.getString("status");

                    Log.e("Status: ", status);
                    if (status.equalsIgnoreCase("success")) {
                        JSONObject description = jsonObjectResponse.getJSONObject("description");
                        JSONObject user = description.getJSONObject("User");
                        JSONObject vars = description.getJSONObject("vars");

                        String mobileNumber = user.getString("mobile");
                        int groupId = user.getInt("group_id");

                        Storage.setPersistentString(mContext, Storage.SHAREDPREFERENCE_MOBILE_NUMBER, mobileNumber);
                        Storage.setPersistentInt(mContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID, groupId);
                        Storage.setSyncPreferencesString(mContext, Storage.SHAREDPREFERENCE_USER_GROUP_ID, "" + groupId);

                        String distributor_id = Storage.getSyncPreferencesString(mContext,
                                Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID);

                        String trial_period = vars.getString("trial_period");

                        Storage.setPersistentString(mContext, Storage.SHAREDPREFERENCE_TRIAL_PERIOD, trial_period);

                        if(groupId == Storage.DISTRIBUTOR || groupId == Storage.SALESMAN) {
                            if (groupId == Storage.DISTRIBUTOR) {
                                JSONObject distributor = jsonObjectResponse.getJSONObject("distributor");
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID, distributor.getString("id"));
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_USER_ID, user.getString("id"));
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_USER_NAME, description.getString("name"));
                                Storage.setPersistentString(mContext, Storage.SHAREDPREFERENCE_BALANCE, distributor.getString("balance"));

                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID, distributor.getString("id"));
                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_ID, "");
                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID, "");

                                JSONArray salesmen = jsonObjectResponse.getJSONArray("salesmen");

                                mSalesmenDataSource.open();
                                mSalesmenDataSource.save(salesmen);
                                mSalesmenDataSource.close();
                            } else if (groupId == Storage.SALESMAN) {
                                JSONObject salesman = jsonObjectResponse.getJSONObject("salesman");
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_ID, salesman.getString("id"));
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID, salesman.getString("dist_id"));
                                Storage.setPersistentString(mContext,
                                        Storage.SHAREDPREFERENCE_USER_NAME, salesman.getString("name"));
                                Storage.setPersistentString(mContext, Storage.SHAREDPREFERENCE_BALANCE, salesman.getString("balance"));

                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_ID, salesman.getString("id"));
                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_SALESMAN_DISTRIBUTOR_ID, salesman.getString("dist_id"));
                                Storage.setSyncPreferencesString(mContext,
                                        Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID, "");
                            }

                            Storage.setPersistentBoolean(mContext, Storage.SHAREDPREFERENCE_IS_LOGIN, true);

                            if(!distributor_id.equals(Storage.getSyncPreferencesString(mContext,
                                    Storage.SHAREDPREFERENCE_DISTRIBUTOR_ID)))
                                Storage.setSyncPreferencesString(mContext,Storage.LAST_SYNC_RUN_TIME, "");

                            ContentResolver.setIsSyncable(syncAccount, Storage.AUTHORITY, 1);
                            ContentResolver.setSyncAutomatically(syncAccount, Storage.AUTHORITY, true);
                            ContentResolver.addPeriodicSync(
                                    syncAccount,
                                    Storage.AUTHORITY,
                                    Bundle.EMPTY,
                                    Storage.SYNC_INTERVAL);
                            Motley.sync(syncAccount);

                            finish();
                            Bundle extras = getIntent().getExtras();
                            if (extras != null) {
                                String action = extras.getString("ACTION");
                                if(action.equals("FINISH_LOGIN")) {
                                    setResult(Activity.RESULT_OK,
                                            new Intent());
                                }
                                else
                                    startActivity(new Intent(mContext, DashboardActivity.class));
                            }
                            else
                                startActivity(new Intent(mContext, DashboardActivity.class));
//                            Runnable afterResponseBlock = new Runnable() {
//                                @Override
//                                public void run() {
//
//                                }
//                            };
//
//                            Motley.getAllRetailers(mContext, afterResponseBlock);
                        }

                    } else {
                        Motley.showOneButtonDialog(mContext, "Incorrect Mobile or Pin", "LOGIN FAILED", "OK");
                        mPinView.requestFocus();
                    }
                }
                catch(Exception e) {
                    Log.e("Login result: ", e.getMessage() + "");
                    e.printStackTrace();
                }
                finally {
                    progressDialog.dismiss();
                }
            }
        };
        Runnable errorBlock = new Runnable() {
            @Override
            public void run() {
                progressDialog.dismiss();
            }
        };
        NetworkConnection.getInstance(mContext).handleRequest(loginParams(mobile, pin), responseListener, errorBlock);
    }
}

